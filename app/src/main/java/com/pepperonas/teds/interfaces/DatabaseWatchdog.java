package com.pepperonas.teds.interfaces;

import android.content.Context;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public interface DatabaseWatchdog {

    void onDataChanged(Context ctx);

}
