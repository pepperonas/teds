package com.pepperonas.teds.interfaces;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public interface IconChangedListener {

    void onIconChanged(String iconValue);

}
