package com.pepperonas.teds.interfaces;

import android.content.Context;

/**
 * Created by pepperonas on 04/13/2015.
 * TaskListener
 */
public interface TaskListener {

    void onPassed(Context ctx, String arg);

    void onFailed(Context ctx, String arg);

}
