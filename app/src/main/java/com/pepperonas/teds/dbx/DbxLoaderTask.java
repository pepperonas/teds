package com.pepperonas.teds.dbx;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.exception.DropboxServerException;
import com.pepperonas.andbasx.base.ToastUtils;
import com.pepperonas.teds.R;
import com.pepperonas.teds.model.Database;
import com.pepperonas.teds.interfaces.TaskListener;
import com.pepperonas.teds.settings.Setup;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.Date;

/**
 * Created by pepperonas on 04/13/2015.
 * DbxLoaderTask
 */
public class DbxLoaderTask extends AsyncTask<String, Void, String> {

    public static final String TAG = "DbxLoaderTask";

    private Context mCtx;
    private DropboxAPI<AndroidAuthSession> mDbxApi;
    private TaskListener mTaskListener;

    private ProgressDialog mProgressDialog;

    private boolean mShowProgress;
    private boolean mCloseAppWhenFinished = false;


    public DbxLoaderTask(Context ctx, DropboxAPI<AndroidAuthSession> dbxApi, boolean showProgress) {
        mCtx = ctx;
        mProgressDialog = new ProgressDialog(mCtx);
        mDbxApi = dbxApi;
        mShowProgress = showProgress;
    }


    public void setTaskListener(TaskListener listener) {
        mTaskListener = listener;
    }


    @Override
    protected void onPreExecute() {
        if (mShowProgress) {
            mProgressDialog.setMessage(mCtx.getString(R.string.progress_sync));
            mProgressDialog.show();
        }
    }


    @Override
    protected String doInBackground(String... strings) {

        if (strings[0].equals("init_upload")) {
            return onInitUpload();
        }
        if (strings[0].equals("init_download")) {
            mCloseAppWhenFinished = true;
            return onInitDownload();
        }
        if (strings[0].equals("upload")) {
            return uploadDb(true);
        }
        if (strings[0].equals("download")) {
            return downloadDb(null);
        }

        return "Error";
    }


    private String onInitUpload() {
        try {
            File tmpDb = new File(mCtx.getDatabasePath(Database.DATABASE_NAME_TMP).getPath());
            OutputStream tmpOs = new FileOutputStream(tmpDb);
            mDbxApi.getFile(Database.DATABASE_NAME, null, tmpOs, null);
            return uploadDb(true);
        } catch (DropboxServerException e) {
            return uploadDb(false);
        } catch (FileNotFoundException | DropboxException e1) {
            e1.printStackTrace();
        }
        return "Error";
    }


    private String onInitDownload() {
        try {
            DropboxAPI.DropboxInputStream inStream = mDbxApi.getFileStream("revs.txt", null);
            OutputStream outStream = new FileOutputStream(new File(mCtx.getFilesDir() + "r_tmp.txt"));
            byte[] bytes = new byte[1024];
            int read;
            while ((read = inStream.read(bytes)) != -1) {
                outStream.write(bytes, 0, read);
            }
            addRev(inStream.getFileInfo().getMetadata().rev);
        } catch (Exception e) {
            onError(e.getLocalizedMessage());
        }

        String rev = "";
        try {
            rev = readFile(mCtx.getFilesDir() + "r_tmp.txt");
            Log.i(TAG, "onInitDownload rev: " + rev);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (rev.isEmpty()) {
            ToastUtils.toastShortFromBackground("Database not found");
            return "Error";
        } else {
            return downloadDb(rev);
        }
    }


    String readFile(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }


    private String onError(String error) {
        if (error != null) Log.e(TAG, error);
        return "Error";
    }


    private String uploadDb(boolean override) {
        try {
            File file = new File(mCtx.getDatabasePath(Database.DATABASE_NAME).getPath());
            FileInputStream fis = new FileInputStream(file);
            DropboxAPI.Entry response;
            if (override) {
                response = mDbxApi.putFileOverwrite(Database.DATABASE_NAME, fis, file.length(), null);
            } else response = mDbxApi.putFile(Database.DATABASE_NAME, fis, file.length(), null, null);
            fis.close();
            if (!response.modified.equals("0")) {
                long remote = DbxHelper.convertTimeStamp(response.modified);
                Setup.setRemoteModified(remote);
            }
            addRev(response.rev);
        } catch (Exception e) {
            e.printStackTrace();
            return "Error";
        }
        return "Upload passed!";
    }


    private String downloadDb(String rev) {
        try {
            DropboxAPI.DropboxInputStream inStream = mDbxApi.getFileStream(Database.DATABASE_NAME, rev);
            OutputStream outStream = new FileOutputStream(new File(mCtx.getDatabasePath(Database.DATABASE_NAME).getPath()));
            byte[] bytes = new byte[1024];
            int read;
            while ((read = inStream.read(bytes)) != -1) {
                outStream.write(bytes, 0, read);
            }
            addRev(inStream.getFileInfo().getMetadata().rev);
        } catch (Exception e) {
            onError(e.getLocalizedMessage());
            return "Error";
        }

        return "Download passed!";
    }


    private void addRev(String rev) {
        File file;
        try {
            file = createRevFile(rev);
            FileInputStream inputStream = null;
            try {
                inputStream = new FileInputStream(file);
                DropboxAPI.Entry newEntry = mDbxApi.putFileOverwrite("revs.txt", inputStream, file.length(), null);
            } catch (Exception e) {
                System.out.println("Something went wrong: " + e);
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            file.delete();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private File createRevFile(String rev) throws IOException {
        String content = rev;

        File file = new File(mCtx.getFilesDir() + "r.txt");

        // if file doesnt exists, then create it
        if (!file.exists()) {
            file.createNewFile();
        }

        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(content);
        bw.close();
        return file;
    }


    private String checkSync() throws FileNotFoundException, DropboxException, ParseException {

        Date remoteModified = new Date(Setup.getRemoteModified());
        Date systemModified = new Date(Setup.getSystemModified());

        if (systemModified.equals(remoteModified)) {
            return "No changes (passed)";
        }
        if (systemModified.before(remoteModified)) {
            Log.d(TAG, "checkSync " + "downloading... " + String.format("-> %s. (%s.java:%d)", getClass().getName(), getClass().getSimpleName(), 157));
            return downloadDb(null);
        } else {
            Log.d(TAG, "checkSync " + "uploading... " + String.format("-> %s. (%s.java:%d)", getClass().getName(), getClass().getSimpleName(), 163));
            return uploadDb(true);
        }
    }


    @Override
    protected void onPostExecute(String s) {
        if (s.contains("passed")) {
            if (mCloseAppWhenFinished) {
                mTaskListener.onPassed(mCtx, s + "close_app");
            } else mTaskListener.onPassed(mCtx, s);
            Log.i(TAG, "onPostExecute passed.");
        } else if (s.contains("Error")) {
            mTaskListener.onFailed(mCtx, s);
            Log.e(TAG, "onPostExecute failed.");
        }
        dismissProgressDialog();
    }


    @Override
    protected void onProgressUpdate(Void... values) {

    }


    public ProgressDialog getProgressDialog() {
        return mProgressDialog;
    }


    public void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            try {
                mProgressDialog.dismiss();
            } catch (Exception e) {
                Log.e(TAG, "dismissProgressDialog failed...");
            }

        }
    }

}

