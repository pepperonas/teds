package com.pepperonas.teds.dbx;

import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.session.AccessTokenPair;
import com.dropbox.client2.session.AppKeyPair;
import com.dropbox.client2.session.Session;
import com.pepperonas.aesprefs.AesPrefs;
import com.pepperonas.teds.settings.Pref;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by pepperonas on 04/02/2015.
 * DbxHelper
 */
public class DbxHelper {

    public final static Session.AccessType ACCESS_TYPE = Session.AccessType.APP_FOLDER;

    private final static String APP_KEY = "dnqcfzze4z7ehqz";
    private final static String APP_SECRET = "vwbg716cv64u21q";


    public static AndroidAuthSession buildSession() {

        AppKeyPair appKeyPair = new AppKeyPair(APP_KEY, APP_SECRET);
        AndroidAuthSession session;

        String[] stored = getKeys();

        if (stored != null) {
            AccessTokenPair accessToken = new AccessTokenPair(stored[0], stored[1]);
            session = new AndroidAuthSession(appKeyPair, ACCESS_TYPE, accessToken);
        } else {
            session = new AndroidAuthSession(appKeyPair, ACCESS_TYPE);
        }

        return session;
    }


    public static void storeKeys(String key, String secret) {
        AesPrefs.put(Pref.DROPBOX_TOKEN, key);
        AesPrefs.put(Pref.DROPBOX_TOKEN_SEC, secret);
    }


    public static String[] getKeys() {
        String res[] = new String[2];
        res[0] = AesPrefs.get(Pref.DROPBOX_TOKEN, "");
        res[1] = AesPrefs.get(Pref.DROPBOX_TOKEN_SEC, "");
        return res;
    }


    public static void deleteKeys() {
        AesPrefs.put(Pref.DROPBOX_TOKEN, "");
        AesPrefs.put(Pref.DROPBOX_TOKEN_SEC, "");
    }


    public static boolean isDbxRegistered() {
        return !AesPrefs.get(Pref.DROPBOX_TOKEN, "").isEmpty()
               && !AesPrefs.get(Pref.DROPBOX_TOKEN_SEC, "").isEmpty();
    }


    public static long convertTimeStamp(String modified) {
        SimpleDateFormat df = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z", Locale.US);
        try {
            Date date = df.parse(modified);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

}
