package com.pepperonas.teds.settings;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Environment;
import android.os.Handler;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.MultiSelectListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceGroup;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatCheckedTextView;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.session.TokenPair;
import com.mikepenz.community_material_typeface_library.CommunityMaterial;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.pepperonas.aesprefs.AesPrefs;
import com.pepperonas.andbasx.base.ToastUtils;
import com.pepperonas.andbasx.concurrency.ThreadUtils;
import com.pepperonas.andbasx.system.AppUtils;
import com.pepperonas.andbasx.system.SystemUtils;
import com.pepperonas.teds.R;
import com.pepperonas.teds.activities.AccountManagerActivity;
import com.pepperonas.teds.activities.SendEmailActivity;
import com.pepperonas.teds.activities.iab.IabHelper;
import com.pepperonas.teds.activities.iab.IabResult;
import com.pepperonas.teds.activities.iab.Inventory;
import com.pepperonas.teds.activities.iab.Purchase;
import com.pepperonas.teds.dbx.DbxHelper;
import com.pepperonas.teds.dbx.DbxLoaderTask;
import com.pepperonas.teds.dialogs.DialogChangelog;
import com.pepperonas.teds.dialogs.DialogDeveloperInfo;
import com.pepperonas.teds.dialogs.DialogExportInfo;
import com.pepperonas.teds.dialogs.DialogFaqs;
import com.pepperonas.teds.dialogs.DialogLicences;
import com.pepperonas.teds.dialogs.DialogPremiumFeatures;
import com.pepperonas.teds.dialogs.DialogPromotion;
import com.pepperonas.teds.interfaces.TaskListener;
import com.pepperonas.teds.model.Card;
import com.pepperonas.teds.model.CryptaAccessor;
import com.pepperonas.teds.model.DataPair;
import com.pepperonas.teds.model.Database;
import com.pepperonas.teds.utils.AesCryptIV;
import com.pepperonas.teds.utils.Const;
import com.pepperonas.teds.utils.HttpTask;
import com.pepperonas.teds.utils.Logic;
import com.samsung.android.sdk.SsdkUnsupportedException;
import com.samsung.android.sdk.pass.Spass;
import com.samsung.android.sdk.pass.SpassFingerprint;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Callable;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by pepperonas on 05.02.15.
 * SettingsActivity
 */
public class SettingsActivity
        extends PreferenceActivity
        implements Preference.OnPreferenceChangeListener,
        SharedPreferences.OnSharedPreferenceChangeListener,
        TaskListener {

    private static final String TAG = "SettingsActivity";

    static final int RC_REQUEST = 10001;
    static final String SKU_PREMIUM = "premium";

    private static final int EXPORT = 1, DELETE = 2;

    private SharedPreferences mConfigPrefs;
    private SharedPreferences.Editor mConfigEditor;

    private Database mDatabase;

    private Context mContext;

    private SettingsUpdater mSettingsUpdater;

    private SpassFingerprint mSpassFingerprint;

    private String mPwd = "";

    private BroadcastReceiver mMsgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctx, Intent intent) {
            finish();
        }
    };

    private String mIabDeveloperPayload = "black";

    private IabHelper mIabHelper;

    private boolean mDoLock = true;

    private DropboxAPI<AndroidAuthSession> mDbxApi;

    private boolean mDoNotLock = false;
    private DbxLoaderTask mDbxLoaderTask;

    private int mHiddenCounter = 0;
    private long mLastClicked = 0;

    private FingerprintManager mFingerprintManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS);
        }

        Logic.applyTheme(this);

        super.onCreate(savedInstanceState);

        Logic.ensureHideFromRecents(this);

        mSettingsUpdater = new SettingsUpdater(this);

        mContext = this;

        LocalBroadcastManager.getInstance(this).registerReceiver(mMsgReceiver, new IntentFilter(Const.LBC_INTENT_TO_FINISH));

        mPwd = getIntent().getStringExtra(Const.BNL_PW);

        mDatabase = new Database(this, mPwd);

        // Verweis auf Custom SharedPreferences-File
        PreferenceManager prefMgr = getPreferenceManager();
        prefMgr.setSharedPreferencesName(Pref.CONFIG_FILE);
        prefMgr.setSharedPreferencesMode(MODE_PRIVATE);

        // GUI laden
        addPreferencesFromResource(R.xml.prefs_main);

        // SharedPreferences und Editor initialisieren
        mConfigPrefs = getSharedPreferences(Pref.CONFIG_FILE, Context.MODE_PRIVATE);
        mConfigEditor = mConfigPrefs.edit();

        // Sicherheitseinstellungen laden
        setupSecurityScreen();

        setupUsabilityScreen();

        setupSyncScreen();

        setupPremiumScreen();

        setupAboutScreen();

        // Aktuell gewählte Konfiguration (auch) in Unterbildschirmen darstellen...
        initSummary(getPreferenceScreen());

        openUsabilityScreenIfThemeChanged();

        setIcons(Logic.getIconColorForSettings(this));
    }


    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        // Allow super to try and create a view first
        final View result = super.onCreateView(name, context, attrs);
        if (result != null) {
            return result;
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            if (name.equals("EditText")) {
                return new AppCompatEditText(this, attrs);
            } else if (name.equals("Spinner")) {
                return new AppCompatSpinner(this, attrs);
            } else if (name.equals("CheckBox")) {
                return new AppCompatCheckBox(this, attrs);
            } else if (name.equals("RadioButton")) {
                return new AppCompatRadioButton(this, attrs);
            } else if (name.equals("CheckedTextView")) {
                return new AppCompatCheckedTextView(this, attrs);
            }
        }
        return null;
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (mDoLock) {
            Logic.ensureShowLoginScreen(this);
        }
        saveDbxConnection();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }


    @Override
    protected void onPause() {
        if (mDbxLoaderTask != null) {
            mDbxLoaderTask.dismissProgressDialog();
        }
        Logic.ensureShowLoginIfLocked(this);
        if (!mDoNotLock) Logic.ensureEnableLockTimer(this, this);
        mDoNotLock = false;
        mDatabase.close();

        super.onPause();
    }


    @Override
    protected void onDestroy() {
        mPwd = null;
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMsgReceiver);
        if (mIabHelper != null) {
            mIabHelper.dispose();
            mIabHelper = null;
        }

        super.onDestroy();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mIabHelper == null) return;
        if (!mIabHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        } else Log.d(TAG, "onActivityResult handled by IABUtil.");
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        LinearLayout root = (LinearLayout) findViewById(android.R.id.list).getParent().getParent().getParent();
        Toolbar bar = (Toolbar) LayoutInflater.from(this).inflate(R.layout.settings_toolbar, root, false);
        root.addView(bar, 0); // insert at top
        bar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        return true;
    }


    /**
     * Aktuell gewählte Einstellungen auch in Unterbildschirmen darstellen...
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        updatePrefSummary(findPreference(key));
    }


    @Override
    public void onPassed(Context ctx, String arg) {
        if (arg.contains("close_app")) {
            mConfigEditor.putBoolean(Pref.PK_CB_FINGERPRINT, false).apply();
            mConfigEditor.putBoolean(Pref.PK_CB_FINGERPRINT_GOOGLE, false).apply();
            ToastUtils.toastLong(R.string.download_success_restart_app);
            ThreadUtils.runDelayed(3000, closeApp());
        }
        Log.d(TAG, "onPassed " + "" + String.format("-> %s. (%s.java:%d)", getClass().getName(), getClass().getSimpleName(), 286));
    }


    private Callable<Void> closeApp() {
        SystemUtils.closeEntireApp(this);
        return null;
    }


    @Override
    public void onFailed(Context ctx, String arg) {
        Log.d(TAG, "onFailed " + "" + String.format("-> %s. (%s.java:%d)", getClass().getName(), getClass().getSimpleName(), 292));
    }


    private void saveDbxConnection() {
        if (mDbxApi != null) {

            AndroidAuthSession session = mDbxApi.getSession();

            if (session.authenticationSuccessful()) {
                try {
                    session.finishAuthentication();

                    TokenPair tokens = session.getAccessTokenPair();
                    DbxHelper.storeKeys(tokens.key, tokens.secret);

                    mSettingsUpdater.updateSyncState(true);

                    mDbxLoaderTask = new DbxLoaderTask(this, mDbxApi, true);
                    mDbxLoaderTask.setTaskListener(this);

                    new MaterialDialog.Builder(this)
                            .title(R.string.dialog_title_restore_db)
                            .content(R.string.dialog_msg_restore_db)
                            .positiveText(R.string.yes)
                            .negativeText(R.string.no)
                            .callback(new MaterialDialog.ButtonCallback() {
                                @Override
                                public void onPositive(MaterialDialog dialog) {
                                    super.onPositive(dialog);
                                    mDbxLoaderTask.execute("init_download");
                                }


                                @Override
                                public void onNegative(MaterialDialog dialog) {
                                    super.onNegative(dialog);
                                    mDbxLoaderTask.execute("init_upload");
                                }
                            })
                            .build()
                            .show();

                } catch (IllegalStateException e) {
                    Log.e(TAG, "Couldn't authenticate with Dropbox:" + e.getLocalizedMessage());
                }
            }
        }
    }


    private void loadDropbox() {
        mDoNotLock = true;
        AndroidAuthSession session = DbxHelper.buildSession();
        mDbxApi = new DropboxAPI<>(session);
        mDbxApi.getSession().startAuthentication(SettingsActivity.this);
    }


    private void unlinkDropbox() {
        AndroidAuthSession session = DbxHelper.buildSession();
        mDbxApi = new DropboxAPI<>(session);
        mDbxApi.getSession().unlink();
        mDbxApi = null;
        DbxHelper.deleteKeys();
        mSettingsUpdater.updateSyncState(false);
    }


    private void setIcons(int color) {
        Preference security = findPreference(getString(R.string.pk_cat_security));
        security.setIcon(new IconicsDrawable(this, CommunityMaterial.Icon.cmd_security).colorRes(color).sizeDp(24));

        Preference sync = findPreference(getString(R.string.pk_cat_sync));
        sync.setIcon(new IconicsDrawable(this, CommunityMaterial.Icon.cmd_sync).colorRes(color).sizeDp(24));

        Preference usability = findPreference(getString(R.string.pk_cat_usability));
        usability.setIcon(new IconicsDrawable(this, CommunityMaterial.Icon.cmd_cursor_pointer).colorRes(color).sizeDp(24));

        Preference about = findPreference(getString(R.string.pk_cat_about));
        about.setIcon(new IconicsDrawable(this, CommunityMaterial.Icon.cmd_information_outline).colorRes(color).sizeDp(24));

        Preference premium = findPreference(getString(R.string.pk_cat_premium));
        premium.setIcon(new IconicsDrawable(this, CommunityMaterial.Icon.cmd_trophy_variant).colorRes(color).sizeDp(24));
    }


    private void openUsabilityScreenIfThemeChanged() {
        if (getIntent().getBooleanExtra(Const.BNL_RESTART_PREF, false)) {
            final PreferenceScreen mainPref = (PreferenceScreen) findPreference(getString(R.string.pk_pref_main));
            int pos = findPreference(getString(R.string.pk_cat_usability)).getOrder();
            mainPref.onItemClick(null, null, pos, 0);
        }
    }


    private void loadIab() {
        HttpTask httpTask = new HttpTask(this);
        httpTask.execute();

        String pubkey = "";
        try {
            pubkey = httpTask.get();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (pubkey.isEmpty()) {
            ToastUtils.toastShort(R.string.missing_connection);
            return;
        }

        mIabHelper = new IabHelper(this, pubkey);
        mIabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                Log.d(TAG, "Setup finished.");

                if (!result.isSuccess()) {
                    Log.e(TAG, "Problem setting up in-app billing: " + result);
                    return;
                }

                if (mIabHelper == null) return;

                Log.d(TAG, "Setup successful. Querying inventory.");
                mIabHelper.queryInventoryAsync(mGotInventoryListener);
            }
        });
    }


    // Listener that's called when we finish querying the items and subscriptions we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            Log.d(TAG, "Query inventory finished.");

            if (mIabHelper == null) return;

            if (result.isFailure()) {
                Log.e(TAG, "Failed to query inventory: " + result);
                return;
            }

            Log.d(TAG, "Query inventory was successful.");

            // Do we have the premium upgrade?
            Purchase premiumPurchase = inventory.getPurchase(SKU_PREMIUM);
            boolean isPremium = (premiumPurchase != null && verifyDeveloperPayload(premiumPurchase));
            Log.d(TAG, "User is " + (isPremium ? "PREMIUM" : "NOT PREMIUM"));

            if (isPremium) {
                mDatabase.setVersionInfoAndPremiumState(AppUtils.getVersionName(), "true");
            }

            updateUi();

            Log.d(TAG, "Initial inventory query finished; enabling main UI.");
        }
    };


    private void updateUi() {
        Preference premiumFeaturesPref = findPreference(getString(R.string.pk_pr_premium_features));
        Preference buyPremiumPref = findPreference(getString(R.string.pk_pr_get_premium));
        if (mDatabase.getIsPremium()) {
            premiumFeaturesPref.setEnabled(false);
            buyPremiumPref.setEnabled(false);
            premiumFeaturesPref.setTitle(getString(R.string.premium_unlocked));
            buyPremiumPref.setTitle(getString(R.string.thanks));
        }
    }


    /**
     * - Fingerprint (optional)
     * - Sperrung
     */
    private void setupSecurityScreen() {
        PreferenceScreen psSecurity = (PreferenceScreen) findPreference(getString(R.string.pk_cat_security));

        /*
         * Fingerprint Scanner Setup
         */
        CheckBoxPreference cbFingerprint = (CheckBoxPreference) findPreference(Pref.PK_CB_FINGERPRINT);

        final CheckBoxPreference cbFingerprintGoogle = (CheckBoxPreference) findPreference(Pref.PK_CB_FINGERPRINT_GOOGLE);
        removeGoogleFingerprintForOlderApis(psSecurity, cbFingerprintGoogle);

        CheckBoxPreference cbDirectLock = (CheckBoxPreference) findPreference(Pref.PK_CB_DIRECT_LOCK);

        boolean isFingerRegistered = false;
        boolean isFingerprintAvailable = false;

        mSpassFingerprint = new SpassFingerprint(SettingsActivity.this);
        Spass spass = new Spass();

        cbFingerprint.setChecked(mConfigPrefs.getBoolean(Pref.PK_CB_FINGERPRINT, false));
        cbDirectLock.setChecked(mConfigPrefs.getBoolean(Pref.PK_CB_DIRECT_LOCK, true));

        try {
            spass.initialize(mContext);
            isFingerRegistered = mSpassFingerprint.hasRegisteredFinger();
            isFingerprintAvailable = spass.isFeatureEnabled(Spass.DEVICE_FINGERPRINT);
        } catch (SsdkUnsupportedException e) {
            Log.e(TAG, "Error while Login using Fingerprint-Sensor. Error: " + e);
        }

        if (!isFingerprintAvailable) psSecurity.removePreference(cbFingerprint);
        if (isFingerprintAvailable && !isFingerRegistered) {
            cbFingerprint.setEnabled(false);
            cbFingerprint.setSummary(getString(R.string.pref_sum_cb_fingerprint_register_finger));
        }
        if (isFingerprintAvailable && isFingerRegistered) {
            cbFingerprint.setEnabled(true);
            cbFingerprint.setSummary(getString(R.string.pref_sum_cb_fingerprint));
            cbFingerprint.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    // Speicherung des Fingerprint-Scanners für Login
                    if ((Boolean) newValue) {
                        SharedPreferences s = getSharedPreferences(Const.DATA_FILE, MODE_PRIVATE);
                        SharedPreferences.Editor e = s.edit();
                        SparseArray sparseArray = mSpassFingerprint.getRegisteredFingerprintUniqueID();

                        String iv = String.valueOf(System.currentTimeMillis());
                        e.putString(Const.PR_FVALUE, AesCryptIV.encrypt(sparseArray.valueAt(0).toString(), mPwd, iv)).apply();
                        e.putString(Const.PR_FVALUE_IV, iv).apply();
                        mConfigEditor.putBoolean(Pref.PK_CB_FINGERPRINT, true).apply();
                        return true;
                    }
                    mConfigEditor.putBoolean(Pref.PK_CB_FINGERPRINT, false).apply();
                    return true;
                }
            });
        }

        if (Build.VERSION.SDK_INT >= 23) {
            initGoogleFingerPrint(cbFingerprintGoogle);
        }

        // Master-Passwort setzen
        Preference masterPwdPref = findPreference(getString(R.string.pk_btn_masterpwd));
        masterPwdPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                showTypeOldPasswordDialog();
                return false;
            }
        });

        // Datenbank-Sicherung
        Preference databaseBackupPref = findPreference(getString(R.string.pk_pr_make_database_accessable));
        databaseBackupPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Database db = new Database(SettingsActivity.this, mPwd);
                db.exportDatabase();
                return true;
            }
        });

        // Alle Karten exportieren
        Preference exportAllCardsPref = findPreference(getString(R.string.pk_export_all_cards));
        exportAllCardsPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                showPasswordDialog(EXPORT, getString(R.string.dialog_title_export_all_cards), getString(R.string.dialog_msg_export_all_cards));
                return true;
            }
        });

        // Exportierte Datei löschen
        Preference deleteExportedFilePref = findPreference(getString(R.string.pk_delete_exported_file));
        deleteExportedFilePref.setOnPreferenceClickListener(
                new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        try {
                            writeDataToSdCard(DELETE, Const.OVERWRITE_EXPORTED_FILE);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return true;
                    }
                });

        // Alle Karten löschen
        Preference deleteAllCardsPref = findPreference(getString(R.string.pk_delete_all_cards));
        deleteAllCardsPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                showPasswordDialog(DELETE, getString(R.string.dialog_title_delete_all_cards), getString(R.string.dialog_msg_delete_all_cards));
                return true;
            }
        });

    }


    @SuppressLint({"NewApi", "StringFormatInvalid"})
    private void initGoogleFingerPrint(final CheckBoxPreference cbFingerprintGoogle) {
        mFingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

        if (!mFingerprintManager.hasEnrolledFingerprints()) {
            ToastUtils.toastShort(getString(R.string.google_fingerprint_requires_at_least_one_finger));
            cbFingerprintGoogle.setEnabled(false);
        } else {
            cbFingerprintGoogle.setEnabled(true);
            cbFingerprintGoogle.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    if ((Boolean) newValue) {
                        final MaterialDialog dialog = new MaterialDialog.Builder(SettingsActivity.this)
                                .title(R.string.dialog_title_register_fingerprint)
                                .content(R.string.dialog_msg_register_fingerprint)
                                .build();

                        dialog.show();

                        if (ActivityCompat.checkSelfPermission(SettingsActivity.this, Manifest.permission.USE_FINGERPRINT)
                                != PackageManager.PERMISSION_GRANTED) {
                        }

                        CancellationSignal cs = new CancellationSignal();

                        mFingerprintManager.authenticate(null, cs, 0, new FingerprintManager.AuthenticationCallback() {
                            @Override
                            public void onAuthenticationError(int errorCode, CharSequence errString) {
                                super.onAuthenticationError(errorCode, errString);
                                Log.d(TAG, "onAuthenticationError  " + "");
                                ToastUtils.toastShort(getString(R.string.google_fingerprint_authentication_failed));
                            }


                            @Override
                            public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
                                super.onAuthenticationHelp(helpCode, helpString);
                                Log.d(TAG, "onAuthenticationHelp  " + "");
                            }


                            @Override
                            public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
                                super.onAuthenticationSucceeded(result);
                                dialog.dismiss();

                                ToastUtils.toastShort(getString(R.string.google_fingerprint_authentication_success));

                                Log.d(TAG, "onAuthenticationSucceeded  " + result);
                                cbFingerprintGoogle.setEnabled(true);
                                cbFingerprintGoogle.setChecked(true);
                                mConfigEditor.putBoolean(Pref.PK_CB_FINGERPRINT_GOOGLE, true).apply();

                                SharedPreferences s = getSharedPreferences(Const.DATA_FILE, MODE_PRIVATE);
                                SharedPreferences.Editor e = s.edit();

                                String iv = String.valueOf(System.currentTimeMillis());
                                e.putString(Const.PR_FGOOGLEVALUE, AesCryptIV.encrypt("google_fingerPr!nt", mPwd, iv)).apply();
                                e.putString(Const.PR_FGOOGLEVALUE_IV, iv).apply();
                            }


                            @Override
                            public void onAuthenticationFailed() {
                                super.onAuthenticationFailed();
                            }
                        }, null);
                        return false;
                    } else {
                        ToastUtils.toastShort(R.string.fingerprint_removed);
                        cbFingerprintGoogle.setEnabled(true);
                        cbFingerprintGoogle.setChecked(false);
                        mConfigEditor.putBoolean(Pref.PK_CB_FINGERPRINT_GOOGLE, false).apply();
                        return true;
                    }
                }
            });
        }
    }


    private void removeGoogleFingerprintForOlderApis(PreferenceScreen psSecurity, CheckBoxPreference cbFingerprintGoogle) {
        if (Build.VERSION.SDK_INT < 23) {
            psSecurity.removePreference(cbFingerprintGoogle);
            cbFingerprintGoogle.setChecked(mConfigPrefs.getBoolean(Pref.PK_CB_FINGERPRINT_GOOGLE, false));
        }
    }


    private void showPasswordDialog(final int mode, String title, final String msg) {

        MaterialDialog.Builder builder = new MaterialDialog.Builder(SettingsActivity.this)
                .title(title)
                .customView(R.layout.dialog_main_login, true)
                .showListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        final MaterialDialog materialDialog = (MaterialDialog) dialog;
                        final Button btnLogin = (Button) materialDialog.findViewById(R.id.btn_login);
                        btnLogin.setClickable(false);
                        btnLogin.setEnabled(false);
                        btnLogin.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                EditText et = (EditText) materialDialog.findViewById(R.id.et_masterpwd_login);
                                String input = et.getText().toString();
                                String pwdCheck = getPwdCheckFromDb(input);
                                if (Const.TEST_PWD_SOME_VALUE.equals(pwdCheck)) {

                                    switch (mode) {
                                        case EXPORT:
                                            try {
                                                exportAll();
                                                materialDialog.dismiss();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            break;

                                        case DELETE:
                                            deleteDatabaseContent();
                                            materialDialog.dismiss();
                                            break;
                                    }

                                } else {
                                    switch (mode) {
                                        case EXPORT:
                                            ToastUtils.toastShort(R.string.toast_wrong_pwd_export_cards_failed);
                                            break;

                                        case DELETE:
                                            ToastUtils.toastShort(R.string.toast_wrong_pwd_delete_cards_failed);
                                            break;
                                    }
                                }
                            }
                        });

                        TextView tv = (TextView) materialDialog.findViewById(R.id.tv_masterpwd_msg);
                        tv.setText(msg);
                        EditText et = (EditText) materialDialog.findViewById(R.id.et_masterpwd_login);
                        et.setHint(getString(R.string.password));
                        et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                            @Override
                            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                btnLogin.callOnClick();
                                return true;
                            }
                        });

                        LinearLayout horizontalCreate = (LinearLayout) materialDialog.findViewById(R.id.horizontal_create);
                        horizontalCreate.setVisibility(View.GONE);
                        createShowHidePwdCheckBox(materialDialog, et, null);
                        checkPwdLength(materialDialog, et, null, false, btnLogin);
                    }
                });

        final MaterialDialog dialog = builder.build();

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        dialog.show();
    }


    private String getPwdCheckFromDb(String input) {
        Database tmpDb = new Database(this, "");

        CryptaAccessor accessor = tmpDb.getEncryptedPassword();
        String password = accessor.getPassword();
        String iv = accessor.getIv();

        CryptaAccessor testDataAccessor = tmpDb.getPasswordTestData();
        String testData = testDataAccessor.getPassword();
        String testDataIv = testDataAccessor.getIv();

        String contentKey = AesCryptIV.decrypt(input, password, iv);

        return AesCryptIV.decrypt(contentKey, testData, testDataIv);
    }


    private void exportAll() throws IOException {
        String data = "";

        Database db = new Database(this, mPwd);
        List<Card> cardList = db.getAllCards();

        int i = 1;
        for (Card card : cardList) {
            data += Const.EXPORT_DIVIDER + " " +
                    i + ". " + getString(R.string.card) + " " +
                    Const.EXPORT_DIVIDER +
                    "\n";

            data += getString(R.string.title) + ": " + card.getTitle();

            List<DataPair> datapairs = db.getDatapairsByCard(card.getId());

            for (DataPair dataPair : datapairs) {
                data += "\n";
                data += dataPair.getDescription() + ": " + dataPair.getValue();
            }
            data += "\n";
            i++;
        }
        writeDataToSdCard(EXPORT, data);
    }


    private void writeDataToSdCard(int mode, String data) throws IOException {
        File sd = Environment.getExternalStorageDirectory();
        File file;
        file = new File(sd + File.separator + Const.EXPORT_FILE_NAME + ".txt");
        FileOutputStream fos = new FileOutputStream(file);
        OutputStreamWriter osw = new OutputStreamWriter(fos);
        osw.write(data);
        osw.close();
        if (mode == EXPORT) showExportInfoDialog();
        else if (mode == DELETE) {
            ToastUtils.toastLong(R.string.toast_exported_file_overridden);
        }
    }


    private void showExportInfoDialog() {
        new DialogExportInfo(this);
    }


    private void deleteDatabaseContent() {
        Database db = new Database(this, mPwd);
        db.formatDatabase();
        db.close();
        ToastUtils.toastLong(R.string.toast_cards_deleted_writing_default_data);
    }


    private void setupUsabilityScreen() {
        Preference startAccountManagerPref = findPreference(getString(R.string.pk_account_manager));
        startAccountManagerPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                startAccountManagerActivity();
                return true;
            }
        });

        final ListPreference themePreference = (ListPreference) findPreference(Pref.PK_LIST_THEME);
        themePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                setResult(Const.RESULT_THEME_CHANGED);
                Intent restartIntent = getIntent();
                restartIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                restartIntent.putExtra(Const.BNL_RESTART_PREF, true);
                startActivity(getIntent());
                finish();
                return true;
            }
        });

        Logic.resetHints(this);
    }


    private void startAccountManagerActivity() {
        Intent intent = new Intent(SettingsActivity.this, AccountManagerActivity.class);
        intent.putExtra(Const.BNL_PW, mPwd);
        startActivity(intent);
    }


    private void setupSyncScreen() {
        Preference pState = findPreference(getString(R.string.pk_p_sync_state));
        pState.setSummary(AesPrefs.getBoolean(R.string.ap_sync_data, false) ? getString(R.string.pref_sum_sync_state_active)
                : getString(R.string.pref_sum_sync_state_inactive));
        CheckBoxPreference cbSync = (CheckBoxPreference) findPreference(getString(R.string.ap_sync_data));
        cbSync.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean b = (Boolean) newValue;
                if (b) loadDropbox();
                else {
                    unlinkDropbox();
                    return true;
                }
                return false;
            }
        });
    }


    private void setupPremiumScreen() {
        Preference premiumPref = findPreference(getString(R.string.pk_cat_premium));
        premiumPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (Logic.isInternetConnected(SettingsActivity.this)) {
                    loadIab();
                } else {
                    ToastUtils.toastShort(R.string.no_internet);
                    return false;
                }
                return true;
            }
        });

        Preference premiumFeaturesPref = findPreference(getString(R.string.pk_pr_premium_features));
        premiumFeaturesPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                showPremiumFeaturesDialog();
                return true;
            }
        });


        Preference buyPremiumPref = findPreference(getString(R.string.pk_pr_get_premium));

        if (mDatabase.getIsPremium()) {
            updateUi();
            return;
        }

        buyPremiumPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                Handler mainHandler = new Handler(mContext.getMainLooper());

                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {

                        mIabHelper.launchPurchaseFlow(
                                SettingsActivity.this, SKU_PREMIUM, RC_REQUEST,
                                mPurchaseFinishedListener, mIabDeveloperPayload);

                    }
                };

                if (Logic.isInternetConnected(mContext)) {
                    mainHandler.post(myRunnable);
                } else {
                    ToastUtils.toastShort(R.string.no_internet);
                    return false;
                }

                return true;
            }
        });

    }


    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

            // if we were disposed of in the meantime, quit.
            if (mIabHelper == null) return;

            if (result.isFailure()) {
                Log.e(TAG, "Error purchasing: " + result);
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                Log.e(TAG, "Error purchasing. Authenticity verification failed.");
                return;
            }

            Log.d(TAG, "Purchase successful.");

            if (purchase.getSku().equals(SKU_PREMIUM)) {
                Log.d(TAG, "Purchase is premium upgrade. Congratulating user.");
                storePremiumStateInDatabase();
                updateUi();
            }
        }

    };


    private void storePremiumStateInDatabase() {
        mDatabase.setVersionInfoAndPremiumState(AppUtils.getVersionName(), "true");
    }


    /**
     * Verifies the developer payload of a purchase.
     */
    boolean verifyDeveloperPayload(Purchase p) {
        return mIabDeveloperPayload.equals(p.getDeveloperPayload());
    }


    private void showPremiumFeaturesDialog() {
        mDoLock = false;

        new DialogPremiumFeatures(this);
    }


    private void setupAboutScreen() {

        int color = Logic.getIconColorForSettings(this);

        setupFaqs(color);
        setupRateApp(color);
        setupShareApp(color);
        setupLicences(color);
        setupDevMail(color);
        setupChangelog(color);
        setupDevInfo(color);

        PreferenceScreen psAbout = (PreferenceScreen) findPreference(getString(R.string.pk_cat_about));
        showBuildNumber(psAbout);

        mLastClicked = 0;
        Preference version = findPreference(getString(R.string.pk_pr_buildversion));
        version.setIcon(new IconicsDrawable(this, GoogleMaterial.Icon.gmd_perm_device_information).colorRes(color).sizeDp(24));
        version.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                checkClickTimer();
                return true;
            }
        });
    }


    private void setupChangelog(final int color) {
        Preference changelog = findPreference(getString(R.string.pk_pr_changelog));
        changelog.setIcon(new IconicsDrawable(this, CommunityMaterial.Icon.cmd_xml).colorRes(color).sizeDp(24));
        changelog.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                showChangelog(color);
                return true;
            }
        });
    }


    public void showChangelog(int color) {
        new DialogChangelog(this, color);
    }


    private void checkClickTimer() {
        if ((mLastClicked + 1000) > System.currentTimeMillis() || mLastClicked == 0) {
            mHiddenCounter++;
        } else mHiddenCounter = 0;
        if (mHiddenCounter == 8) {
            showPromoDialog(mDatabase);
        }
        mHiddenCounter = mHiddenCounter > 8 ? 0 : mHiddenCounter;
        mLastClicked = System.currentTimeMillis();
    }


    private void showPromoDialog(final Database db) {
        new DialogPromotion(this);
    }


    public void checkPromoCode(EditText etCode, MaterialDialog matDia, Database db) {
        if (etCode.getText().toString().isEmpty()) {
            matDia.dismiss();
            return;
        }

        String input = etCode.getText().toString(), xtra = "", code;
        Calendar calendar = Calendar.getInstance();
        int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR);

        switch (dayOfYear % 7) {
            case 0:
                xtra = "_art";
                break;
            case 1:
                xtra = "_box";
                break;
            case 2:
                xtra = "_cmd";
                break;
            case 3:
                xtra = "_dea";
                break;
            case 4:
                xtra = "_end";
                break;
            case 5:
                xtra = "_fux";
                break;
            case 6:
                xtra = "_git";
                break;
        }

        code = String.valueOf((String.valueOf((dayOfYear * 299)) + xtra).hashCode()).replace("-", "");
        if (input.equals(code)) {
            // freischalten
            db.setVersionInfoAndPremiumState(AppUtils.getVersionName(), "true");
            ToastUtils.toastLong(R.string.premium_unlocked);
            matDia.dismiss();
        } else matDia.dismiss();
    }


    private void setupDevInfo(final int color) {
        Preference dev = findPreference(getString(R.string.pk_pr_developer));
        dev.setIcon(new IconicsDrawable(this, CommunityMaterial.Icon.cmd_leaf).colorRes(color).sizeDp(24));
        dev.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                showDeveloperDialog(color);
                return true;
            }
        });
    }


    private void showDeveloperDialog(int color) {
        new DialogDeveloperInfo(this, color);
    }


    private void setupFaqs(final int color) {
        Preference faqsPref = findPreference(getString(R.string.pk_pr_faqs));
        faqsPref.setIcon(new IconicsDrawable(this, CommunityMaterial.Icon.cmd_help_circle).colorRes(color).sizeDp(24));
        faqsPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                showFaqsDialog(color);
                return true;
            }
        });
    }


    public String getThemedAppName(boolean colorized) {
        if (!colorized) return "<b>" + getString(R.string.app_name) + "</b>";
        String colorStr;
        if (Logic.isDarkTheme(SettingsActivity.this)) colorStr = "03a9f4";
        else colorStr = "2e7d32";
        return "<b><font color=\"#" + colorStr + "\">" + getString(R.string.app_name) + "</font></b>";
    }


    private void showFaqsDialog(int color) {
        new DialogFaqs(this, color);
    }


    private void setupRateApp(int color) {
        Preference ratePref = findPreference(getString(R.string.pk_pr_rate));
        ratePref.setIcon(new IconicsDrawable(this, CommunityMaterial.Icon.cmd_star).colorRes(color).sizeDp(24));
        ratePref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                String appPackageName = getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                return true;
            }
        });
    }


    private void setupShareApp(int color) {
        Preference shareAppPref = findPreference(getString(R.string.pk_pr_share_app));
        shareAppPref.setIcon(new IconicsDrawable(this, GoogleMaterial.Icon.gmd_share).colorRes(color).sizeDp(24));
        shareAppPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        getString(R.string.share_app_msg) + "\n" +
                                "https://play.google.com/store/apps/details?id=com.pepperonas.teds");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                return true;
            }
        });
    }


    private void setupLicences(final int color) {
        Preference licensePref = findPreference(getString(R.string.pk_pr_license));
        licensePref.setIcon(new IconicsDrawable(this, GoogleMaterial.Icon.gmd_subject).colorRes(color).sizeDp(24));
        licensePref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                showLicenceDialog(color);
                return true;
            }
        });
    }


    private void showLicenceDialog(int color) {
        new DialogLicences(this, color);
    }


    private void setupDevMail(int color) {
        final Preference sendDevMailPref = findPreference(getString(R.string.pk_pr_send_dev_mail));
        sendDevMailPref.setIcon(new IconicsDrawable(this, GoogleMaterial.Icon.gmd_email).colorRes(color).sizeDp(24));
        sendDevMailPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent sendEmailIntent = new Intent(SettingsActivity.this, SendEmailActivity.class);
                sendEmailIntent.putExtra(Const.BNL_PW, mPwd);
                startActivity(sendEmailIntent);
                finish();
                return true;
            }
        });
    }


    public void showBuildNumber(PreferenceScreen preference) {
        try {
            Preference p = preference.findPreference(getString(R.string.pk_pr_buildversion));
            p.setSummary(getPackageManager().getPackageInfo(
                    getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }


    private void showTypeOldPasswordDialog() {
        final String msg = getString(R.string.dialog_msg_change_masterpwd_type_old_pwd);
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_title_change_masterpwd))
                .customView(R.layout.dialog_main_login, true)
                .autoDismiss(false)
                .showListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        final MaterialDialog materialDialog = (MaterialDialog) dialog;
                        materialDialog.getActionButton(DialogAction.POSITIVE).setClickable(false);
                        materialDialog.getActionButton(DialogAction.POSITIVE).setEnabled(false);
                        TextView tv = (TextView) materialDialog.findViewById(R.id.tv_masterpwd_msg);
                        tv.setText(msg);
                        EditText et = (EditText) materialDialog.findViewById(R.id.et_masterpwd_login);
                        et.setHint(getString(R.string.password));
                        et.requestFocus();
                        final Button btnLogin = (Button) materialDialog.findViewById(R.id.btn_login);
                        btnLogin.setClickable(false);
                        btnLogin.setEnabled(false);
                        btnLogin.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                EditText et = (EditText) materialDialog.findViewById(R.id.et_masterpwd_login);
                                String input = et.getText().toString();
                                String pwdCheck = getPwdCheckFromDb(input);
                                if (Const.TEST_PWD_SOME_VALUE.equals(pwdCheck)) {
                                    // Passwort korrekt
                                    materialDialog.dismiss();
                                    showSetNewPasswordDialog(input);
                                } else {
                                    // Passwort war falsch... hier noch Zähler einfügen...
                                    ToastUtils.toastShort(R.string.toast_password_is_not_matching);
                                }
                            }
                        });
                        LinearLayout horizontalCreate = (LinearLayout) materialDialog.findViewById(R.id.horizontal_create);
                        horizontalCreate.setVisibility(View.GONE);
                        createShowHidePwdCheckBox(materialDialog, et, null);
                        checkPwdLength(materialDialog, et, null, false, btnLogin);
                        et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                            @Override
                            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                btnLogin.callOnClick();
                                return true;
                            }
                        });
                    }
                });
        final MaterialDialog dialog = builder.build();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.show();
    }


    /**
     * Erstellung eines neuen Passworts.
     */
    private void showSetNewPasswordDialog(final String oldPassword) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_title_create_masterpwd))
                .customView(R.layout.dialog_main_login, true)
                .autoDismiss(false)
                .showListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        final MaterialDialog matDia = (MaterialDialog) dialog;
                        TextView tv = (TextView) matDia.findViewById(R.id.tv_masterpwd_msg);
                        tv.setText(getString(R.string.dialog_msg_create_masterpwd));
                        EditText etLogin = (EditText) matDia.findViewById(R.id.et_masterpwd_login);
                        etLogin.requestFocus();
                        EditText etCreate = (EditText) matDia.findViewById(R.id.et_masterpwd_create);
                        createShowHidePwdCheckBox(matDia, etCreate, etLogin);
                        Button btnLogin = (Button) matDia.findViewById(R.id.btn_login);
                        btnLogin.setVisibility(View.GONE);
                        final Button btnOKCreate = (Button) matDia.findViewById(R.id.btn_create);
                        btnOKCreate.setEnabled(false);
                        btnOKCreate.setClickable(false);
                        btnOKCreate.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                okFiredSetNewPassword(matDia, oldPassword);
                            }
                        });
                        etCreate.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                            @Override
                            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                btnOKCreate.callOnClick();
                                return true;
                            }
                        });
                        checkPwdLength(matDia, etCreate, etLogin, true, btnOKCreate);
                        CheckBox chbx = (CheckBox) matDia.findViewById(R.id.cbx_hide_password);
                        chbx.setText(getString(R.string.dialog_show_passwords));
                    }
                });
        final MaterialDialog dialog = builder.build();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.show();
    }


    private void okFiredSetNewPassword(MaterialDialog materialDialog, String oldPassword) {
        EditText etCreate = (EditText) materialDialog.findViewById(R.id.et_masterpwd_create);
        EditText etLogin = (EditText) materialDialog.findViewById(R.id.et_masterpwd_login);
        String inputCreate = etCreate.getText().toString();
        String inputLogin = etLogin.getText().toString();
        if (inputCreate.equals(inputLogin)) {
            changeMasterPassword(oldPassword, inputLogin);
            materialDialog.dismiss();
        } else ToastUtils.toastShort(R.string.passwords_must_be_matching);
    }


    private void changeMasterPassword(String oldPassword, String newEncryptionKey) {
        Database db = new Database(this, "");
        CryptaAccessor accessor = db.getEncryptedPassword();
        String encryptedContentKey = accessor.getPassword();
        String iv = accessor.getIv();
        String contentKey = AesCryptIV.decrypt(oldPassword, encryptedContentKey, iv);
        iv = String.valueOf(System.currentTimeMillis());
        db.storeEncryptedPassword(newEncryptionKey, contentKey, iv);
        ensureTeachFingerprint();
        mPwd = contentKey;
        ToastUtils.toastShort(R.string.toast_masterpwd_changed_success);
        if (Setup.isSyncEnabled()) {
            AndroidAuthSession session = DbxHelper.buildSession();
            DropboxAPI<AndroidAuthSession> dbxApi = new DropboxAPI<AndroidAuthSession>(session);
            mDbxLoaderTask = new DbxLoaderTask(this, dbxApi, true);
            mDbxLoaderTask.setTaskListener(this);
            mDbxLoaderTask.execute("upload");
        }
        finish();
    }


    private void ensureTeachFingerprint() {
        CheckBoxPreference cbFingerprint = (CheckBoxPreference) findPreference(Pref.PK_CB_FINGERPRINT);
        if (cbFingerprint != null && cbFingerprint.isChecked()) {
            cbFingerprint.setChecked(false);
            cbFingerprint.setChecked(true);
        }
    }


    private void checkPwdLength(final MaterialDialog dialog, final EditText et, EditText et2, final boolean create, final Button btnOk) {

        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }


            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }


            @Override
            public void afterTextChanged(Editable s) {
                checkPwdLength(s, dialog, create, btnOk);
            }
        });
        if (et2 != null) {
            et2.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }


                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }


                @Override
                public void afterTextChanged(Editable s) {
                    if (!s.toString().equals(et.getText().toString())) {
                        btnOk.setClickable(false);
                        btnOk.setEnabled(false);
                    } else checkPwdLength(s, dialog, create, btnOk);
                }
            });
        }
    }


    private void checkPwdLength(Editable s, MaterialDialog matDia, boolean create, Button btnOk) {
        if (s.length() > Const.MIN_MASTER_PWD_LENGTH) {
            btnOk.setClickable(true);
            btnOk.setEnabled(true);
        }
        if (s.length() <= Const.MIN_MASTER_PWD_LENGTH + 1) {
            btnOk.setClickable(false);
            btnOk.setEnabled(false);
        }
        if (create) {
            EditText etCreate = (EditText) matDia.findViewById(R.id.et_masterpwd_login);
            if (!etCreate.getText().toString().equals(s.toString())) {
                btnOk.setClickable(false);
                btnOk.setEnabled(false);
            }
        }
    }


    private void createShowHidePwdCheckBox(MaterialDialog matDia, final EditText et, final EditText et2) {
        et.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        if (et2 != null) {
            et2.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
        CheckBox cbxHidePwd = (CheckBox) matDia.findViewById(R.id.cbx_hide_password);
        cbxHidePwd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    et.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    if (et2 != null) {
                        et2.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    }
                    if (et2 == null) et.setSelection(et.getText().length());
                } else {
                    et.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    if (et2 != null) {
                        et2.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    }
                }
                if (et.isFocused()) et.setSelection(et.getText().length());
                if (et2 != null && et2.isFocused()) et2.setSelection(et2.getText().length());

            }
        });
    }


    /**
     * Aktuell gewählte Einstellungen auch in Unterbildschirmen darstellen...
     */
    private void initSummary(Preference p) {
        if (p instanceof PreferenceGroup) {
            PreferenceGroup pGrp = (PreferenceGroup) p;
            for (int i = 0; i < pGrp.getPreferenceCount(); i++) initSummary(pGrp.getPreference(i));
        } else updatePrefSummary(p);
    }


    /**
     * Aktuell gewählte Einstellungen auch in Unterbildschirmen darstellen...
     */
    private void updatePrefSummary(Preference p) {

        if (p instanceof ListPreference) {
            ListPreference listPref = (ListPreference) p;
            p.setSummary(listPref.getEntry());
        }

        if (p instanceof EditTextPreference) {
            EditTextPreference editTextPref = (EditTextPreference) p;
            if (p.getTitle().toString().contains("assword")) p.setSummary("******");
            else p.setSummary(editTextPref.getText());
        }

        if (p instanceof MultiSelectListPreference) {
            EditTextPreference editTextPref = (EditTextPreference) p;
            p.setSummary(editTextPref.getText());
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    public Database getDatabase() {
        return mDatabase;
    }
}


