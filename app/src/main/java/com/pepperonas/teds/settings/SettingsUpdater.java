package com.pepperonas.teds.settings;

import android.preference.CheckBoxPreference;
import android.preference.Preference;

import com.pepperonas.aesprefs.AesPrefs;
import com.pepperonas.teds.R;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class SettingsUpdater {

    private SettingsActivity mAct;


    public SettingsUpdater(SettingsActivity act) {
        mAct = act;
    }


    protected void updateSyncState(boolean syncActive) {
        Preference pState = mAct.findPreference(mAct.getString(R.string.pk_p_sync_state));
        pState.setSummary(syncActive ? mAct.getString(R.string.pref_sum_sync_state_active)
                                     : mAct.getString(R.string.pref_sum_sync_state_inactive));

        CheckBoxPreference cbxp = (CheckBoxPreference) mAct.findPreference(mAct.getString(R.string.ap_sync_data));
        cbxp.setChecked(syncActive);
        AesPrefs.putBoolean(R.string.ap_sync_data, syncActive);
    }

}
