package com.pepperonas.teds.settings;

/**
 * Created by pepperonas on 26.02.15.
 * PreferenceKeys
 */
public final class Pref {

    public static final String CONFIG_FILE = "config";

    public static final String PK_CB_FINGERPRINT = "pk_cb_fingerprint";
    public static final String PK_CB_FINGERPRINT_GOOGLE = "pk_cb_fingerprint_google";

    public static final String PK_CB_DIRECT_LOCK = "pk_cb_direct_lock";
    public static final String PK_LIST_DELAYED_LOCK = "pk_list_delayed_lock";
    public static final String DELAYED_LOCK_DEFAULT = "5";

    public static final String PK_LIST_AUTO_CLEAR_CLIPBOARD = "pk_list_auto_clear_clipboard";
    public static final String DELAYED_AUTO_CLEAR_DEFAULT = "1";

    public static final String PK_CB_HIDE_RECENTS = "pk_cb_hide_recents";

    public static final String PK_CB_OPEN_KEYBOARD = "pk_cb_open_with_keyboard";

    public static final String PK_CB_SAVE_INSTANT = "pk_cb_save_instant";

    public static final String PK_CB_APPLY_DELETE = "pk_cb_apply_delete";

    public static final String LIGHT = "light";
    public static final String PK_LIST_THEME = "pk_list_theme";
    public static final String DEFAULT_THEME = "light";

    public static final String PK_SORT_LIST = "pk_sort_by";
    public static final int SORT_BY_ALPHA_A_Z = 0;
    public static final int SORT_BY_ALPHA_Z_A = 1;
    public static final int SORT_BY_LAST_EDIT_OLD_ON_TOP = 2;
    public static final int SORT_BY_LAST_EDIT_NEW_ON_TOP = 3;

    /* Kategorie:
     *       Sync */
    public static final String PK_CAT_SYNC = "pk_cat_sync";
    //    public static final String PK_CB_SYNC = "pk_cb_sync";

    public static final String DROPBOX_TOKEN = "dbx_at";
    public static final String DROPBOX_TOKEN_SEC = "dbx_ats";

}


