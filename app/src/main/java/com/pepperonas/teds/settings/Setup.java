package com.pepperonas.teds.settings;

import com.pepperonas.aesprefs.AesPrefs;
import com.pepperonas.jbasx.format.NumberFormatUtils;
import com.pepperonas.teds.R;
import com.pepperonas.teds.utils.TimeHelper;

import java.util.Date;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class Setup {

    public static boolean isSyncEnabled() {return AesPrefs.getBoolean(R.string.ap_sync_data, false);}


    public static void setRemoteModified(long time) {
        long formatted = NumberFormatUtils.removeLastDigits(time, 3);
        AesPrefs.putLong(R.string.ap_remote_modified, formatted);
    }


    public static long getRemoteModified() {
        return AesPrefs.getLong(R.string.ap_remote_modified, 0);
    }


    public static void setSystemModifiedEarly() {
        Date earlier = new Date(1000);
        AesPrefs.putLong(R.string.ap_system_modified, earlier.getTime());
    }


    public static void setSystemModified() {
        //        if (isSyncEnabled()) {
        AesPrefs.putLong(R.string.ap_system_modified, TimeHelper.convertSystemStamp());
        //        }
    }


    public static long getSystemModified() {
        return AesPrefs.getLong(R.string.ap_system_modified, 0);
    }
}
