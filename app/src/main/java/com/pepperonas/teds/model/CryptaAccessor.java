package com.pepperonas.teds.model;

/**
 * Created by pepperonas on 04/12/2015.
 * IVAccessor
 */
public class CryptaAccessor {

    private String password;
    private String iv;


    public CryptaAccessor(String password, String iv) {
        this.password = password;
        this.iv = iv;
    }


    public String getPassword() {
        return password;
    }


    public void setPassword(String password) {
        this.password = password;
    }


    public String getIv() {
        return iv;
    }


    public void setIv(String iv) {
        this.iv = iv;
    }
}
