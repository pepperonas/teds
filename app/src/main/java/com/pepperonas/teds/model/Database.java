package com.pepperonas.teds.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import com.pepperonas.teds.activities.MainActivity;
import com.pepperonas.teds.interfaces.DatabaseWatchdog;
import com.pepperonas.teds.settings.Setup;
import com.pepperonas.teds.utils.AesCryptIV;
import com.pepperonas.teds.utils.Const;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pepperonas on 09.03.15.
 * Database
 */
public class Database extends SQLiteOpenHelper {

    private static final String TAG = "Database";
    private final Context mCtx;

    DatabaseWatchdog mDatabaseWatchdog;

    /*
     * Allgemein
     */
    public static final String KEY_ID = "id";
    public static final int COL_ID = 0;

    /*
     * Karte
     */
    public static final String K_CARD_CREA = "card_obj";
    public static final String K_CARD_TITLE = "card_title";
    public static final String K_CARD_FAVOR = "card_favorite";
    public static final String K_CARD_ICON = "card_icon";
    public static final String K_CARD_IV = "card_iv";
    public static final int COL_CARD_CREA = 1;
    public static final int COL_CARD_TITLE = 2;
    public static final int COL_CARD_FAVOR = 3;
    public static final int COL_CARD_ICON = 4;
    public static final int COL_CARD_IV = 5;

    /*
     * Label
     */
    public static final String K_LABEL_CREA = "label_obj";
    public static final String K_LABEL_TITLE = "label_title";
    public static final String K_LABEL_ICON = "label_icon";
    public static final String K_LABEL_IV = "label_iv";
    public static final int COL_LABEL_CREA = 1;
    public static final int COL_LABEL_TITLE = 2;
    public static final int COL_LABEL_ICON = 3;
    public static final int COL_LABEL_IV = 4;

    /*
     * Datenpaar
     */
    public static final String K_PAIR_CREA = "pair_obj";
    public static final String K_PAIR_DES = "pair_des";
    public static final String K_PAIR_VAL = "pair_val";
    public static final String K_PAIR_IV = "pair_iv";
    public static final int COL_PAIR_IV = 4;

    /*
     * Karte <-> Datenpaar
     */
    public static final String PAIRLINK_CARD = "pairlink_card";
    public static final String PAIRLINK_PAIR = "pairlink_pair";

    /*
     * Karte <-> Label
     */
    public static final String LABELLINK_CARD = "labellink_card";
    public static final String LABELLINK_LABEL = "labellink_label";

    /*
     * Zusätzliche Infos
     */
    private static final String INFO_DESC = "inf";
    private static final String INFO_DATA_0 = "data";
    private static final String INFO_DATA_1 = "data_add";
    private static final String INFO_DATA_2 = "data_add_2";
    private static final String INFO_DATA_3 = "data_add_3";

    /*
     * Accounts
     */
    private static final String K_ACCOUNT_CREA = "acc_obj";
    private static final String K_ACCOUNT_NAME = "acc";
    private static final String K_ACCOUNT_IV = "iv";

    public static final String[]
            C_ALL_KEYS = new String[]{KEY_ID, K_CARD_CREA, K_CARD_TITLE, K_CARD_FAVOR, K_CARD_ICON},
            L_ALL_KEYS = new String[]{KEY_ID, K_LABEL_CREA, K_LABEL_TITLE, K_LABEL_ICON},
            P_ALL_KEYS = new String[]{KEY_ID, K_PAIR_CREA, K_PAIR_DES, K_PAIR_VAL};

    /*
     * Datenbank-Infos
     */
    public static final String DATABASE_NAME = "teds.db";
    public static final String DATABASE_NAME_TMP = "teds_tmp.db";

    public static final String TBL_CARDS = "cards";

    public static final String TBL_LABELS = "lab_els";

    public static final String TBL_PAIRS = "pairs";

    public static final String TBL_LINKS_TO_PAIRS = "links_to_pairs";

    public static final String TBL_LINKS_TO_LABELS = "links_to_labels";

    public static final String TBL_INFO = "infos";

    private static final String TBL_ACCOUNTS = "accounts";

    // Versionierung für Erkennung von Änderungen
    public static final int DATABASE_VERSION = 2;

    public static final String _UNSET_ = "x";
    public static final String _NULL_ = "0";
    public static final String _NULLCARDICON_ = "'" + Const.ICON_UNSET + "_" + Const.ICON_UNSET + "'";
    private static final String CSV_DIVIDER = ";";

    private static final String PASSWORD_HOLDER = "pwd";
    private static final String PASSWORD_TEST_DATA = "pwd_test";
    private static final String LAST_EDIT = "last_edit";
    private static final String VERSION = "version";
    private static final String PASSWORD_POSITION = "1";
    private static final String PASSWORD_TEST_DATA_POSITION = "2";
    private static final String LAST_EDIT_POSITION = "3";
    private static final String APP_INFO = "4";

    private static final String SQLITE_CREATE_CARDS_TABLE =
            "create table if not exists " + TBL_CARDS
            + " (" + KEY_ID + " integer primary key autoincrement, "
            + K_CARD_CREA + " text not null, "
            + K_CARD_TITLE + " text not null, "
            + K_CARD_FAVOR + " integer default " + _NULL_ + ", "
            + K_CARD_ICON + " text default " + _NULLCARDICON_ + ", "
            + K_CARD_IV + " text not null "
            + ");";

    private static final String SQLITE_CREATE_LABEL_TABLE =
            "create table if not exists " + TBL_LABELS
            + " (" + KEY_ID + " integer primary key autoincrement, "
            + K_LABEL_CREA + " text not null, "
            + K_LABEL_TITLE + " text not null, "
            + K_LABEL_ICON + " text default " + _NULL_ + ", "
            + K_LABEL_IV + " text not null "
            + ");";

    private static final String SQLITE_CREATE_PAIRS_TABLE =
            "create table if not exists " + TBL_PAIRS
            + " (" + KEY_ID + " integer primary key autoincrement, "
            + K_PAIR_CREA + " text not null, "
            + K_PAIR_DES + " text not null, "
            + K_PAIR_VAL + " text not null, "
            + K_PAIR_IV + " text not null "
            + ");";

    private static final String SQLITE_CREATE_CARDPAIR_LINKS_TABLE =
            "create table if not exists " + TBL_LINKS_TO_PAIRS
            + " (" + KEY_ID + " integer primary key autoincrement, "
            + PAIRLINK_CARD + " integer, "
            + PAIRLINK_PAIR + " integer "
            + ");";

    private static final String SQLITE_CREATE_CARDLABEL_LINKS_TABLE =
            "create table if not exists " + TBL_LINKS_TO_LABELS
            + " (" + KEY_ID + " integer primary key autoincrement, "
            + LABELLINK_CARD + " integer, "
            + LABELLINK_LABEL + " integer "
            + ");";

    private static final String SQLITE_CREATE_INFO_TABLE =
            "create table if not exists " + TBL_INFO
            + " (" + KEY_ID + " integer primary key autoincrement, "
            + INFO_DESC + " text not null, "
            + INFO_DATA_0 + " text not null, "
            + INFO_DATA_1 + " text not null, "
            + INFO_DATA_2 + " text not null, "
            + INFO_DATA_3 + " text not null "
            + ");";

    private static final String SQLITE_CREATE_ACCOUNT_TABLE =
            "create table if not exists " + TBL_ACCOUNTS
            + " (" + KEY_ID + " integer primary key autoincrement, "
            + K_ACCOUNT_CREA + " text not null, "
            + K_ACCOUNT_NAME + " text not null, "
            + K_ACCOUNT_IV + " text not null "
            + ");";

    private String mPwd;


    public Database(Context ctx, String pw) {
        super(ctx, DATABASE_NAME, null, DATABASE_VERSION);

        mPwd = pw;
        mCtx = ctx;

        Log.v(TAG, "opening Database...");
    }


    public Database(MainActivity main, String pw) {
        super(main, DATABASE_NAME, null, DATABASE_VERSION);

        mPwd = pw;
        mCtx = main;
        mDatabaseWatchdog = main;

        Log.v(TAG, "opening Database...");
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQLITE_CREATE_CARDS_TABLE);
        db.execSQL(SQLITE_CREATE_LABEL_TABLE);
        db.execSQL(SQLITE_CREATE_PAIRS_TABLE);
        db.execSQL(SQLITE_CREATE_CARDPAIR_LINKS_TABLE);
        db.execSQL(SQLITE_CREATE_CARDLABEL_LINKS_TABLE);
        db.execSQL(SQLITE_CREATE_INFO_TABLE);
        db.execSQL(SQLITE_CREATE_ACCOUNT_TABLE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {

        // alte Datenbank zerstören
        database.execSQL("DROP TABLE IF EXISTS " + SQLITE_CREATE_CARDS_TABLE);
        database.execSQL("DROP TABLE IF EXISTS " + SQLITE_CREATE_LABEL_TABLE);
        database.execSQL("DROP TABLE IF EXISTS " + SQLITE_CREATE_PAIRS_TABLE);
        database.execSQL("DROP TABLE IF EXISTS " + SQLITE_CREATE_CARDPAIR_LINKS_TABLE);
        database.execSQL("DROP TABLE IF EXISTS " + SQLITE_CREATE_CARDLABEL_LINKS_TABLE);

        // Datenbank neu-erstellen
        onCreate(database);
    }


    public void storeEncryptedPassword(String encryptionKey, String contentKey, String iv) {
        setModified();

        SQLiteDatabase db = this.getWritableDatabase();

        String sqlite =
                "INSERT OR REPLACE INTO " + TBL_INFO + " " + "(" +
                KEY_ID + ", " +
                INFO_DESC + ", " +
                INFO_DATA_0 + ", " +
                INFO_DATA_1 + ", " +
                INFO_DATA_2 + ", " +
                INFO_DATA_3 + ") " +

                "VALUES (" +
                "" + PASSWORD_POSITION + ", " +
                "'" + PASSWORD_HOLDER + "', " +
                "'" + AesCryptIV.encrypt(encryptionKey, contentKey, iv) + "', " +
                "'" + iv + "', " +
                "'" + Const.ICON_UNSET + "', " +
                "'" + Const.ICON_UNSET + "')";

        db.execSQL(sqlite);
    }


    public void storePwdCheckData(String encryptionKey, String testData, String iv) {

        SQLiteDatabase db = this.getWritableDatabase();

        String sqlite =
                "INSERT OR REPLACE INTO " + TBL_INFO + " " + "(" +
                KEY_ID + ", " +
                INFO_DESC + ", " +
                INFO_DATA_0 + ", " +
                INFO_DATA_1 + ", " +
                INFO_DATA_2 + ", " +
                INFO_DATA_3 + ") " +

                "VALUES (" +
                "" + PASSWORD_TEST_DATA_POSITION + ", " +
                "'" + PASSWORD_TEST_DATA + "', " +
                "'" + AesCryptIV.encrypt(encryptionKey, testData, iv) + "', " +
                "'" + iv + "', " +
                "'" + Const.ICON_UNSET + "', " +
                "'" + Const.ICON_UNSET + "')";

        db.execSQL(sqlite);
    }


    public boolean getIsPremium() {
        String premiumString = null;

        String selectQuery = "select * from " + TBL_INFO +
                             " where " + KEY_ID + " = " + APP_INFO;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                String iv = c.getString(c.getColumnIndex(INFO_DATA_2));

                premiumString = AesCryptIV.decrypt(
                        mPwd, c.getString(c.getColumnIndex(INFO_DATA_1)), iv);

            } while (c.moveToNext());
        }
        c.close();

        Log.d(TAG, "getIsPremium return: " + premiumString);

        return Boolean.parseBoolean(premiumString);
    }


    public void setVersionInfoAndPremiumState(String versionInfo, String premiumState) {
        SQLiteDatabase db = this.getWritableDatabase();

        String iv = getIV();

        String sqlite =
                "INSERT OR REPLACE INTO " + TBL_INFO + " " + "(" +
                KEY_ID + ", " +
                INFO_DESC + ", " +
                INFO_DATA_0 + ", " +
                INFO_DATA_1 + ", " +
                INFO_DATA_2 + ", " +
                INFO_DATA_3 + ") " +

                "VALUES (" +
                "" + APP_INFO + ", " +
                "'" + VERSION + "', " +
                "'" + AesCryptIV.encrypt(mPwd, versionInfo, iv) + "', " +
                "'" + AesCryptIV.encrypt(mPwd, premiumState, iv) + "', " +
                "'" + iv + "', " +
                "'" + Const.ICON_UNSET + "')";

        db.execSQL(sqlite);
    }


    public long createCard(Card card) {
        setModified();

        String iv = getIV();

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(K_CARD_TITLE, AesCryptIV.encrypt(mPwd, card.getTitle(), iv));
        values.put(K_CARD_CREA, AesCryptIV.encrypt(mPwd, card.getCreated(), iv));
        values.put(K_CARD_FAVOR, card.isFavorite());
        values.put(K_CARD_ICON, AesCryptIV.encrypt(mPwd, card.getRawIcon(), iv));
        values.put(K_CARD_IV, iv);

        return db.insert(TBL_CARDS, null, values);
    }


    private String getIV() {return String.valueOf(System.currentTimeMillis());}


    public long createDataPair(DataPair dataPair) {
        setModified();

        String iv = getIV();

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(K_PAIR_CREA, AesCryptIV.encrypt(mPwd, dataPair.getCreated(), iv));
        values.put(K_PAIR_DES, AesCryptIV.encrypt(mPwd, dataPair.getDescription(), iv));
        values.put(K_PAIR_VAL, AesCryptIV.encrypt(mPwd, dataPair.getValue(), iv));
        values.put(K_PAIR_IV, iv);

        return db.insert(TBL_PAIRS, null, values);
    }


    public long createDataPairLink(long cardId, long datapairId) {
        setModified();

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(PAIRLINK_CARD, cardId);
        values.put(PAIRLINK_PAIR, datapairId);

        return db.insert(TBL_LINKS_TO_PAIRS, null, values);
    }


    public long createLabel(Label label) {
        setModified();

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(K_LABEL_TITLE, AesCryptIV.encrypt(mPwd, label.getName(), label.getIV()));
        values.put(K_LABEL_CREA, AesCryptIV.encrypt(mPwd, label.getCreated(), label.getIV()));
        values.put(K_LABEL_ICON, label.getIcon());
        values.put(K_LABEL_IV, label.getIV());

        return db.insert(TBL_LABELS, null, values);
    }


    private void setModified() {
        Setup.setSystemModified();
        if (mDatabaseWatchdog != null) {
            mDatabaseWatchdog.onDataChanged(mCtx);
        }
    }


    public long createLabelLink(long cardId, long Id) {
        setModified();

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(LABELLINK_CARD, cardId);
        values.put(LABELLINK_LABEL, Id);

        return db.insert(TBL_LINKS_TO_LABELS, null, values);
    }


    public long createAccount(AccountInfo ai) {
        setModified();

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(K_ACCOUNT_CREA, AesCryptIV.encrypt(mPwd, ai.getCreated(), ai.getIv()));
        values.put(K_ACCOUNT_NAME, AesCryptIV.encrypt(mPwd, ai.getName(), ai.getIv()));
        values.put(K_ACCOUNT_IV, ai.getIv());

        return db.insert(TBL_ACCOUNTS, null, values);
    }


    public List<AccountInfo> getAllAccounts() {
        List<AccountInfo> accounts = new ArrayList<AccountInfo>();

        String selectQuery = "select * from " + TBL_ACCOUNTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                String iv = c.getString(c.getColumnIndex(K_ACCOUNT_IV));

                accounts.add(new AccountInfo(
                        c.getInt(COL_ID),
                        AesCryptIV.decrypt(mPwd, c.getString(c.getColumnIndex(K_ACCOUNT_NAME)), iv),
                        c.getString(c.getColumnIndex(K_ACCOUNT_IV))));

            } while (c.moveToNext());
        }
        c.close();

        return accounts;
    }


    public long updateCard(Card card) {
        setModified();

        String iv = getIV();

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(K_CARD_TITLE, AesCryptIV.encrypt(mPwd, card.getTitle(), iv));
        values.put(K_CARD_CREA, AesCryptIV.encrypt(mPwd, card.getCreated(), iv));
        values.put(K_CARD_FAVOR, card.isFavorite());
        values.put(K_CARD_ICON, AesCryptIV.encrypt(mPwd, card.getRawIcon(), iv));
        values.put(K_CARD_IV, iv);

        return db.update(TBL_CARDS,
                         values,
                         KEY_ID + " = ?",
                         new String[]{String.valueOf(card.getId())});
    }


    public int updateLabel(Label label) {
        setModified();

        String iv = getIV();

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(K_LABEL_TITLE, AesCryptIV.encrypt(mPwd, label.getName(), iv));
        values.put(K_LABEL_ICON, label.getIcon());
        values.put(K_LABEL_IV, iv);

        return db.update(TBL_LABELS, values, KEY_ID + " = ?",
                         new String[]{String.valueOf(label.getId())});
    }


    public int countAllCards() {
        long start = gt();

        String selectQuery = "select * from " + TBL_CARDS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        int size = c.getCount();
        c.close();

        Log.v(TAG, "countAllCards=" + size + " (" + (gt() - start) + "ms)");
        return size;
    }


    public int countAllDataPairs() {
        long start = gt();

        String selectQuery = "select * from " + TBL_PAIRS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        int size = c.getCount();
        c.close();

        Log.v(TAG, "countAllDataPair=" + size + " (" + (gt() - start) + "ms)");
        return size;
    }


    public int countAllLabels() {
        long start = gt();

        String selectQuery = "select * from " + TBL_LABELS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        int size = c.getCount();
        c.close();

        Log.v(TAG, "countAllLabels=" + size + " (" + (gt() - start) + "ms)");
        return size;
    }


    public int countFavoriteCards() {
        long start = gt();

        String selectQuery = "select * from " + TBL_CARDS + " where " + K_CARD_FAVOR + " = 1";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        int i = c.getCount();
        c.close();

        Log.v(TAG, "countFavoriteCards (" + (gt() - start) + "ms)");
        return i;
    }


    public int countUnlabeledCards() {
        long start = gt();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery
                = "select * from " + TBL_CARDS +
                  " where not exists " +
                  "(select * from " + TBL_LINKS_TO_LABELS +
                  " where " +
                  TBL_LINKS_TO_LABELS + "." + LABELLINK_CARD + " = " + TBL_CARDS + "." + KEY_ID +
                  " AND " +
                  TBL_LINKS_TO_LABELS + "." + LABELLINK_CARD + " = " + TBL_CARDS + "." + KEY_ID +
                  ")";

        Cursor c = db.rawQuery(selectQuery, null);
        int i = c.getCount();
        c.close();

        Log.v(TAG, "countUnlabeledCards (" + (gt() - start) + "ms)");
        return i;
    }


    public int countWeakPasswords() {
        long start = gt();

        List<Integer> pairIds = new ArrayList<Integer>();
        List<Integer> pairLinkIds = new ArrayList<Integer>();
        String selectQuery = "select * from " + TBL_PAIRS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                String iv = c.getString(COL_PAIR_IV);

                String desc = AesCryptIV.decrypt(mPwd, c.getString(c.getColumnIndex(K_PAIR_DES)), iv);
                String value = AesCryptIV.decrypt(mPwd, c.getString(c.getColumnIndex(K_PAIR_VAL)), iv);
                if (isPassword(desc) &&
                    value.length() <= Const.PROGRBAR_PWD_MIN_LENGTH) {
                    // schlechtes Passwort
                    pairIds.add(c.getInt(c.getColumnIndex(KEY_ID)));
                }
            } while (c.moveToNext());
        }
        c.close();

        selectQuery = "select * from " + TBL_LINKS_TO_PAIRS;
        c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                if (pairIds.contains(c.getInt(c.getColumnIndex(KEY_ID)))) {
                    // nur wenn in Id-Liste eingetragen
                    pairLinkIds.add(c.getInt(c.getColumnIndex(PAIRLINK_CARD)));
                }
            } while (c.moveToNext());
        }
        c.close();

        int i = 0;

        selectQuery = "select * from " + TBL_CARDS;
        c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                if (pairLinkIds.contains(c.getInt(c.getColumnIndex(KEY_ID)))) {
                    i++;
                }
            } while (c.moveToNext());
        }
        c.close();

        Log.v(TAG, "countWeakPasswords (" + (gt() - start) + "ms)");
        return i;
    }


    public int countAllCardsByLabel(Label label) {
        long start = gt();

        String selectQuery =
                "select * from " + TBL_CARDS + "" +
                " ca, " + TBL_LABELS + "" +
                " la, " + TBL_LINKS_TO_LABELS + "" +
                " clacc where la." + K_LABEL_TITLE + " = '" + AesCryptIV.encrypt(mPwd, label.getName(), label.getIV()) + "'" +
                " AND la." + KEY_ID + " = " + "clacc." + LABELLINK_LABEL + "" +
                " AND ca." + KEY_ID + " = " + "clacc." + LABELLINK_CARD;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        int i = c.getCount();
        c.close();

        Log.v(TAG, "countAllCardsByLabel (" + (gt() - start) + "ms)");
        return i;
    }


    public Card getCard(long cardId) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "select * from " + TBL_CARDS + " where " + KEY_ID + " = " + cardId;

        Cursor c = db.rawQuery(selectQuery, null);

        Card card = new Card();
        if (c.getCount() > 0) {
            if (c.moveToFirst()) {
                String iv = c.getString(COL_CARD_IV);

                card = new Card(c.getInt(COL_ID),
                                AesCryptIV.decrypt(mPwd, c.getString(COL_CARD_TITLE), iv),
                                AesCryptIV.decrypt(mPwd, c.getString(COL_CARD_CREA), iv),
                                c.getInt(COL_CARD_FAVOR),
                                (c.getString((COL_CARD_ICON)).contains(Const.ICON_DIVIDER)) ?
                                new CardIcon(c.getString(COL_CARD_ICON)) :
                                new CardIcon(AesCryptIV.decrypt(mPwd, c.getString(COL_CARD_ICON), iv)));
            }

        }
        c.close();
        return card;
    }


    public Label getLabel(long Id) {
        String selectQuery = "select * from " + TBL_LABELS + "" +
                             " where " + KEY_ID + " = " + Id + "";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            if (c.getCount() >= 0) {
                String iv = c.getString(COL_LABEL_IV);

                Label label = new Label(c.getInt(COL_ID), AesCryptIV.decrypt(mPwd, c.getString(COL_LABEL_TITLE), iv),
                                        "", "", iv);
                c.close();
                return label;
            } else {
                c.close();
                Log.e(TAG, "WARNING: LABEL = NULL! Returning default...");
                return new Label(0, "", "");
            }
        }
        Log.e(TAG, "WARNING: LABEL = NULL! Returning default...");
        return new Label(0, "", "");
    }


    public List<Card> getAllCards() {
        long start = gt();

        List<Card> cards = new ArrayList<Card>();
        String selectQuery = "select * from " + TBL_CARDS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                String iv = c.getString(COL_CARD_IV);

                cards.add(new Card(c.getInt(COL_ID),
                                   AesCryptIV.decrypt(mPwd, c.getString(COL_CARD_TITLE), iv),
                                   AesCryptIV.decrypt(mPwd, c.getString(COL_CARD_CREA), iv),
                                   c.getInt(COL_CARD_FAVOR),
                                   (c.getString((COL_CARD_ICON)).contains(Const.ICON_DIVIDER)) ?
                                   new CardIcon(c.getString(COL_CARD_ICON)) :
                                   new CardIcon(AesCryptIV.decrypt(mPwd, c.getString(COL_CARD_ICON), iv))));
            } while (c.moveToNext());
        }
        c.close();

        Log.v(TAG, "getAllCards (" + (gt() - start) + "ms)");
        return cards;
    }


    public List<Label> getAllLabels() {
        List<Label> labels = new ArrayList<Label>();

        String selectQuery = "select * from " + TBL_LABELS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                String iv = c.getString(COL_LABEL_IV);

                labels.add(new Label(c.getInt(COL_ID),
                                     AesCryptIV.decrypt(mPwd, c.getString(COL_LABEL_TITLE), iv),
                                     AesCryptIV.decrypt(mPwd, c.getString(COL_LABEL_CREA), iv),
                                     c.getString(COL_LABEL_ICON), iv));
            } while (c.moveToNext());
        }
        c.close();
        return labels;
    }


    public List<DataPair> getAllDataPairs() {
        long start = gt();

        List<DataPair> pairs = new ArrayList<DataPair>();

        String selectQuery = "select * from " + TBL_PAIRS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                String iv = c.getString(COL_PAIR_IV);

                DataPair dataPair = new DataPair();
                dataPair.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                dataPair.setDescription(AesCryptIV.decrypt(mPwd, c.getString(c.getColumnIndex(K_PAIR_DES)), iv));
                dataPair.setValue(AesCryptIV.decrypt(mPwd, c.getString(c.getColumnIndex(K_PAIR_VAL)), iv));
                pairs.add(dataPair);
            } while (c.moveToNext());
        }
        c.close();

        Log.v(TAG, "getAllDataPair=" + pairs.size() + " (" + (gt() - start) + "ms)");
        return pairs;
    }


    public List<Link> getAllLabelLinks() {
        List<Link> links = new ArrayList<Link>();

        String selectQuery = "select * from " + TBL_LINKS_TO_LABELS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                Link link = new Link();
                link.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                link.setLinkA(c.getLong(c.getColumnIndex(LABELLINK_CARD)));
                link.setLinkB(c.getLong(c.getColumnIndex(LABELLINK_LABEL)));
                links.add(link);
            } while (c.moveToNext());
        }
        c.close();

        return links;
    }


    public List<Card> getAllCardsByLabel(String Name, String IV) {
        long start = gt();

        List<Card> cards = new ArrayList<Card>();
        String selectQuery = "select * from " + TBL_CARDS + "" +
                             " ca, " + TBL_LABELS + "" +
                             " la, " + TBL_LINKS_TO_LABELS + "" +
                             " clacc where la." + K_LABEL_TITLE + " = '" + AesCryptIV.encrypt(mPwd, Name, IV) + "'" +
                             " AND la." + KEY_ID + " = " + "clacc." + LABELLINK_LABEL + "" +
                             " AND ca." + KEY_ID + " = " + "clacc." + LABELLINK_CARD;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {

                String ivSaved = c.getString(COL_CARD_IV);

                cards.add(new Card(c.getInt(COL_ID),
                                   AesCryptIV.decrypt(mPwd, c.getString(COL_CARD_TITLE), ivSaved),
                                   AesCryptIV.decrypt(mPwd, c.getString(COL_CARD_CREA), ivSaved),
                                   c.getInt(COL_CARD_FAVOR),
                                   new CardIcon(c.getString(COL_CARD_ICON))));
            } while (c.moveToNext());
        }
        c.close();
        Log.v(TAG, "getAllCardsByLabel (" + (gt() - start) + "ms)");
        return cards;
    }


    public List<Label> getAllLabelByCard(int cardId) {
        long start = gt();

        List<Label> labels = new ArrayList<Label>();
        String selectQuery = "select * from " + TBL_CARDS + " ca, "
                             + TBL_LABELS + " la, " + TBL_LINKS_TO_LABELS + " clacc where ca."
                             + KEY_ID + " = '" + cardId + "' AND la." + KEY_ID
                             + " = " + "clacc." + LABELLINK_LABEL + " AND ca." + KEY_ID + " = "
                             + "clacc." + LABELLINK_CARD;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                String iv = c.getString(c.getColumnIndex(K_LABEL_IV));

                Label label = new Label();
                label.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                label.setName(AesCryptIV.decrypt(mPwd, c.getString(c.getColumnIndex(K_LABEL_TITLE)), iv));
                label.setCreated(AesCryptIV.decrypt(mPwd, c.getString(c.getColumnIndex(K_LABEL_CREA)), iv));
                label.setIcon(c.getString(c.getColumnIndex(K_LABEL_ICON)));
                labels.add(label);
            } while (c.moveToNext());
        }
        c.close();
        Log.v(TAG, "getAllLabelByCard (" + (gt() - start) + "ms)");
        return labels;
    }


    public Label getLabelByName(String name) {
        String selectQuery = "select * from " + TBL_LABELS + "" +
                             " where " + K_LABEL_TITLE + " = '" + name + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            if (c.getCount() >= 0) {

                String iv = c.getString(c.getColumnIndex(K_LABEL_IV));

                Label label = new Label(c.getInt(COL_ID),
                                        AesCryptIV.decrypt(mPwd, c.getString(COL_LABEL_TITLE), iv),
                                        iv);
                c.close();
                return label;
            } else {
                c.close();
                Log.e(TAG, "WARNING: LABEL = NULL! Returning default...");
                return new Label(0, "", getIV());
            }
        }
        Log.e(TAG, "WARNING: LABEL = NULL! Returning default...");
        return new Label(0, "", getIV());
    }


    public List<DataPair> getDatapairsByCard(long cardId) {
        long start = gt();

        List<DataPair> dataPairs = new ArrayList<DataPair>();
        String selectQuery = "select * from " + TBL_CARDS + " ca, "
                             + TBL_PAIRS + " pa, " + TBL_LINKS_TO_PAIRS + " cpacc where ca."
                             + KEY_ID + " = '" + cardId + "' AND pa." + KEY_ID
                             + " = " + "cpacc." + PAIRLINK_PAIR + " AND ca." + KEY_ID + " = "
                             + "cpacc." + PAIRLINK_CARD;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                String iv = c.getString(c.getColumnIndex(K_PAIR_IV));

                DataPair dataPair = new DataPair();
                dataPair.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                dataPair.setDescription(AesCryptIV.decrypt(mPwd, c.getString(c.getColumnIndex(K_PAIR_DES)), iv));
                dataPair.setValue(AesCryptIV.decrypt(mPwd, c.getString(c.getColumnIndex(K_PAIR_VAL)), iv));
                dataPairs.add(dataPair);
            } while (c.moveToNext());
        }
        Log.v(TAG, "getDatapairsByCard (" + (gt() - start) + "ms)");
        c.close();
        return dataPairs;
    }


    public List<Link> getAllDataPairLinks() {
        List<Link> links = new ArrayList<Link>();

        String selectQuery = "select * from " + TBL_LINKS_TO_PAIRS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                Link link = new Link();
                link.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                link.setLinkA(c.getLong(c.getColumnIndex(PAIRLINK_CARD)));
                link.setLinkB(c.getLong(c.getColumnIndex(PAIRLINK_PAIR)));
                links.add(link);
            } while (c.moveToNext());
        }
        c.close();

        return links;
    }


    public List<Card> getFavoriteCards() {
        long start = gt();

        List<Card> cards = new ArrayList<Card>();
        String selectQuery = "select * from " + TBL_CARDS + " where " + K_CARD_FAVOR + " = 1";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                String iv = c.getString(COL_CARD_IV);

                cards.add(new Card(c.getInt(COL_ID),
                                   AesCryptIV.decrypt(mPwd, c.getString(COL_CARD_TITLE), iv),
                                   AesCryptIV.decrypt(mPwd, c.getString(COL_CARD_CREA), iv),
                                   c.getInt(COL_CARD_FAVOR),
                                   new CardIcon(c.getString(COL_CARD_ICON))));
            } while (c.moveToNext());
        }
        c.close();

        Log.v(TAG, "getFavoriteCards (" + (gt() - start) + "ms)");
        return cards;
    }


    public List<Card> getUnlabeledCards() {
        long start = gt();

        List<Card> cards = new ArrayList<Card>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery
                = "select * from " + TBL_CARDS +
                  " where not exists " +
                  "(select * from " + TBL_LINKS_TO_LABELS +
                  " where " +
                  TBL_LINKS_TO_LABELS + "." + LABELLINK_CARD + " = " + TBL_CARDS + "." + KEY_ID +
                  " AND " +
                  TBL_LINKS_TO_LABELS + "." + LABELLINK_CARD + " = " + TBL_CARDS + "." + KEY_ID +
                  ")";

        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                String iv = c.getString(c.getColumnIndex(K_CARD_IV));

                cards.add(new Card(c.getInt(COL_ID),
                                   AesCryptIV.decrypt(mPwd, c.getString(COL_CARD_TITLE), iv),
                                   AesCryptIV.decrypt(mPwd, c.getString(COL_CARD_CREA), iv),
                                   c.getInt(COL_CARD_FAVOR),
                                   new CardIcon(c.getString(COL_CARD_ICON))));
            } while (c.moveToNext());
        }
        c.close();

        Log.v(TAG, "getUnlabeledCards (" + (gt() - start) + "ms)");
        return cards;
    }


    public List<Card> getWeakPasswords() {
        long start = gt();

        List<Integer> pairIds = new ArrayList<Integer>();
        List<Integer> pairLinkIds = new ArrayList<Integer>();
        List<Card> cards = new ArrayList<Card>();
        String selectQuery = "select * from " + TBL_PAIRS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                String iv = c.getString(c.getColumnIndex(K_PAIR_IV));

                String desc = AesCryptIV.decrypt(mPwd, c.getString(c.getColumnIndex(K_PAIR_DES)), iv);
                String value = AesCryptIV.decrypt(mPwd, c.getString(c.getColumnIndex(K_PAIR_VAL)), iv);
                if (isPassword(desc) &&
                    value.length() <= Const.PROGRBAR_PWD_MIN_LENGTH) {
                    // schlechtes Passwort
                    pairIds.add(c.getInt(c.getColumnIndex(KEY_ID)));
                }
            } while (c.moveToNext());
        }
        c.close();

        selectQuery = "select * from " + TBL_LINKS_TO_PAIRS;
        c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                if (pairIds.contains(c.getInt(c.getColumnIndex(KEY_ID)))) {
                    // nur wenn in Id-Liste eingetragen
                    pairLinkIds.add(c.getInt(c.getColumnIndex(PAIRLINK_CARD)));
                }
            } while (c.moveToNext());
        }
        c.close();

        selectQuery = "select * from " + TBL_CARDS;
        c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                if (pairLinkIds.contains(c.getInt(c.getColumnIndex(KEY_ID)))) {

                    String iv = c.getString(c.getColumnIndex(K_CARD_IV));

                    // nur wenn in Id-Liste eingetragen
                    cards.add(new Card(c.getInt(COL_ID),
                                       AesCryptIV.decrypt(mPwd, c.getString(COL_CARD_TITLE), iv),
                                       AesCryptIV.decrypt(mPwd, c.getString(COL_CARD_CREA), iv),
                                       c.getInt(COL_CARD_FAVOR),
                                       new CardIcon(c.getString(COL_CARD_ICON))));
                }
            } while (c.moveToNext());
        }
        c.close();

        Log.v(TAG, "getWeakPasswords (" + (gt() - start) + "ms)");
        return cards;
    }


    private boolean isPassword(String desc) {
        return (desc.equals("Passwort")
                || desc.equals("Password"));
    }


    public void removeCard(String created) {
        setModified();

        SQLiteDatabase db = this.getWritableDatabase();
        String where = K_CARD_CREA + "=" + created;

        db.delete(TBL_CARDS, where, null);
    }


    public void deleteCardInclDataPais(long cardId) {
        setModified();

        List<DataPair> pairs = getDatapairsByCard(cardId);
        for (DataPair pair : pairs) {
            deleteDatapair(pair.getId());
        }

        SQLiteDatabase db = this.getWritableDatabase();
        String where = KEY_ID + "=" + cardId;
        db.delete(TBL_CARDS, where, null);

        where = LABELLINK_CARD + "=" + cardId;
        db.delete(TBL_LINKS_TO_LABELS, where, null);

        where = PAIRLINK_CARD + "=" + cardId;
        db.delete(TBL_LINKS_TO_PAIRS, where, null);
    }


    public void deleteLabel(long Id) {
        setModified();

        SQLiteDatabase db = this.getWritableDatabase();
        String where = KEY_ID + "=" + Id;
        db.delete(TBL_LABELS, where, null);

        where = LABELLINK_LABEL + "=" + Id;
        db.delete(TBL_LINKS_TO_LABELS, where, null);
    }


    public void deleteAccount(long accountId) {
        setModified();

        SQLiteDatabase db = this.getWritableDatabase();
        String where = KEY_ID + "=" + accountId;
        db.delete(TBL_ACCOUNTS, where, null);
    }


    public void deleteAllAccounts() {
        setModified();

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TBL_ACCOUNTS, null, null);
    }


    public void deleteDatapair(Integer datapairId) {
        setModified();

        SQLiteDatabase db = this.getWritableDatabase();
        String where = KEY_ID + "=" + datapairId;

        db.delete(TBL_PAIRS, where, null);
    }


    public void deleteLinksOfLabel(long Id) {
        setModified();

        SQLiteDatabase db = this.getWritableDatabase();

        String where = LABELLINK_LABEL + "=" + Id;
        db.delete(TBL_LINKS_TO_LABELS, where, null);
    }


    /**
     * Entfernt alle Label-Verküpfungen einer bestimmte Karte besitzt.
     */
    public void removeAllLabelLinksToCard(int cardId) {
        setModified();

        SQLiteDatabase db = this.getWritableDatabase();
        String where = LABELLINK_CARD + "=" + cardId;

        db.delete(TBL_LINKS_TO_LABELS, where, null);
    }


    public int[] removeAllDatapairLinks(int cardId) {
        setModified();

        SQLiteDatabase db = this.getWritableDatabase();
        String where = PAIRLINK_CARD + "=" + cardId;

        return new int[]{db.delete(TBL_LINKS_TO_PAIRS, where, null)};
    }


    public void closeDatabase() {
        Log.v(TAG, "closing Database...");
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen()) db.close();
    }


    public void formatDatabase() {
        setModified();

        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TBL_CARDS, null, null);
        db.delete(TBL_PAIRS, null, null);
        db.delete(TBL_LABELS, null, null);
        db.delete(TBL_LINKS_TO_LABELS, null, null);
        db.delete(TBL_LINKS_TO_PAIRS, null, null);
        db.close();
    }


    private long gt() { return System.currentTimeMillis(); }


    public void exportDatabase() {

        File sd = Environment.getExternalStorageDirectory();
        File dir = new File(sd.getAbsolutePath() + "/" + Const.APP_NAME + "/");
        dir.mkdir();

        File data = Environment.getDataDirectory();

        String dbFilePathInDataFolder = "/data/" + Const.PACKAGE_NAME + "/databases/" + Database.DATABASE_NAME;

        File dbFile = new File(data, dbFilePathInDataFolder);
        File backupFile = new File(dir, Database.DATABASE_NAME);

        try {
            FileChannel source, destination;
            source = new FileInputStream(dbFile).getChannel();
            destination = new FileOutputStream(backupFile).getChannel();
            destination.transferFrom(source, 0, source.size());
            source.close();
            destination.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public CryptaAccessor getEncryptedPassword() {
        String pass = "", iv = "";

        String selectQuery = "select * from " + TBL_INFO + " " +
                             "where " + KEY_ID + " = " + "'" + PASSWORD_POSITION + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            pass = c.getString(c.getColumnIndex(INFO_DATA_0));
            iv = c.getString(c.getColumnIndex(INFO_DATA_1));
        }

        return new CryptaAccessor(pass, iv);
    }


    public CryptaAccessor getPasswordTestData() {
        String pass = "", iv = "";

        String selectQuery = "select * from " + TBL_INFO + " " +
                             "where " + KEY_ID + " = " + "'" + PASSWORD_TEST_DATA_POSITION + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            pass = c.getString(c.getColumnIndex(INFO_DATA_0));
            iv = c.getString(c.getColumnIndex(INFO_DATA_1));
        }

        return new CryptaAccessor(pass, iv);
    }

}

