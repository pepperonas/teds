package com.pepperonas.teds.model;

import com.pepperonas.teds.utils.Logic;

/**
 * Created by pepperonas on 23.02.15.
 * Lable
 */
public class Label {

    private int id;
    private String name;
    private String icon;
    private String created;
    private String iv;

    private int size;


    public Label() {
        this.created = Logic.getTimeForId();
    }


    public Label(int id, String name, String iv) {
        this.id = id;
        this.name = name;
        this.created = Logic.getTimeForId();
        this.iv = iv;
    }


    public Label(int id, String name, String created, String icon, String iv) {
        this.id = id;
        this.name = name;
        this.created = created;
        this.icon = icon;
        this.iv = iv;
    }


    public int getId() { return id; }


    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return this.name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getIcon() {
        return icon;
    }


    public void setIcon(String icon) {
        this.icon = icon;
    }


    public String getCreated() {
        return created;
    }


    public void setCreated(String created) {
        this.created = created;
    }


    public int getSize() { return size; }


    public void setSize(int size) { this.size = size; }


    public String getIV() {
        return iv;
    }

}
