package com.pepperonas.teds.model;

import com.pepperonas.teds.utils.Logic;

/**
 * Created by pepperonas on 06.02.15.
 * DataPair - Datensatz umfasst Beschreibung und (Login-, Passwort-, etc. )Wert.
 */
public class DataPair {

    private int id;

    private String created;
    private String description;
    private String value;


    public DataPair() {
        this.created = Logic.getTimeForId();
    }


    public DataPair(String description, String value) {
        this.description = description;
        this.value = value;
        this.created = Logic.getTimeForId();
    }


    public DataPair(int id, String description, String value) {
        this.id = id;
        this.description = description;
        this.value = value;
        this.created = Logic.getTimeForId();
    }


    public int getId() { return id; }


    public void setId(int id) { this.id = id; }


    public String getDescription() { return description; }


    public void setDescription(String description) {
        this.description = description;
    }


    public String getValue() { return value; }


    public String getCreated() {
        return created;
    }


    public void setCreated(String created) {
        this.created = created;
    }


    public void setValue(String value) {
        this.value = value;
    }


}
