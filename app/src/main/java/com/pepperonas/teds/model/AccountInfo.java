package com.pepperonas.teds.model;

import com.pepperonas.teds.utils.Logic;

/**
 * Created by pepperonas on 04/17/2015.
 * Account
 */
public class AccountInfo {

    private int id;
    private String name;
    private String created;
    private String iv;


    public AccountInfo(int id, String name, String iv) {
        this.id = id;
        this.name = name;
        this.created = Logic.getTimeForId();
        this.iv = iv;
    }


    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public String getIv() {
        return iv;
    }


    public void setIv(String iv) {
        this.iv = iv;
    }


    public String getCreated() {
        return created;
    }


    public void setCreated(String created) {
        this.created = created;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }
}
