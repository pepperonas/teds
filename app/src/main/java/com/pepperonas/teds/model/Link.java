package com.pepperonas.teds.model;

/**
 * Created by pepperonas on 03/15/2015.
 * Link
 */
public class Link {


    private int id;
    private long linkA, linkB;


    public Link() {

    }


    public Link(int id, long linkA, long linkB) {
        this.id = id;
        this.linkA = linkA;
        this.linkB = linkB;
    }


    public long getLinkA() {
        return linkA;
    }


    public void setLinkA(long linkA) {
        this.linkA = linkA;
    }


    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public long getLinkB() {
        return linkB;
    }


    public void setLinkB(long linkB) {
        this.linkB = linkB;
    }



}
