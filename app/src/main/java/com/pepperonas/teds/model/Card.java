package com.pepperonas.teds.model;

import com.pepperonas.teds.utils.Logic;

/**
 * Created by pepperonas on 09.03.15.
 * Card
 */
public class Card {

    public static final boolean IS_NOT_FAVORITE = false;
    public static final boolean IS_FAVORITE = true;

    private int id;
    private String title;
    private boolean favorite;
    private String created;
    /*private String icon;*/

    private CardIcon cardIcon;


    public Card() {
        this.created = Logic.getTimeForId();
    }


    public Card(int id, String title, String created, int favorite, CardIcon cardIcon) {
        this.id = id;
        this.title = title;
        this.created = created;
        this.favorite = favorite == 1;
        this.cardIcon = cardIcon;
    }


    public Card(String title, boolean favorite, CardIcon cardIcon) {
        this.title = title;
        this.favorite = favorite;
        this.cardIcon = cardIcon;
        this.created = Logic.getTimeForId();
    }


    public Card(int id, String title, boolean favorite, CardIcon cardIcon) {
        this.id = id;
        this.title = title;
        this.favorite = favorite;
        this.cardIcon = cardIcon;
        this.created = Logic.getTimeForId();
    }


    public void setId(int id) {
        this.id = id;
    }


    public int getId() {
        return id;
    }


    public String getCreated() { return created; }


    public void setCreated(String created) {
        this.created = created;
    }


    public String getRawIcon() {
        return cardIcon.toString();
    }


    public void setIcon(String icon) {
        this.cardIcon = new CardIcon(icon);
    }


    public boolean isFavorite() {
        return favorite;
    }


    public void setFavorite(int favorite) {
        this.favorite = (favorite == 1);
    }


    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }


    public String getTitle() {
        return title;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    public CardIcon getCardIcon() {
        return cardIcon;
    }


    public void setCardIcon(CardIcon cardIcon) {
        this.cardIcon = cardIcon;
    }
}
