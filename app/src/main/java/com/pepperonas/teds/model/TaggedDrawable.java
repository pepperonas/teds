package com.pepperonas.teds.model;

import android.graphics.drawable.Drawable;

import com.mikepenz.iconics.IconicsDrawable;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class TaggedDrawable {

    private IconicsDrawable iconicsDrawable;
    private Drawable drawable;
    private String tag = "";


    public TaggedDrawable(IconicsDrawable iconicsDrawable, String tag) {
        this.iconicsDrawable = iconicsDrawable;
        this.tag = tag;
    }


    public TaggedDrawable(Drawable drawable, String tag) {
        this.drawable = drawable;
        this.tag = tag;
    }


    public IconicsDrawable getIconincsDrawable() {
        return iconicsDrawable;
    }


    public String getTag() {
        return tag;
    }

    public Drawable getDrawable() {
        return drawable;
    }
}
