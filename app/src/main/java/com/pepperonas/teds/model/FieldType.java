package com.pepperonas.teds.model;

/**
 * Created by pepperonas on 07.02.15.
 * FieldType
 */
public class FieldType {

    // TODO: neu sortieren
    public static final int
            TYPE_LOGIN = 0,
            TYPE_PASSWORD = 1,
            TYPE_MAIL = 2,
            TYPE_URL = 3,
            TYPE_TEXT = 4,
            TYPE_NUMBER = 5,
            TYPE_PHONE = 6,
            TYPE_PIN = 7,
            USERS_CUSTOM = 8;

}
