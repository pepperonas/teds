package com.pepperonas.teds.model;

import com.pepperonas.jbasx.div.MaterialColor;
import com.pepperonas.teds.utils.Const;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class CardIcon {

    public enum SymbolColor {
        WHITE(0), BLACK(1);


        SymbolColor(int i) {

        }
    }


    private String color = "";

    private String symbol = "";
    private SymbolColor symbolColor = SymbolColor.WHITE;


    public CardIcon(String icon) {
        this.color = icon.split(Const.ICON_DIVIDER)[0];
        this.symbol = icon.split(Const.ICON_DIVIDER)[1];
        this.symbolColor = (String.valueOf(icon.split(Const.ICON_DIVIDER)[2]).equals(SymbolColor.BLACK.toString())) ? SymbolColor.BLACK : SymbolColor.WHITE;
    }


    public CardIcon(String color, String symbol, SymbolColor symbolColor) {
        this.color = color;
        this.symbol = symbol;
        this.symbolColor = symbolColor;
    }


    public String toString() {
        return color + Const.ICON_DIVIDER + symbol + Const.ICON_DIVIDER + symbolColor;
    }


    public boolean ensureBackground() {
        return !this.color.contains(Const.ICON_UNSET);
    }


    public boolean ensureSymbol() {
        return !this.symbol.contains(Const.ICON_UNSET);
    }


    public String getColor() {
        return color;
    }


    public void setColor(String color) {
        this.color = color;
    }


    public String getSymbol() {
        return symbol;
    }


    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }


    public SymbolColor getSymbolColor() {
        return symbolColor;
    }


    public void setSymbolColor(SymbolColor symbolColor) {
        this.symbolColor = symbolColor;
    }


    public static String getUnset() {
        return new CardIcon(Const.ICON_UNSET + Const.ICON_DIVIDER + Const.ICON_UNSET + Const.ICON_DIVIDER + CardIcon.SymbolColor.WHITE).toString();
    }


    public static String replaceTagged(String symbol) {
        return symbol.replace(Const.SYMB_TAGGD, "");
    }


    public static String convertSymbolColor(CardIcon.SymbolColor symbolColor) {
        if (symbolColor == CardIcon.SymbolColor.BLACK) {
            return MaterialColor.BLACK;
        } else return MaterialColor.WHITE;
    }

}
