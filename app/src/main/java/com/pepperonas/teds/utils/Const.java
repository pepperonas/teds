package com.pepperonas.teds.utils;

/**
 * Created by pepperonas on 04.02.15.
 * KONSTANTE
 */
public final class Const {

    /* GENERAL */
    public static final String PACKAGE_NAME = "com.pepperonas.teds";
    public static final String APP_NAME = "teds";

    /*
     * DEBUG
     */
    public static final boolean FILL_LIST = false;
    public static final boolean BYPASS_LOGIN = false;
    public static final String BYPASS_PW = "";

    public static final int MAX_AMOUNT_CARDS_IN_FREE_VERSION = 50;

    public static final int MIN_ACTIVITY_TRANSITION_TIME_MS = 650;

    public static final int TOUCH_OFFSET_CARD_LIST_FAV_STAR = 20;

    public static final int MAXWORDLNGT_FOR_LRGE_TITL_IN_SHOWCARD = 17;

    public static final int DIALOG_ICON_SIZE = 24;

    /*
     * CONFIG & SETUP
     */
    public static final String DATA_FILE = "data";
    public static final String PR_MASTR_PWD = "crypto_base";
    public static final String PR_MASTR_PWD_ACC = "cyrpto_crtl";
    public static final String PR_FVALUE = "crypto_adv";
    public static final String PR_FVALUE_IV = "crypto_adv_iv";
    public static final String PR_FGOOGLEVALUE = "crypto_google_adv";
    public static final String PR_FGOOGLEVALUE_IV = "crypto_google_adv_iv";

    public static final String LBC_INTENT_TO_FINISH = "make_finish";
    public static final String LBC_MESSAGE = "message";

    public static final String PR_FIRST_START = "first_start";
    public static final String SOME_DEFAULT_X = "x";

    // Fixe Label (erkennt App eigenständig durch Logik)
    public static final int ALL_CARDS = 0;
    public static final int FAVORITES = 1;
    public static final int UNLABELED = 2;
    public static final int WEAK_PASS = 3;

    // Flexible Label (können vom Nutzer gelöscht werden)
    public static final int IMPORTANT = 0;
    public static final int ONLINE = 1;
    public static final int SHOP = 2;
    public static final int MONEY = 3;

    public static final int MIN_MASTER_PWD_LENGTH = 0;
    public static final int PROGRBAR_PWD_MIN_LENGTH = 6;

    public static final int TEXT_SIZE_SLIDER_UNPRESSED = 16;
    public static final int TEXT_SIZE_SLIDER_PRESSED = 20;

    public static final int NAVDRAWER_CLOSING_DELAY = 130;

    /*
     * BUNDLE
     */
    public static final String BNL_PW = "p";

    public static final int BNL_OVERVIEW_MODE_ALL = 0;
    public static final String BNL_FAVORITE_OBJECT_ID = "fav_object_id";

    public static final String BNL_CARD_ID = "card_id";
    public static final String BNL_FROM_MAIN = "from_main";

    public static final String BNL_SEARCH_ID = "search_id";
    public static final String SEARCH_MASK = "_s_e_a_r_c_h_";

    public static final String BNL_CARD_EDIT_MODE = "card_edit_mode";
    public static final String BNL_CARD_PRESET_MODE = "card_preset_mode";
    public static final String BNL_SHOW_ANIMATION = "show_animation";
    public static final String BNL_RESTART_PREF = "restart";

    public static final String BNL_PRESET_MAIL = "preset_mail";
    public static final String BNL_PRESET_NOTE = "preset_note";
    public static final String BNL_PRESET_ACCOUNT = "preset_account";
    public static final String BNL_PRESET_CODE = "preset_code";
    public static final String BNL_PRESET_PGP = "preset_pgp";
    public static final String BNL_PRESET_PIN = "preset_pin";

    public static final String BNL_COPY_CLIPBOARD = "copied_clipbrd_login";
    public static final int NOTIFICATION_LOGIN = 1001;
    public static final int NOTIFICATION_PWD = 1002;

    public static final String OVERVIEW_MODE = "mode";

    /*
     * RESULT CODES
     */
    public static final int SOME_RESULT = 11;
    public static final int ADD_REQUEST = 69;

    public static final int CHANGE_THEME_REQUEST_CODE = 44;
    public static final int RESULT_THEME_CHANGED = 910;

    /*
     * SPINNER - PASSWORD GENERATION MODE
     */
    public static final int GEN_KEEP_IN_MIND = 0;
    public static final int GEN_PARANOID = 1;
    public static final int GEN_NO_SPECIAL_CHARS = 2;
    public static final int GEN_ONLY_NUMBERS = 3;
    public static final int GEN_ONLY_CHARS = 4;

    /*
     * EXPORT
     */
    public static final String EXPORT_DIVIDER = "-------";
    public static final String EXPORT_FILE_NAME = "DELETE_ME";
    public static final String OVERWRITE_EXPORTED_FILE = "";

    public static final String SHOW_ICON_HINT = "show_icon_hint";
    public static final String SHOW_LABEL_EDITOR_HINT = "show_lable_editor_hint";
    public static final String SHOW_SWIPE_IN_ICON_DIALOG_HINT = "show_swipe_hint";
    public static final String SHOW_ACCOUNT_MANAGER_HINT = "show_account_manager_hint";

    public static final String BNL_FAVORITE_OBJECT_IV = "lable_iv";

    public static final String TEST_PWD_SOME_VALUE = "check123";

    public static final String ICON_DIVIDER = "-SP-";
    public static final String ICON_UNSET = "-LOCKED-";

    public static final float SCALE_FAB_CONTENT_IN_SHOWCARD = .7f;
    public static final String LBC_ICON_CHANGED = "icon_changed";
    public static final String ANALYTICS_ID = "UA-58002990-6";

    public static final int RESOLVE_CONNECTION_REQUEST_CODE = 81;

    public static final long DELAY_ON_BACK_PRESSED = 2000;
    public static final boolean TOUCH_TWICE_TO_EXIT = true;
    public static final String ICON_CHANGED_TO_ICONIC = "<ICI>";
    public static final String ICON_CHANGED_TO_CUSTOM = "<ICC>";
    public static final String COLOR_CHANGED = "<CC>";
    public static final CharSequence SYMB_TAGGD = "tagged_";
    public static final CharSequence SYMB_FAW = "faw_";
    public static final String TB_P1_COLOR = "tb_p1";
    public static final String TB_P2_COLOR = "tb_p2";
    public static final String FONT_HELVETICA_NEU_CONDENSED = "fonts/helveticaneue_con.ttf";
}
