package com.pepperonas.teds.utils;

import com.pepperonas.jbasx.format.NumberFormatUtils;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class TimeHelper {

    public static long convertSystemStamp() {
        return NumberFormatUtils.removeLastDigits(System.currentTimeMillis(), 3);
    }

}
