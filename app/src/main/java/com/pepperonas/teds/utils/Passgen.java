package com.pepperonas.teds.utils;

import java.util.Random;

/**
 * Created by pepperonas on 04/10/2015.
 * Passgen
 */
public class Passgen {

    private static final int PASSWORD_LENGTH = 256;

    private static Random mRandomGenerator;


    public static String generatePassword() {
        mRandomGenerator = new Random();
        String password = "";
        Character[] pwdChars = new Character[PASSWORD_LENGTH];
        for (int i = 0; i < PASSWORD_LENGTH; ) {
            int character = getRndInt(93, 33);
            char[] c = Character.toChars(character);
            pwdChars[i] = c[0];
            i++;
        }
        for (int i = 0; i < PASSWORD_LENGTH; i++) {
            password += pwdChars[i];
        }
        return password;
    }


    private static int getRndInt(int amount, int offset) {

        return mRandomGenerator.nextInt(amount) + offset;
    }

}
