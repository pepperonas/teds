package com.pepperonas.teds.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Base64;

import com.pepperonas.jbasx.io.IoUtils;
import com.pepperonas.teds.R;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by pepperonas on 05/02/2015.
 * RetrieveHttpTask
 */
public class HttpTask extends AsyncTask<String, Void, String> {

    private static final String TAG = "HttpTask";

    private Context mContext;
    private ProgressDialog mProgressDialog;


    public HttpTask(Context ctx) {
        mContext = ctx;
        mProgressDialog = new ProgressDialog(mContext);
    }


    @Override
    protected void onPreExecute() {
        mProgressDialog.setMessage(mContext.getString(R.string.progress_load_key));
        mProgressDialog.show();
    }


    @Override
    protected String doInBackground(String... params) {

        URL url;

        String text = "";
        InputStream is;
        try {
            url = new URL("http://ccmp.kochab.uberspace.de/teds/validate.php");
            URLConnection urlConnection = url.openConnection();
            urlConnection.connect();

            is = new BufferedInputStream(url.openStream(), 8192);
            text = IoUtils.convertStreamToString(is);

            text = decodePurchaseKey(text);

            is.close();
        } catch (IOException e) {

            e.printStackTrace();
        }

        return text;

    }


    @Override
    protected void onPostExecute(String result) {
        if (mProgressDialog != null) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        }
        super.onPostExecute(result);
    }


    public static String decodePurchaseKey(String arg) {
        byte[] data = Base64.decode(arg, Base64.DEFAULT);
        String result = null;
        try {
            result = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }


    public static String encodePurchaseKey(String arg) {
        byte[] data = new byte[0];
        try {
            data = arg.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Base64.encodeToString(data, Base64.DEFAULT);
    }

}