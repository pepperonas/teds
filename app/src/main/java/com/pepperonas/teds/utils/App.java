package com.pepperonas.teds.utils;

import android.app.Application;

import com.pepperonas.aesprefs.AesPrefs;
import com.pepperonas.andbasx.AndBasx;
import com.pepperonas.teds.R;

import java.util.Timer;
import java.util.TimerTask;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by pepperonas on 26.02.15.
 * MyApplication
 */
public class App extends Application {

    private static final String TAG = "App";

    private Timer mActivityTransitionTimer;
    private TimerTask mActivityTransitionTimerTask;

    public boolean mWasInBackground;


    @Override
    public void onCreate() {
        super.onCreate();

        AesPrefs.init(this, "app_config", "w!x891##+e((EWa~?XDsAwä%G85-*", AesPrefs.LogMode.ALL);
        AndBasx.init(this);

        CalligraphyConfig.initDefault                          (
                new CalligraphyConfig.Builder()
                        .setDefaultFontPath(Const.FONT_HELVETICA_NEU_CONDENSED)
                        .setFontAttrId(R.attr.fontPath).build());

    }


    public void startActivityTransitionTimer(int minutes) {
        long sessionTime = (minutes * 1000 * 60) + Const.MIN_ACTIVITY_TRANSITION_TIME_MS;
        this.mActivityTransitionTimer = new Timer();
        this.mActivityTransitionTimerTask = new TimerTask() {
            public void run() {
                App.this.mWasInBackground = true;
            }
        };
        this.mActivityTransitionTimer.schedule(mActivityTransitionTimerTask, sessionTime);
    }


    public void stopActivityTransitionTimer() {
        if (this.mActivityTransitionTimerTask != null) {
            this.mActivityTransitionTimerTask.cancel();
        }

        if (this.mActivityTransitionTimer != null) {
            this.mActivityTransitionTimer.cancel();
        }

        this.mWasInBackground = false;
    }

}
