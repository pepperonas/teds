package com.pepperonas.teds.utils;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by pepperonas on 25.02.15.
 * AESCryptIV
 */
public class AesCryptIV {


    public static String encrypt(String mP, String plainText, String ivlong) {

        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Cipher cipher = null;
        SecretKeySpec key;
        AlgorithmParameterSpec spec;

        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (digest != null) digest.update(mP.getBytes());
        byte[] keyBytes = new byte[32];
        if (digest != null) System.arraycopy(digest.digest(), 0, keyBytes, 0, keyBytes.length);

        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        } catch (Exception e) {
            e.printStackTrace();
        }
        key = new SecretKeySpec(keyBytes, "AES");

        ByteBuffer bb = ByteBuffer.allocate(16);
        bb.putLong(0, Long.parseLong(ivlong));
        byte[] iv = bb.array();

        spec = new IvParameterSpec(iv);

        try {
            if (cipher != null) {
                cipher.init(Cipher.ENCRYPT_MODE, key, spec);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        byte[] encrypted = new byte[0];
        try {
            if (cipher != null) {
                encrypted = cipher.doFinal(plainText.getBytes("UTF-8"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String encryptedText = null;
        try {
            encryptedText = new String(Base64.encode(encrypted, Base64.NO_WRAP), "UTF-8");
        } catch (Exception e) {

            e.printStackTrace();
        }

        return encryptedText;
    }


    public static String decrypt(String password, String cryptedText, String savedIV) {
        Cipher cipher = null;
        SecretKeySpec key;
        AlgorithmParameterSpec spec;
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (digest != null) digest.update(password.getBytes());
        byte[] keyBytes = new byte[32];
        if (digest != null) System.arraycopy(digest.digest(), 0, keyBytes, 0, keyBytes.length);

        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        } catch (Exception e) {
            e.printStackTrace();
        }
        key = new SecretKeySpec(keyBytes, "AES");


        ByteBuffer bb = ByteBuffer.allocate(16);
        bb.putLong(0, Long.parseLong(savedIV));
        byte[] iv = bb.array();

        spec = new IvParameterSpec(iv);

        try {
            if (cipher != null) {
                cipher.init(Cipher.DECRYPT_MODE, key, spec);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        byte[] bytes = Base64.decode(cryptedText, Base64.DEFAULT);

        byte[] decrypted = new byte[0];
        try {
            if (cipher != null) {
                decrypted = cipher.doFinal(bytes);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String decryptedText = null;
        try {
            decryptedText = new String(decrypted, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return decryptedText;
    }

}
