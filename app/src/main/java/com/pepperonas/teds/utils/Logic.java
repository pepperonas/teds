package com.pepperonas.teds.utils;

import android.accounts.Account;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.PowerManager;
import android.os.SystemClock;
import android.preference.Preference;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.mikepenz.iconics.IconicsDrawable;
import com.pepperonas.andbasx.base.ToastUtils;
import com.pepperonas.andbasx.graphic.DrawableCircleLetter;
import com.pepperonas.jbasx.color.ColorUtils;
import com.pepperonas.teds.R;
import com.pepperonas.teds.activities.LoginActivity;
import com.pepperonas.teds.model.CardIcon;
import com.pepperonas.teds.settings.Pref;
import com.pepperonas.teds.settings.SettingsActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public final class Logic {

    private static final String TAG = "Logic";


    public static void ensureHideFromRecents(Activity activity) {
        SharedPreferences prefs = activity.getSharedPreferences(
                Pref.CONFIG_FILE, Context.MODE_PRIVATE         );

        if (prefs.getBoolean(Pref.PK_CB_HIDE_RECENTS, true)) {
            activity.getWindow().setFlags                 (
                    WindowManager.LayoutParams.FLAG_SECURE,
                    WindowManager.LayoutParams.FLAG_SECURE);
        }
    }


    public static int getIconColorForSettings(Context ctx) {
        return Logic.isDarkTheme(ctx) ? R.color.settingsIcons_darkTheme : R.color.settingsIcons;
    }


    public static int getIconColorForDialog(Context ctx) {
        return Logic.isDarkTheme(ctx) ? R.color.settingsIcons_darkTheme : R.color.settingsIcons;
    }


    public static boolean isInternetConnected(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }


    public static boolean isSecretField(Context ctx, String description) {
        return description.toLowerCase().contains(ctx.getString(R.string.password).toLowerCase())
               || description.toLowerCase().contains(ctx.getString(R.string.code).toLowerCase())
               || description.toLowerCase().contains(ctx.getString(R.string.pin).toLowerCase())
               || description.toLowerCase().contains("password".toLowerCase())
               || description.toLowerCase().contains("passwort".toLowerCase());
    }


    public static boolean isUrlField(Context ctx, String description) {
        return description.toLowerCase().contains(ctx.getString(R.string.url).toLowerCase());
    }


    public static void ensureClearClipboard(Context ctx, final ClipboardManager clipboard) {
        SharedPreferences prefs = ctx.getSharedPreferences(
                Pref.CONFIG_FILE, Context.MODE_PRIVATE    );

        int duration = Integer.parseInt                                (
                prefs.getString(Pref.PK_LIST_AUTO_CLEAR_CLIPBOARD, "1"));

        Log.i("Logic", "PK_LIST_AUTO_CLEAR_CLIPBOARD: " + duration);

        if (duration != 0) {
            Timer mTimer = new Timer();
            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    clipboard.setPrimaryClip                           (
                            new ClipData(ClipData.newPlainText("", "")));

                }
            }, duration * 1000 * 60);
        }
    }


    public static boolean ensureWritePref(Context ctx, String prefName, String defaultValue) {
        SharedPreferences prefs = ctx.getSharedPreferences(Const.DATA_FILE, Context.MODE_PRIVATE);

        if (prefs.contains(prefName)) return true;
        else {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(prefName, defaultValue).apply();
            return false;
        }
    }


    /**
     * Zeigt den Login-Bildschirm an.
     */
    public static boolean ensureShowLoginScreen(Activity activity) {
        App myApp = (App) activity.getApplication();
        if (myApp.mWasInBackground) {
            activity.startActivity(new Intent(activity, LoginActivity.class));
        }
        myApp.stopActivityTransitionTimer();
        return myApp.mWasInBackground;
    }


    public static void ensureShowLoginIfLocked(Context ctx) {
        PowerManager powerManager = (PowerManager) ctx.getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn;
        if (Build.VERSION.SDK_INT >= 20) isScreenOn = powerManager.isInteractive();
        else isScreenOn = powerManager.isScreenOn();
        if (!isScreenOn && Logic.isDirectLockEnabled(ctx)) {
            ctx.startActivity(new Intent(ctx, LoginActivity.class));
        }
    }


    /**
     * Lädt die passende Sperr-Konfiguration und wendet diese auf die App an.
     */
    public static void ensureEnableLockTimer(Activity activity, Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(Pref.CONFIG_FILE, Context.MODE_PRIVATE);
        boolean isDirectLock = prefs.getBoolean(Pref.PK_CB_DIRECT_LOCK, true);
        int delayedLock = Integer.parseInt(prefs.getString(Pref.PK_LIST_DELAYED_LOCK, Pref.DELAYED_LOCK_DEFAULT));

        // nicht sperren
        if (delayedLock == 0 && !isDirectLock) return;

        // sofort sperren ("Mindest-Transistion-Zeit" in MyApplication beachten)
        if (isDirectLock) {
            ((App) activity.getApplication()).startActivityTransitionTimer(0);
        } else {
            // nach Zeit, die in DelayedLock-Liste gewählt ist, sperren.
            ((App) activity.getApplication()).startActivityTransitionTimer(delayedLock);
        }
    }


    public static int generateInt(int amount, int offset) {
        Random random = new Random();
        return random.nextInt(amount) + offset;
    }


    public static void applyLengthCheck(final EditText et, final MaterialDialog materialDialog) {
        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }


            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }


            @Override
            public void afterTextChanged(Editable s) {
                checkEditTextLength(et.getText(), materialDialog);
            }
        });
    }


    private static void checkEditTextLength(Editable s, MaterialDialog materialDialog) {

        if (s.length() > Const.MIN_MASTER_PWD_LENGTH) {
            materialDialog.getActionButton(DialogAction.POSITIVE).setClickable(true);
            materialDialog.getActionButton(DialogAction.POSITIVE).setEnabled(true);
        }

        if (s.length() <= Const.MIN_MASTER_PWD_LENGTH + 1) {
            materialDialog.getActionButton(DialogAction.POSITIVE).setClickable(false);
            materialDialog.getActionButton(DialogAction.POSITIVE).setEnabled(false);
        }
    }


    public static boolean isAccountInfoUseful(ArrayList<String> accountList, Account a) {
        return !accountList.contains(a.name)
               && a.name.contains("@");
    }


    public static String getTimeForId() {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();

        SystemClock.sleep(1);

        return gregorianCalendar.get(Calendar.YEAR)
               + String.format("%02d", gregorianCalendar.get(Calendar.MONTH))
               + String.format("%02d", gregorianCalendar.get(Calendar.DAY_OF_MONTH))
               + String.format("%02d", gregorianCalendar.get(Calendar.HOUR_OF_DAY))
               + String.format("%02d", gregorianCalendar.get(Calendar.MINUTE))
               + String.format("%02d", gregorianCalendar.get(Calendar.SECOND))
               + String.format("%04d", gregorianCalendar.get(Calendar.MILLISECOND));
    }


    public static String calculateStrength(Context ctx, String password) {
        int currentScore = 0;
        boolean sawUpper = false, sawLower = false, sawDigit = false, sawSpecial = false;

        if (password.length() <= 6) return ctx.getString(R.string.password_strength_very_weak);
        if (password.length() <= 8) return ctx.getString(R.string.password_strength_weak);

        if (password.length() > 12) currentScore += 1;

        for (int i = 0; i < password.length(); i++) {
            char c = password.charAt(i);
            if (!sawSpecial && !Character.isLetterOrDigit(c)) {
                currentScore += 1;
                sawSpecial = true;
            } else if (!sawDigit && Character.isDigit(c)) {
                currentScore += 1;
                sawDigit = true;
            } else if (!sawUpper || !sawLower) {
                if (Character.isUpperCase(c)) sawUpper = true;
                else sawLower = true;
                if (sawUpper && sawLower) currentScore += 1;
            }
        }

        if (password.length() >= 18) currentScore += 1;
        if (password.length() >= 26) currentScore += 1;

        switch (currentScore) {
            case 0:
                return ctx.getString(R.string.password_strength_very_weak);
            case 1:
                return ctx.getString(R.string.password_strength_weak);
            case 2:
                return ctx.getString(R.string.password_strength_medium);
            case 3:
                return ctx.getString(R.string.password_strength_strong);
            case 4:
                return ctx.getString(R.string.password_strength_very_strong);
            case 5:
                return ctx.getString(R.string.password_strength_extreme_strong);
            case 6:
                return ctx.getString(R.string.password_strength_gutmann);
            default:
                return ctx.getString(R.string.password_strength_very_weak);
        }
    }


    public static void applyTheme(Context ctx) {
        final SharedPreferences prefs = ctx.getSharedPreferences(Pref.CONFIG_FILE, Context.MODE_PRIVATE);
        if (prefs.getString(Pref.PK_LIST_THEME, Pref.DEFAULT_THEME).equals(Pref.LIGHT))
            ctx.setTheme(R.style.AppTheme);
        else ctx.setTheme(R.style.AppTheme_Dark);
    }


    public static void themeToolbar(Context ctx, Toolbar toolbar, RelativeLayout toolbarExtension) {
        final SharedPreferences prefs = ctx.getSharedPreferences(
                Pref.CONFIG_FILE, Context.MODE_PRIVATE          );

        toolbar.setTitle("");

        if (prefs.getString(Pref.PK_LIST_THEME, Pref.DEFAULT_THEME).equals(Pref.LIGHT)) {
            toolbar.setBackgroundColor(ctx.getResources().getColor(R.color.primaryColor));

            if (toolbarExtension != null) {
                toolbarExtension.setBackgroundColor(ctx.getResources().getColor(R.color.primaryColor));
            }

        } else {
            toolbar.setBackgroundColor(ctx.getResources().getColor(R.color.primaryColor_darkTheme));

            if (toolbarExtension != null) {
                toolbarExtension.setBackgroundColor                             (ctx.getResources().getColor(
                        R.color.primaryColor_darkTheme                         ));
            }
        }
    }


    public static boolean isDarkTheme(Context ctx) {
        final SharedPreferences prefs = ctx.getSharedPreferences(
                Pref.CONFIG_FILE, Context.MODE_PRIVATE          );

        return !prefs.getString(Pref.PK_LIST_THEME, Pref.DEFAULT_THEME).equals(Pref.LIGHT);
    }


    /**
     * Returns density independent pixel
     */
    public static int mkDiP(Context ctx, int pixel) {
        DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();
        return (int) (metrics.density * pixel);
    }


    public static Drawable scaleFabContent(Context ctx, Drawable toShrink, float v) {
        Bitmap b = ((BitmapDrawable) toShrink).getBitmap();
        Bitmap resized = Bitmap.createScaledBitmap(b, (int) (b.getWidth() * v), (int) (b.getHeight() * v), false);
        return new BitmapDrawable(ctx.getResources(), resized);
    }


    public static Drawable loadBackground(Context ctx, String color) {
        Drawable drawable = ctx.getResources().getDrawable(R.drawable.circle_white);

        if (color.length() == 10) {
            color = color.substring(0, 8);
        }
        String colorToParse = ColorUtils.hexToHtml(color);
        drawable.setColorFilter(Color.parseColor(colorToParse), PorterDuff.Mode.SRC_ATOP);
        return drawable;
    }


    public static Drawable loadIconics(Context ctx, String symbol, int size, String symbolColor) {
        return new IconicsDrawable(ctx, symbol).color(ColorUtils.toInt(symbolColor)).sizeDp(size);
    }


    public static Drawable loadTextDrawable(String symbol, int size, String symbolColor) {
        symbol = CardIcon.replaceTagged(symbol);
        return new DrawableCircleLetter.Builder(size, "#00FFFFFF", symbol).textColor(symbolColor).build();
    }


    public static boolean isDarkColor(int color) {
        if (android.R.color.transparent == color) return true;
        int[] rgb = {Color.red(color), Color.green(color), Color.blue(color)};
        int brightness = (int) Math.sqrt(
                rgb[0] * rgb[0] * .241 +
                rgb[1] * rgb[1] * .691 +
                rgb[2] * rgb[2] * .068  );
        // color is dark
        if (brightness <= 40) return true;

        // color is light
        return true;
    }


    public static boolean isDirectLockEnabled(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(
                Pref.CONFIG_FILE, Context.MODE_PRIVATE    );

        return prefs.getBoolean(Pref.PK_CB_DIRECT_LOCK, true);
    }


    public static void ensureShowHint(Context ctx, String nameOfHint, String msg, int howOften) {
        SharedPreferences prefs = ctx.getSharedPreferences(
                Const.DATA_FILE, Context.MODE_PRIVATE     );

        SharedPreferences.Editor editor = prefs.edit();

        if (!prefs.contains(nameOfHint)) editor.putInt(nameOfHint, 0);

        int amount = prefs.getInt(nameOfHint, 0);
        if (amount < howOften) {
            ToastUtils.toastLong(msg);
            amount += 1;
            editor.putInt(nameOfHint, amount);
        }

        editor.apply();
    }


    public static void resetHints(final SettingsActivity act) {
        Preference resetHints = act.findPreference(act.getString(R.string.pk_reset_hints));
        resetHints.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                SharedPreferences prefs = act.getSharedPreferences(
                        Const.DATA_FILE, Context.MODE_PRIVATE     );

                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt(Const.SHOW_ICON_HINT, 0);
                editor.putInt(Const.SHOW_LABEL_EDITOR_HINT, 0);
                editor.putInt(Const.SHOW_SWIPE_IN_ICON_DIALOG_HINT, 0);
                editor.apply();

                ToastUtils.toastShort(R.string.toast_hints_reset_done);
                return true;
            }
        });
    }


    public static String getIV() {
        return String.valueOf(System.currentTimeMillis());
    }


    public static String getDataPath(Context cxt) {
        return cxt.getFilesDir().getPath();
    }

}