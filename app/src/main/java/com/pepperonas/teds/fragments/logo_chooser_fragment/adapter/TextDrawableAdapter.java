package com.pepperonas.teds.fragments.logo_chooser_fragment.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.pepperonas.andbasx.graphic.DrawableSquareLetter;
import com.pepperonas.andbasx.system.DeviceUtils;
import com.pepperonas.jbasx.color.ColorUtils;
import com.pepperonas.jbasx.div.MaterialColor;
import com.pepperonas.teds.model.TaggedDrawable;
import com.pepperonas.teds.interfaces.IconChangedListener;
import com.pepperonas.teds.utils.Const;
import com.pepperonas.teds.utils.Logic;
import com.pepperonas.teds.utils.TextDrawable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class TextDrawableAdapter extends BaseAdapter {

    private static final String TAG = "TextDrawableAdapter";
    public static final int ICON_SIZE = 48;
    private List<TaggedDrawable> icons = new ArrayList<TaggedDrawable>();

    private IconChangedListener mIconChangedListener;

    private final Context mCtx;


    void registerListener(IconChangedListener listener) {
        mIconChangedListener = listener;
    }


    public TextDrawableAdapter(Context context) {
        mCtx = context;

        registerListener((IconChangedListener) context);

        int c_col = ColorUtils.toInt(MaterialColor.GREY_800);
        int t_col = ColorUtils.toInt(MaterialColor.WHITE);
        if (Logic.isDarkTheme(mCtx)) {
            c_col = ColorUtils.toInt(MaterialColor.GREY_300);
            t_col = ColorUtils.toInt(MaterialColor.BLACK);
        }
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "A").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "A"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "B").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "B"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "C").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "C"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "D").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "D"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "E").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "E"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "F").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "F"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "G").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "G"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "H").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "H"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "I").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "I"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "J").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "J"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "K").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "K"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "L").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "L"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "M").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "M"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "N").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "N"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "O").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "O"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "P").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "P"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "Q").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "Q"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "R").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "R"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "S").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "S"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "T").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "T"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "U").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "U"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "V").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "V"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "W").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "W"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "X").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "X"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "Y").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "Y"));
        icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, "Z").rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "Z"));

        for (int i = 0; i < 10; i++) {
            icons.add(new TaggedDrawable(new DrawableSquareLetter.Builder(48, c_col, String.valueOf(i)).rounded(4).disableBold().textColor(t_col).build(), Const.SYMB_TAGGD + "" + i));
        }

    }


    @Override
    public int getCount() {
        return icons.size();
    }


    @Override
    public Object getItem(int position) {
        return icons.get(position);
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ImageView imageView = new ImageView(mCtx);

        Drawable drawable;
        if (icons.get(position).getDrawable() instanceof TextDrawable) {
            drawable = icons.get(position).getDrawable();
        } else drawable = icons.get(position).getDrawable();
        imageView.setImageDrawable(drawable);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setLayoutParams(new GridView.LayoutParams(DeviceUtils.dp2px(ICON_SIZE), DeviceUtils.dp2px(ICON_SIZE)));
        imageView.setId(position);
        imageView.setTag(icons.get(position).getTag());
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIconChangedListener.onIconChanged(Const.ICON_CHANGED_TO_CUSTOM + icons.get(position).getTag());
            }
        });
        return imageView;
    }
}
