package com.pepperonas.teds.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pepperonas.aesprefs.AesPrefs;
import com.pepperonas.teds.R;
import com.pepperonas.teds.utils.Logic;

public class NavDrawerFragment extends Fragment {

    private static final String TAG = "NavDrawerFragment";

    private View mContainerView;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;

    private boolean mUserLearnedDrawer;
    private boolean mIsSavedInstanceState;

    private BroadcastReceiver mMsgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctx, Intent intent) {
            getActivity().finish();
        }
    };


    public NavDrawerFragment() { }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState) {
        View view;

        if (Logic.isDarkTheme(getActivity())) {
            view = inflater.inflate(R.layout.nav_drawer_dark, container, false);
        } else view = inflater.inflate(R.layout.nav_drawer, container, false);

        return view;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        Logic.applyTheme(getActivity());

        super.onCreate(savedInstanceState);

        mUserLearnedDrawer = AesPrefs.getBoolean(R.string.ap_user_learned_drawer, false);
        if (savedInstanceState != null) mIsSavedInstanceState = true;
    }


    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMsgReceiver);

        super.onDestroy();
    }


    public void setUp(int drawerId, DrawerLayout drawerLayout, final Toolbar tb) {
        mContainerView = getActivity().findViewById(drawerId);

        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, tb, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                if (!mUserLearnedDrawer) {
                    mUserLearnedDrawer = true;
                    AesPrefs.putBoolean(R.string.ap_user_learned_drawer, true);
                }

                getActivity().invalidateOptionsMenu();
            }


            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }


            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                if (slideOffset < 0.4f) tb.setAlpha(1f - slideOffset);
            }
        };

        if (!mUserLearnedDrawer && !mIsSavedInstanceState) {
            mDrawerLayout.openDrawer(mContainerView);
        }

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }


    public void closeDrawer() {
        mDrawerLayout.closeDrawer(mContainerView);
    }


    public void toggleDrawerSyncState() {
        mDrawerToggle.syncState();
    }

}
