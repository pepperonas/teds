package com.pepperonas.teds.fragments.color_chooser_fragment.icon_chooser_fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.pepperonas.teds.R;

/**
 * Created by pepperonas on 04/24/2015.
 * a
 */
public class FccTab extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.color_chooser_tab, container, false);
    }


}
