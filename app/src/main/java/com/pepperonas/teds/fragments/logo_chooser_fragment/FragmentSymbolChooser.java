package com.pepperonas.teds.fragments.logo_chooser_fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.pepperonas.teds.R;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class FragmentSymbolChooser extends DialogFragment {

    private static final String TAG = "FragmentSymbolChooser";


    public FragmentSymbolChooser() {

    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        return dialog;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.logo_chooser_fragment, container);

        // tab slider
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());

        // Set up the ViewPager with the sections adapter.
        ViewPager viewPager = (ViewPager) view.findViewById(R.id.pager);
        viewPager.setAdapter(sectionsPagerAdapter);

        return view;
    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public int getCount() {
            return 5;
        }


        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: return new FscTabOnline();
                case 1: return new FscTabMoney();
                case 2: return new FscTabTech();
                case 3: return new FscTabOther();
                case 4: return new FscTabTextDrawable();
                default: return null;
            }
        }


        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0: return getString(R.string.tab_web_internet);
                case 1: return getString(R.string.tab_shopping_travel);
                case 2: return getString(R.string.tab_tech_hobby);
                case 3: return getString(R.string.tab_money_work);
                case 4: return getString(R.string.tab_alphabet);
                default: return null;
            }
        }
    }

}
