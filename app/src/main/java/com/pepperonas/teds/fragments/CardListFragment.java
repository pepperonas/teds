package com.pepperonas.teds.fragments;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.clans.fab.FloatingActionButton;
import com.pepperonas.andbasx.base.ToastUtils;
import com.pepperonas.teds.R;
import com.pepperonas.teds.activities.MainActivity;
import com.pepperonas.teds.activities.ShowCardActivity;
import com.pepperonas.teds.adapter.CardListAdapter;
import com.pepperonas.teds.model.Card;
import com.pepperonas.teds.model.Label;
import com.pepperonas.teds.settings.Pref;
import com.pepperonas.teds.utils.Const;
import com.pepperonas.teds.utils.Logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CardListFragment extends Fragment
        implements View.OnClickListener, AdapterView.OnItemClickListener {

    private static final String TAG = "CardListFragment";

    private View mRoot;

    private List<Card> mCards = new ArrayList<Card>();

    private CardListAdapter mCardListAdapter;

    private ListView mListView;

    private String mPwd;
    private int mLastIndex;
    private int mOffset;
    private Bundle mBundle;
    private static long mLastShownNoCardsToast = 0L;

    private BroadcastReceiver mMsgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctx, Intent intent) {
            getActivity().finish();
        }
    };

    private BroadcastReceiver mIconChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            initializeTable();
        }
    };


    public CardListFragment() { }


    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRoot = inflater.inflate(R.layout.fragment_card_listview, container, false);

        Logic.applyTheme(getActivity());

        mBundle = this.getArguments();

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mIconChangedReceiver, new IntentFilter(Const.LBC_ICON_CHANGED));

        initializeTable();

        return mRoot;
    }


    @Override
    public void onResume() {
        super.onResume();

        mBundle = this.getArguments();
        mPwd = mBundle.getString(Const.BNL_PW);

        // Args überbringt die Namen der Labels
        String lId = mBundle.getString(Const.BNL_FAVORITE_OBJECT_ID);
        String lIV = mBundle.getString(Const.BNL_FAVORITE_OBJECT_IV);

        MainActivity main = (MainActivity) getActivity();

        // für den Fall dass kein NavDrawer-Eintrag ausgewählt ist
        // werden als Default alle Karten geladen und angezeigt
        if (lId != null) {

            String[] logicalLables = getResources().getStringArray(R.array.logical_label);

            if (lId.equals(logicalLables[Const.ALL_CARDS])) mCards = main.getDatabase().getAllCards();
            else if (lId.equals(logicalLables[Const.FAVORITES])) mCards = main.getDatabase().getFavoriteCards();
            else if (lId.equals(logicalLables[Const.UNLABELED])) mCards = main.getDatabase().getUnlabeledCards();
            else if (lId.equals(logicalLables[Const.WEAK_PASS])) mCards = main.getDatabase().getWeakPasswords();
            else if (!lId.equals(Const.SEARCH_MASK)) mCards = main.getDatabase().getAllCardsByLabel(lId, lIV);

            if (lId.equals(Const.SEARCH_MASK)) fillListByMatchingSearch(mBundle, main);

            // als Default-Verhalten "Alle Karten"..
        } else mCards = main.getDatabase().getAllCards();

        ensureShowToast();

        mListView.setSelectionFromTop(mLastIndex, mOffset);

        if (mCardListAdapter != null) mCardListAdapter.sort();
    }


    @Override
    public void onPause() {
        View v = mListView.getChildAt(0);
        mOffset = (v == null) ? 0 : (v.getTop() - mListView.getPaddingTop());
        mLastIndex = mListView.getFirstVisiblePosition();

        super.onPause();
    }


    @Override
    public void onDestroy() {
        mPwd = null;
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMsgReceiver);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mIconChangedReceiver);

        super.onDestroy();
    }


    @Override
    public void onClick(View view) { }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
        Intent showCardIntent = new Intent(getActivity(), ShowCardActivity.class);

        showCardIntent.putExtra(Const.BNL_PW, mPwd);
        showCardIntent.putExtra(Const.BNL_CARD_ID, mCards.get(pos).getId());
        showCardIntent.putExtra(Const.BNL_FROM_MAIN, true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(
                    getActivity(),
                    mCardListAdapter.getCardIcon(parent, view, pos),
                    getString(R.string.SHARED_ICON));

            getActivity().getWindow().setAllowEnterTransitionOverlap(true);

            getActivity().startActivity(showCardIntent, options.toBundle());
        }

        // für API < 21 (Lollipop)
        else getActivity().startActivity(showCardIntent);
    }


    private void ensureShowToast() {
        if ((mCards == null || mCards.size() == 0) && avoidToastFlood()) {

            mLastShownNoCardsToast = System.currentTimeMillis();
            ToastUtils.toastShort(R.string.no_cards_found);
        }
    }


    private boolean avoidToastFlood() {
        return (mLastShownNoCardsToast + 10000) < System.currentTimeMillis();
    }


    private void fillListByMatchingSearch(Bundle bundle, MainActivity mainActivity) {
        mCards.clear();

        List<Card> tmpAllCards = mainActivity.getDatabase().getAllCards();
        String userInput = bundle.getString(Const.BNL_SEARCH_ID);

        for (Card card : tmpAllCards) {
            if (card.getTitle().toLowerCase().contains(userInput.toLowerCase())) mCards.add(card);
        }
    }


    public void initializeTable() {
        mCardListAdapter = new CardListAdapter(getActivity(), this);

        mCardListAdapter.sort();

        mCardListAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
            }
        });

        mListView = (ListView) mRoot.findViewById(android.R.id.list);
        mListView.setAdapter(mCardListAdapter);
        mListView.setFastScrollEnabled(true);
        mListView.setOnItemClickListener(this);


        mListView.setLongClickable(true);
        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                return true;
            }
        });

        mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        mListView.setMultiChoiceModeListener(new ModeCallback());

        FloatingActionButton fab = (FloatingActionButton) mRoot.findViewById(R.id.fab_add_card);

        //        fab.attachToListView(mListView, new ScrollDirectionListener() {
        //            @Override
        //            public void onScrollDown() { }
        //
        //
        //            @Override
        //            public void onScrollUp() { }
        //        }, new AbsListView.OnScrollListener() {
        //            @Override
        //            public void onScrollStateChanged(AbsListView view, int scrollState) { }
        //
        //
        //            @Override
        //            public void onScroll(AbsListView v, int firstVisibItem, int visibItemCount, int totalItemCount) { }
        //        });

    }


    public List<Card> getCards() { return mCards; }


    public CardListAdapter getAdapter() { return mCardListAdapter; }


    private class ModeCallback implements AbsListView.MultiChoiceModeListener {

        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.menu_card_list_editor, menu);

            MenuItem setLable = menu.getItem(0);
            setLable.setVisible(true);

            MenuItem delete = menu.getItem(1);
            delete.setVisible(true);

            return true;
        }


        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return true;
        }


        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            MainActivity mainActivity = (MainActivity) getActivity();

            switch (item.getItemId()) {

                case R.id.action_delete_card:
                    SharedPreferences prefs = getActivity()
                            .getSharedPreferences(Pref.CONFIG_FILE, Context.MODE_PRIVATE);

                    if (prefs.getBoolean(Pref.PK_CB_APPLY_DELETE, true)) showDeleteDialog(mainActivity, mode);
                    else {
                        deleteSelectedCards(mainActivity, null);
                        mode.finish();
                    }
                    break;


                case R.id.action_set_lable:
                    showLableSelectionDialog();
                    mode.finish();
                    break;

                default:
                    break;
            }

            return true;
        }


        public void onDestroyActionMode(ActionMode mode) {
        }


        public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
            final int checkedCount = mListView.getCheckedItemCount();

            switch (checkedCount) {
                case 0:
                    mode.setTitle(getString(R.string.title_edit_card));
                    break;
                case 1:
                    mode.setTitle(getString(R.string.one_card_selected));
                    break;
                default:
                    mode.setTitle(checkedCount + " " + getString(R.string.card_selected));
                    break;
            }
        }
    }


    private void showLableSelectionDialog() {

        MainActivity mainActivity = (MainActivity) getActivity();

        List<Card> cards = new ArrayList<Card>();

        SparseBooleanArray checkedItems = mListView.getCheckedItemPositions();
        if (checkedItems != null) {
            for (int i = 0; i < checkedItems.size(); i++) {
                if (checkedItems.valueAt(i)) {
                    String item = mListView.getAdapter().getItem(checkedItems.keyAt(i)).toString();

                    cards.add(mainActivity.getDatabase().getCard(Long.parseLong(item)));

                }
            }
        }

        List<Label> resolvedLabels = new ArrayList<Label>();
        boolean firstRun = true;

        for (Card card : cards) {
            if (firstRun) {
                firstRun = false; // <- Neu.. TODO: prüfen
                resolvedLabels = mainActivity.getDatabase().getAllLabelByCard(card.getId());
            } else {
                List<Label> tmpLablesOfCard = mainActivity.getDatabase().getAllLabelByCard(card.getId());
                for (Label tmpLabel : tmpLablesOfCard) {
                    if (!resolvedLabels.contains(tmpLabel)) {
                        resolvedLabels.remove(tmpLabel);
                        ToastUtils.toastLong(R.string.toast_selection_is_smallest_intersection);
                    }
                }
            }
        }

        showLableChooserDialog(mainActivity, cards, resolvedLabels);
    }


    /**
     * Label für Karte bestimmen.
     */
    private void showLableChooserDialog(final MainActivity mainActivity, final List<Card> cards, List<Label> resolvedLabels) {

        final List<Integer> selection = new ArrayList<Integer>();
        final List<Label> labels = mainActivity.getDatabase().getAllLabels();

        String lableNameArray[] = new String[labels.size()];
        List<Integer> _preSelected = new ArrayList<Integer>();

        for (int i = 0; i < labels.size(); i++) lableNameArray[i] = labels.get(i).getName();

        int i = 0;
        for (Label label : labels) {
            for (Label linkedId : resolvedLabels) {
                if (label.getName().equals(linkedId.getName())) _preSelected.add(i);
            }
            i++;
        }

        Integer[] preSelected = new Integer[_preSelected.size()];
        for (int j = 0; j < _preSelected.size(); j++) preSelected[j] = _preSelected.get(j);

        new MaterialDialog.Builder(getActivity())
                .title(R.string.choose_label_title)
                .positiveText(R.string.ok)
                .neutralText(getString(R.string.New))
                .negativeText(R.string.cancel)
                .items(lableNameArray)
                .itemsCallbackMultiChoice(preSelected, new MaterialDialog.ListCallbackMultiChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, Integer[] which, CharSequence[] text) {
                        Collections.addAll(selection, which);
                        setLables(mainActivity, cards, labels, selection);
                        return true;
                    }
                })
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) { super.onPositive(dialog); }


                    @Override
                    public void onNegative(MaterialDialog dialog) { }


                    @Override
                    public void onNeutral(MaterialDialog dialog) {
                        showAddNewLableDialog(mainActivity);
                    }
                })
                .show();

        updateNavDrawer();
    }


    private void setLables(MainActivity mainActivity, List<Card> cards, List<Label> labels, List<Integer> selection) {

        for (Card card : cards) {
            // alte Einträge löschen
            mainActivity.getDatabase().removeAllLabelLinksToCard(card.getId());

            // neue Einträge setzen
            for (Integer aSelection : selection) {
                mainActivity.getDatabase().createLabelLink(card.getId(), labels.get(aSelection).getId());
            }
        }

        updateNavDrawer();
    }


    private void showAddNewLableDialog(final MainActivity mainActivity) {

        MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.dialog_title_new_label))
                .customView(R.layout.dialog_data_editor, true)
                .positiveText(R.string.ok)
                .negativeText(R.string.cancel)
                .autoDismiss(true)
                .showListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(final DialogInterface dialog) {

                        final MaterialDialog materialDialog = (MaterialDialog) dialog;

                        final EditText et = (EditText) materialDialog
                                .findViewById(R.id.et_rename_data);

                        Logic.applyLengthCheck(et, materialDialog);

                        et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                            @Override
                            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                materialDialog.getActionButton(DialogAction.POSITIVE).callOnClick();
                                return true;
                            }
                        });

                        Logic.applyLengthCheck(et, materialDialog);
                    }
                })
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        EditText et = (EditText) dialog.findViewById(R.id.et_rename_data);
                        if (et.getText().toString().isEmpty()) {
                            ToastUtils.toastLong(R.string.toast_label_editor_empty_inputs_are_not_allowed);
                        } else {
                            Label label = new Label(
                                    0, et.getText().toString(), String.valueOf(System.currentTimeMillis()));

                            mainActivity.getDatabase().createLabel(label);

                            showLableSelectionDialog();
                        }
                    }


                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }
                });

        MaterialDialog dialog = builder.build();

        dialog.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        dialog.show();
    }


    private void showDeleteDialog(final MainActivity mainActivity, final ActionMode mode) {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.hint)
                .content(R.string.dialog_msg_delete_selected_cards)
                .positiveText(R.string.ok)
                .negativeText(R.string.cancel)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        deleteSelectedCards(mainActivity, mode);
                    }


                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }
                })
                .show();
    }


    private void deleteSelectedCards(MainActivity mainActivity, ActionMode mode) {

        List<Card> cardsToDelete = new ArrayList<Card>();

        SparseBooleanArray checkedItems = mListView.getCheckedItemPositions();

        if (checkedItems == null) return;

        for (int i = 0; i < checkedItems.size(); i++) {
            if (checkedItems.valueAt(i)) {
                String item = mListView.getAdapter().getItem(
                        checkedItems.keyAt(i)).toString();

                for (Card card : mCards) {
                    if (card.getId() == Long.parseLong(item)) {
                        cardsToDelete.add(card);
                    }
                }
            }
        }

        for (Card card : cardsToDelete) {
            mainActivity.getDatabase().deleteCardInclDataPais(card.getId());
            mainActivity.getDatabase().removeAllLabelLinksToCard(card.getId());
            mCards.remove(card);
        }

        if (mode != null) {
            mode.finish();
        }

        mCardListAdapter.notifyDataSetChanged();

        updateNavDrawer();
    }


    public void updateNavDrawer() {
        MainActivity main = (MainActivity) getActivity();
        main.loadNavDrawer();
    }

}

