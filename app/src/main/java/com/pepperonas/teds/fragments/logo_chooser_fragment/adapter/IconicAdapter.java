package com.pepperonas.teds.fragments.logo_chooser_fragment.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.pepperonas.andbasx.system.DeviceUtils;
import com.pepperonas.jbasx.color.ColorUtils;
import com.pepperonas.jbasx.div.MaterialColor;
import com.pepperonas.jbasx.log.Log;
import com.pepperonas.teds.model.TaggedDrawable;
import com.pepperonas.teds.interfaces.IconChangedListener;
import com.pepperonas.teds.utils.Const;
import com.pepperonas.teds.utils.Logic;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class IconicAdapter extends BaseAdapter {

    private static final String TAG = "IconicAdapter";

    private static final int ICON_SIZE = 40;
    public static final int ONLINE = 0;
    public static final int MONEY = 1;
    public static final int TECH = 2;
    public static final int OTHER = 3;

    private Context mCtx;

    private List<TaggedDrawable> icons = new ArrayList<TaggedDrawable>();

    private IconChangedListener mIconChangedListener;


    void registerListener(IconChangedListener listener) {
        mIconChangedListener = listener;
    }


    public IconicAdapter(Context context, int position) {
        mCtx = context;
        registerListener((IconChangedListener) context);

        int iconColor = ColorUtils.toInt(MaterialColor.GREY_800);
        if (Logic.isDarkTheme(mCtx)) {
            iconColor = ColorUtils.toInt(MaterialColor.GREY_300);
        }

        switch (position) {
            case ONLINE: {
                Log.d(TAG, "initIcons  " + "ONLINE");
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_bitbucket).color(iconColor).sizeDp(ICON_SIZE), "faw_bitbucket"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_dropbox).color(iconColor).sizeDp(ICON_SIZE), "faw_dropbox"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_facebook).color(iconColor).sizeDp(ICON_SIZE), "faw_facebook"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_foursquare).color(iconColor).sizeDp(ICON_SIZE), "faw_foursquare"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_git).color(iconColor).sizeDp(ICON_SIZE), "faw_git"));

                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_github).color(iconColor).sizeDp(ICON_SIZE), "faw_github"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_github_alt).color(iconColor).sizeDp(ICON_SIZE), "faw_github_alt"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_github_square).color(iconColor).sizeDp(ICON_SIZE), "faw_github_square"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_google).color(iconColor).sizeDp(ICON_SIZE), "faw_google"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_google_plus).color(iconColor).sizeDp(ICON_SIZE), "faw_google_plus"));

                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_google_wallet).color(iconColor).sizeDp(ICON_SIZE), "faw_google_wallet"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_instagram).color(iconColor).sizeDp(ICON_SIZE), "faw_instagram"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_lastfm).color(iconColor).sizeDp(ICON_SIZE), "faw_lastfm"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_linkedin).color(iconColor).sizeDp(ICON_SIZE), "faw_linkedin"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_pinterest).color(iconColor).sizeDp(ICON_SIZE), "faw_pinterest"));

                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_reddit).color(iconColor).sizeDp(ICON_SIZE), "faw_reddit"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_skype).color(iconColor).sizeDp(ICON_SIZE), "faw_skype"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_spotify).color(iconColor).sizeDp(ICON_SIZE), "faw_spotify"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_soundcloud).color(iconColor).sizeDp(ICON_SIZE), "faw_soundcloud"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_stack_exchange).color(iconColor).sizeDp(ICON_SIZE), "faw_stack_exchange"));

                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_stack_overflow).color(iconColor).sizeDp(ICON_SIZE), "faw_stack_overflow"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_steam).color(iconColor).sizeDp(ICON_SIZE), "faw_steam"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_tumblr).color(iconColor).sizeDp(ICON_SIZE), "faw_tumblr"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_twitch).color(iconColor).sizeDp(ICON_SIZE), "faw_twitch"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_twitter).color(iconColor).sizeDp(ICON_SIZE), "faw_twitter"));

                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_vimeo_square).color(iconColor).sizeDp(ICON_SIZE), "faw_vimeo_square"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_wordpress).color(iconColor).sizeDp(ICON_SIZE), "faw_wordpress"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_xing).color(iconColor).sizeDp(ICON_SIZE), "faw_xing"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_yelp).color(iconColor).sizeDp(ICON_SIZE), "faw_yelp"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_youtube).color(iconColor).sizeDp(ICON_SIZE), "faw_youtube"));
                break;
            }
            case MONEY: {
                Log.d(TAG, "initIcons  " + "MONEY");
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_angellist).color(iconColor).sizeDp(ICON_SIZE), "faw_angellist"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_beer).color(iconColor).sizeDp(ICON_SIZE), "faw_beer"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_bank).color(iconColor).sizeDp(ICON_SIZE), "faw_bank"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_barcode).color(iconColor).sizeDp(ICON_SIZE), "faw_barcode"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_birthday_cake).color(iconColor).sizeDp(ICON_SIZE), "faw_birthday_cake"));

                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_btc).color(iconColor).sizeDp(ICON_SIZE), "faw_btc"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_cab).color(iconColor).sizeDp(ICON_SIZE), "faw_cab"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_cc_amex).color(iconColor).sizeDp(ICON_SIZE), "faw_cc_amex"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_cc_mastercard).color(iconColor).sizeDp(ICON_SIZE), "faw_cc_mastercard"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_cc_paypal).color(iconColor).sizeDp(ICON_SIZE), "faw_cc_paypal"));

                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_cc_visa).color(iconColor).sizeDp(ICON_SIZE), "faw_cc_visa"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_credit_card).color(iconColor).sizeDp(ICON_SIZE), "faw_credit_card"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_cutlery).color(iconColor).sizeDp(ICON_SIZE), "faw_cutlery"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_diamond).color(iconColor).sizeDp(ICON_SIZE), "faw_diamond"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_cny).color(iconColor).sizeDp(ICON_SIZE), "faw_cny"));

                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_dollar).color(iconColor).sizeDp(ICON_SIZE), "faw_dollar"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_eur).color(iconColor).sizeDp(ICON_SIZE), "faw_eur"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_gbp).color(iconColor).sizeDp(ICON_SIZE), "faw_gbp"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_gift).color(iconColor).sizeDp(ICON_SIZE), "faw_gift"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_globe).color(iconColor).sizeDp(ICON_SIZE), "faw_globe"));

                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_info).color(iconColor).sizeDp(ICON_SIZE), "faw_info"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_line_chart).color(iconColor).sizeDp(ICON_SIZE), "faw_line_chart"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_life_ring).color(iconColor).sizeDp(ICON_SIZE), "faw_life_ring"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_location_arrow).color(iconColor).sizeDp(ICON_SIZE), "faw_location_arrow"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_paypal).color(iconColor).sizeDp(ICON_SIZE), "faw_paypal"));

                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_ship).color(iconColor).sizeDp(ICON_SIZE), "faw_ship"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_shopping_cart).color(iconColor).sizeDp(ICON_SIZE), "faw_shopping_cart"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_smile_o).color(iconColor).sizeDp(ICON_SIZE), "faw_smile_o"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_tag).color(iconColor).sizeDp(ICON_SIZE), "faw_tag"));
                break;
            }
            case TECH: {
                Log.d(TAG, "initIcons  " + "TECH");
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_android).color(iconColor).sizeDp(ICON_SIZE), "faw_android"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_apple).color(iconColor).sizeDp(ICON_SIZE), "faw_apple"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_certificate).color(iconColor).sizeDp(ICON_SIZE), "faw_certificate"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_chain).color(iconColor).sizeDp(ICON_SIZE), "faw_chain"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_cloud).color(iconColor).sizeDp(ICON_SIZE), "faw_cloud"));

                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_crosshairs).color(iconColor).sizeDp(ICON_SIZE), "faw_crosshairs"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_comments_o).color(iconColor).sizeDp(ICON_SIZE), "faw_comments_o"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_css3).color(iconColor).sizeDp(ICON_SIZE), "faw_css3"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_digg).color(iconColor).sizeDp(ICON_SIZE), "faw_digg"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_dribbble).color(iconColor).sizeDp(ICON_SIZE), "faw_dribbble"));

                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_gear).color(iconColor).sizeDp(ICON_SIZE), "faw_gear"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_gears).color(iconColor).sizeDp(ICON_SIZE), "faw_gears"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_hdd_o).color(iconColor).sizeDp(ICON_SIZE), "faw_hdd_o"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_laptop).color(iconColor).sizeDp(ICON_SIZE), "faw_laptop"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_linux).color(iconColor).sizeDp(ICON_SIZE), "faw_linux"));

                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_sitemap).color(iconColor).sizeDp(ICON_SIZE), "faw_sitemap"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_superscript).color(iconColor).sizeDp(ICON_SIZE), "faw_superscript"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_server).color(iconColor).sizeDp(ICON_SIZE), "faw_server"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_windows).color(iconColor).sizeDp(ICON_SIZE), "faw_windows"));
                break;
            }
            case OTHER: {
                Log.d(TAG, "initIcons  " + "OTHER");
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_automobile).color(iconColor).sizeDp(ICON_SIZE), "faw_automobile"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_asterisk).color(iconColor).sizeDp(ICON_SIZE), "faw_asterisk"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_briefcase).color(iconColor).sizeDp(ICON_SIZE), "faw_briefcase"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_binoculars).color(iconColor).sizeDp(ICON_SIZE), "faw_binoculars"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_codepen).color(iconColor).sizeDp(ICON_SIZE), "faw_codepen"));

                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_dashcube).color(iconColor).sizeDp(ICON_SIZE), "faw_dashcube"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_envelope_o).color(iconColor).sizeDp(ICON_SIZE), "faw_envelope_o"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_eye).color(iconColor).sizeDp(ICON_SIZE), "faw_eye"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_fax).color(iconColor).sizeDp(ICON_SIZE), "faw_fax"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_file_o).color(iconColor).sizeDp(ICON_SIZE), "faw_file_o"));

                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_file_code_o).color(iconColor).sizeDp(ICON_SIZE), "faw_file_code_o"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_file_excel_o).color(iconColor).sizeDp(ICON_SIZE), "faw_file_excel_o"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_file_image_o).color(iconColor).sizeDp(ICON_SIZE), "faw_file_image_o"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_file_movie_o).color(iconColor).sizeDp(ICON_SIZE), "faw_file_movie_o"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_file_pdf_o).color(iconColor).sizeDp(ICON_SIZE), "faw_file_pdf_o"));

                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_file_photo_o).color(iconColor).sizeDp(ICON_SIZE), "faw_file_photo_o"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_file_powerpoint_o).color(iconColor).sizeDp(ICON_SIZE), "faw_file_powerpoint_o"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_file_word_o).color(iconColor).sizeDp(ICON_SIZE), "faw_file_word_o"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_file_zip_o).color(iconColor).sizeDp(ICON_SIZE), "faw_file_zip_o"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_female).color(iconColor).sizeDp(ICON_SIZE), "faw_female"));

                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_flask).color(iconColor).sizeDp(ICON_SIZE), "faw_flask"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_futbol_o).color(iconColor).sizeDp(ICON_SIZE), "faw_futbol_o"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_gamepad).color(iconColor).sizeDp(ICON_SIZE), "faw_gamepad"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_graduation_cap).color(iconColor).sizeDp(ICON_SIZE), "faw_graduation_cap"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_group).color(iconColor).sizeDp(ICON_SIZE), "faw_group"));

                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_home).color(iconColor).sizeDp(ICON_SIZE), "faw_home"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_key).color(iconColor).sizeDp(ICON_SIZE), "faw_key"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_motorcycle).color(iconColor).sizeDp(ICON_SIZE), "faw_motorcycle"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_male).color(iconColor).sizeDp(ICON_SIZE), "faw_male"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_music).color(iconColor).sizeDp(ICON_SIZE), "faw_music"));

                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_puzzle_piece).color(iconColor).sizeDp(ICON_SIZE), "faw_puzzle_piece"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_question).color(iconColor).sizeDp(ICON_SIZE), "faw_question"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_recycle).color(iconColor).sizeDp(ICON_SIZE), "faw_recycle"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_rocket).color(iconColor).sizeDp(ICON_SIZE), "faw_rocket"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_save).color(iconColor).sizeDp(ICON_SIZE), "faw_save"));

                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_shield).color(iconColor).sizeDp(ICON_SIZE), "faw_shield"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_sliders).color(iconColor).sizeDp(ICON_SIZE), "faw_sliders"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_unlock).color(iconColor).sizeDp(ICON_SIZE), "faw_unlock"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_unlock_alt).color(iconColor).sizeDp(ICON_SIZE), "faw_unlock_alt"));
                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_user_secret).color(iconColor).sizeDp(ICON_SIZE), "faw_user_secret"));

                icons.add(new TaggedDrawable(new IconicsDrawable(mCtx, FontAwesome.Icon.faw_wrench).color(iconColor).sizeDp(ICON_SIZE), "faw_wrench"));
                break;
            }
        }
    }


    @Override
    public int getCount() {
        return icons.size();
    }


    @Override
    public Object getItem(int position) {
        return icons.get(position);
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ImageView imageView = new ImageView(mCtx);
        imageView.setImageDrawable(icons.get(position).getIconincsDrawable());
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setLayoutParams(new GridView.LayoutParams(DeviceUtils.dp2px(40), DeviceUtils.dp2px(40)));
        imageView.setId(position);
        imageView.setTag(icons.get(position).getTag());

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIconChangedListener.onIconChanged(Const.ICON_CHANGED_TO_ICONIC + icons.get(position).getTag());
            }
        });

        return imageView;
    }

}
