package com.pepperonas.teds.fragments.logo_chooser_fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.pepperonas.teds.R;
import com.pepperonas.teds.fragments.logo_chooser_fragment.adapter.IconicAdapter;
import com.pepperonas.teds.utils.Logic;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class FscTabMoney extends Fragment {

    private LinearLayout mLinearLayout;


    public FscTabMoney() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (Logic.isDarkTheme(getActivity())) {
            mLinearLayout = (LinearLayout) inflater.inflate(R.layout.logo_chooser_tab_dark, container, false);
        } else mLinearLayout = (LinearLayout) inflater.inflate(R.layout.logo_chooser_tab, container, false);

        return mLinearLayout;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        IconicAdapter mIconicAdapter = new IconicAdapter(getContext(),1);
        GridView gridView = (GridView) mLinearLayout.findViewById(R.id.grid_view);
        gridView.setAdapter(mIconicAdapter);
    }

}
