package com.pepperonas.teds.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pepperonas.teds.R;
import com.pepperonas.teds.model.AccountInfo;
import com.pepperonas.teds.model.Database;
import com.pepperonas.teds.utils.Logic;

import java.util.List;

/**
 * Created by pepperonas on 05/03/2015.
 * AccountAdapter
 */
public class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.ViewHolder> {

    private static final String TAG = "AccountAdapter";

    private List<AccountInfo> mAccountList;
    private int mRowLayout;
    private Context mContext;


    public AccountAdapter(List<AccountInfo> accounts, int rowLayout, Context ctx) {
        this.mAccountList = accounts;
        this.mRowLayout = rowLayout;
        this.mContext = ctx;
    }


    public void refreshAccountList(Database db) {
        mAccountList = db.getAllAccounts();
        notifyDataSetChanged();
    }


    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i) {
        final View v = LayoutInflater.from(viewGroup.getContext()).inflate(mRowLayout, viewGroup, false);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        AccountInfo account = mAccountList.get(i);
        viewHolder.accountName.setText(account.getName());
        viewHolder.accountImage.setBackgroundColor(generateColor());
    }


    private int generateColor() {
        if (Logic.isDarkTheme(mContext)) {
            return mContext.getResources().getColor(R.color.Grey_2);
        } return mContext.getResources().getColor(R.color.Grey_8);

    }


    @Override
    public int getItemCount() {
        return mAccountList == null ? 0 : mAccountList.size();
    }


    @Override
    public long getItemId(int position) {
        return mAccountList.get(position).getId();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView accountName;
        public ImageView accountImage;


        public ViewHolder(View itemView) {
            super(itemView);
            accountName = (TextView) itemView.findViewById(R.id.accountName);
            accountImage = (ImageView) itemView.findViewById(R.id.accountImage);

        }
    }


}
