package com.pepperonas.teds.adapter;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pepperonas.aesprefs.AesPrefs;
import com.pepperonas.jbasx.div.MaterialColor;
import com.pepperonas.teds.R;
import com.pepperonas.teds.activities.MainActivity;
import com.pepperonas.teds.model.Card;
import com.pepperonas.teds.model.CardIcon;
import com.pepperonas.teds.fragments.CardListFragment;
import com.pepperonas.teds.settings.Pref;
import com.pepperonas.teds.utils.Const;
import com.pepperonas.teds.utils.Logic;

import java.util.Collections;
import java.util.Comparator;

/**
 * Created by pepperonas on 05.03.15.
 * ListViewAdapter
 */
public class CardListAdapter extends BaseAdapter {

    private static final String TAG = "CardListAdapter";

    private final Context mCtx;

    private CardListFragment mCardListFragment;


    public CardListAdapter(Context context, CardListFragment cardListFragment) {
        mCtx = context;
        mCardListFragment = cardListFragment;
    }


    public void sort() {
        long start = System.currentTimeMillis();
        String mode = "";

        switch (AesPrefs.getInt(Pref.PK_SORT_LIST, 0)) {
            case Pref.SORT_BY_ALPHA_A_Z:
                mode = "a->z";
                Collections.sort(mCardListFragment.getCards(), new Comparator<Card>() {
                    public int compare(Card cardA, Card cardB) {
                        return cardA.getTitle().compareToIgnoreCase(cardB.getTitle());
                    }
                });

                break;

            case Pref.SORT_BY_ALPHA_Z_A:
                mode = "z->a";
                Collections.sort(mCardListFragment.getCards(), new Comparator<Card>() {
                    public int compare(Card cardA, Card cardB) {
                        return cardA.getTitle().compareToIgnoreCase(cardB.getTitle());
                    }
                });
                Collections.reverse(mCardListFragment.getCards());
                break;

            case Pref.SORT_BY_LAST_EDIT_NEW_ON_TOP:
                mode = "newest top";
                Collections.sort(mCardListFragment.getCards(), new Comparator<Card>() {
                    public int compare(Card cardA, Card cardB) {
                        return cardA.getCreated().compareToIgnoreCase(cardB.getCreated());
                    }
                });
                Collections.reverse(mCardListFragment.getCards());
                break;

            case Pref.SORT_BY_LAST_EDIT_OLD_ON_TOP:
                mode = "oldest top";
                Collections.sort(mCardListFragment.getCards(), new Comparator<Card>() {
                    public int compare(Card cardA, Card cardB) {
                        return cardA.getCreated().compareToIgnoreCase(cardB.getCreated());
                    }
                });
                //                Collections.reverse(mCardListFragment.getCards());
                break;
        }

        Log.i(TAG, "sorted! mode: " + mode + " # in " + (System.currentTimeMillis() - start) + "ms");
    }


    @Override
    public int getCount() {
        return mCardListFragment.getCards().size();
    }


    @Override
    public String getItem(int position) {
        return String.valueOf(mCardListFragment.getCards().get(position).getId());
    }


    @Override
    public long getItemId(int position) { return position; }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if (convertView == null) {

            convertView = LayoutInflater.from(mCtx).inflate(R.layout.card_list_row, parent, false);

            viewHolder = new ViewHolder();

            convertView.setTag(viewHolder);

        } else viewHolder = (ViewHolder) convertView.getTag();

        // Titel setzen (Item-Click-Listener gilt "hauptsächlich" für die Karten-Titel,
        // somit überwacht das Fragment den Listener)
        viewHolder.mTvTitle = (TextView) convertView.findViewById(R.id.tv_card_title);
        viewHolder.mTvTitle.setText(mCardListFragment.getCards().get(position).getTitle());

        // Favoriten-Stern (inkl. Logik)
        viewHolder.mIbStar = (ImageView) convertView.findViewById(R.id.ib_star);

        if (mCardListFragment.getCards().get(position).isFavorite()) {
            viewHolder.mIbStar.setBackgroundResource(R.drawable.star);
        } else viewHolder.mIbStar.setBackgroundResource(R.drawable.unstar);

        setIcon(convertView, viewHolder, mCardListFragment.getCards().get(position).getRawIcon());

        viewHolder.mIbStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mCardListFragment.getCards().get(position).isFavorite()) {
                    viewHolder.mIbStar.setBackgroundResource(R.drawable.unstar);
                    mCardListFragment.getCards().get(position).setFavorite(false);
                    changeFavoriteStatus(position);
                } else {
                    viewHolder.mIbStar.setBackgroundResource(R.drawable.star);
                    mCardListFragment.getCards().get(position).setFavorite(true);
                    changeFavoriteStatus(position);
                }

                // TODO: das könnte noch auf die Favoriten reduziert werden
                mCardListFragment.updateNavDrawer();
            }
        });

        parent.post(new Runnable() {
            public void run() {
                Rect delegateArea = new Rect();
                ImageView delegate = viewHolder.mIbStar;
                delegate.getHitRect(delegateArea);
                delegateArea.top -= Logic.mkDiP(mCtx, Const.TOUCH_OFFSET_CARD_LIST_FAV_STAR);
                delegateArea.bottom += Logic.mkDiP(mCtx, Const.TOUCH_OFFSET_CARD_LIST_FAV_STAR);
                delegateArea.left -= Logic.mkDiP(mCtx, Const.TOUCH_OFFSET_CARD_LIST_FAV_STAR);
                delegateArea.right += Logic.mkDiP(mCtx, Const.TOUCH_OFFSET_CARD_LIST_FAV_STAR);
                TouchDelegate expandedArea = new TouchDelegate(delegateArea, delegate);

                if (View.class.isInstance(delegate.getParent())) {
                    ((View) delegate.getParent()).setTouchDelegate(expandedArea);
                }
            }
        });

        return convertView;
    }


    private void setIcon(View convertView, ViewHolder viewHolder, String iconArg) {
        CardIcon cardIcon = new CardIcon(iconArg);

        viewHolder.mIvBg = (ImageView) convertView.findViewById(R.id.iv_background);
        viewHolder.mIvIcon = (ImageView) convertView.findViewById(R.id.iv_logo);

        Drawable background, logo;

        if (cardIcon.ensureBackground()) {
            background = Logic.loadBackground(mCtx, cardIcon.getColor());
        } else {
            background = Logic.loadBackground(mCtx, Logic.isDarkTheme(mCtx) ? MaterialColor.BLUE_GREY_500 : MaterialColor.DEEP_PURPLE_A400);
        }

        String symbolColor = CardIcon.convertSymbolColor(cardIcon.getSymbolColor());

        if (cardIcon.ensureSymbol()) {
            if (cardIcon.getSymbol().contains(Const.SYMB_FAW)) {
                logo = Logic.loadIconics(mCtx, cardIcon.getSymbol(), 32, symbolColor);
            } else {
                logo = Logic.loadTextDrawable(cardIcon.getSymbol(), 32, symbolColor);
            }
        } else logo = Logic.loadIconics(mCtx, "faw_question", 32, symbolColor);

        viewHolder.mIvBg.setImageDrawable(background);
        viewHolder.mIvIcon.setImageDrawable(logo);
    }


    private void changeFavoriteStatus(int i) {
        if (mCtx instanceof MainActivity) {

            // update card in database
            ((MainActivity) mCtx).getDatabase().updateCard(mCardListFragment.getCards().get(i));

            // update drawer if favorite state changed
            ((MainActivity) mCtx).collectLogicalLabelSizes();
        }
    }


    public View getCardIcon(AdapterView<?> parent, View view, int position) {
        View v = getView(position, view, parent);
        return v.findViewById(R.id.iv_card_icon);
    }


    private static class ViewHolder {

        public ImageView mIvBg;
        public ImageView mIvIcon;
        public TextView mTvTitle;
        public ImageView mIbStar;
    }

}
