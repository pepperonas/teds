package com.pepperonas.teds.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pepperonas.teds.R;
import com.pepperonas.teds.model.Label;
import com.pepperonas.teds.utils.Const;
import com.pepperonas.teds.utils.Logic;

import java.util.List;

/**
 * Created by pepperonas on 05.03.15.
 * NavigationDrawerAdapter
 */
public class NavDrawerAdapter extends BaseAdapter {

    private static final String TAG = "NavDrawerAdapter";

    private Context mCxt;

    private List<Label> mLabels;

    private int mSelectedItem;
    private String[] mDefLables;


    class MyViewHolder {

        ImageView mIvLableIcon;
        TextView mTvLableName;
        TextView mTvAmount;
        LinearLayout mDivider;
        LinearLayout mRoot;


        MyViewHolder(View v) {
            mIvLableIcon = (ImageView) v.findViewById(R.id.iv_navdrawer_lable_icon);
            mTvLableName = (TextView) v.findViewById(R.id.tv_card_title_);
            mTvAmount = (TextView) v.findViewById(R.id.tv_list_links_amount);
            mDivider = (LinearLayout) v.findViewById(R.id.navdr_divider);
            mRoot = (LinearLayout) v.findViewById(R.id.drawer_list_item);
        }

    }


    public NavDrawerAdapter(Context ctx, List<Label> obj) {
        mCxt = ctx;
        mLabels = obj;

        mDefLables = mCxt.getResources().getStringArray(R.array.logical_label);
    }


    @Override
    public int getCount() {
        return mLabels.size();
    }


    @Override
    public Label getItem(int position) {
        markItemAsSelected(position);
        return mLabels.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    public void markItemAsSelected(int position) {
        mSelectedItem = position;
    }


    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }


    public void updateLogicalLabels(int[] amountOfEntries) {
        int i = 0;
        for (Label label : mLabels) {
            if (label.getName().equals(mDefLables[Const.ALL_CARDS])) {
                label.setSize(amountOfEntries[Const.ALL_CARDS]);
                mLabels.set(i, label);
            }
            if (label.getName().equals(mDefLables[Const.FAVORITES])) {
                label.setSize(amountOfEntries[Const.FAVORITES]);
                mLabels.set(i, label);
            }
            if (label.getName().equals(mDefLables[Const.UNLABELED])) {
                label.setSize(amountOfEntries[Const.UNLABELED]);
                mLabels.set(i, label);
            }
            if (label.getName().equals(mDefLables[Const.WEAK_PASS])) {
                label.setSize(amountOfEntries[Const.WEAK_PASS]);
                mLabels.set(i, label);
            }
            i++;
        }
        notifyDataSetChanged();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        MyViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = (LayoutInflater)
                    mCxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            row = inflater.inflate(R.layout.navdrawer_list_item, parent, false);
            holder = new MyViewHolder(row);
            row.setTag(holder);
        } else {
            // View wird receicelt
            holder = (MyViewHolder) row.getTag();
        }

        Label label = mLabels.get(position);

        holder.mTvLableName.setText(label.getName());
        holder.mTvAmount.setText("" + label.getSize());

        if (Logic.isDarkTheme(mCxt)) makeDarkTheme(position, holder, label);
        else makeLightTheme(position, holder, label);

        return row;
    }


    private void makeLightTheme(int position, MyViewHolder holder, Label label) {

        setLightIcons(holder, label);

        int unselected, selected;
        selected = mCxt.getResources().getColor(R.color.colorNavDrawerIndicator);
        unselected = mCxt.getResources().getColor(R.color.Grey_2);
        if (position == mSelectedItem) {
            holder.mTvLableName.setTextColor(selected);
            holder.mTvAmount.setTextColor(selected);

            holder.mTvLableName.setBackgroundColor(mCxt.getResources().getColor(R.color.Grey_7));
            holder.mTvAmount.setBackgroundColor(mCxt.getResources().getColor(R.color.Grey_7));
            holder.mRoot.setBackgroundColor(mCxt.getResources().getColor(R.color.Grey_7));

        } else {
            holder.mTvLableName.setTextColor(unselected);
            holder.mTvAmount.setTextColor(unselected);
            holder.mTvLableName.setBackgroundColor(mCxt.getResources().getColor(R.color.Grey_8));
            holder.mTvAmount.setBackgroundColor(mCxt.getResources().getColor(R.color.Grey_8));
            holder.mRoot.setBackgroundColor(mCxt.getResources().getColor(R.color.Grey_8));
        }
    }


    private void makeDarkTheme(int position, MyViewHolder holder, Label label) {
        setDarkIcons(holder, label);

        int unselected, selected;
        selected = mCxt.getResources().getColor(R.color.colorNavDrawerIndicator_darkTheme);
        unselected = mCxt.getResources().getColor(R.color.Grey_8);

        if (position == mSelectedItem) {
            holder.mTvLableName.setTextColor(selected);
            holder.mTvAmount.setTextColor(selected);

            holder.mTvLableName.setBackgroundColor(mCxt.getResources().getColor(R.color.Grey_3));
            holder.mTvAmount.setBackgroundColor(mCxt.getResources().getColor(R.color.Grey_3));
            holder.mRoot.setBackgroundColor(mCxt.getResources().getColor(R.color.Grey_3));

        } else {
            holder.mTvLableName.setTextColor(unselected);
            holder.mTvAmount.setTextColor(unselected);
            holder.mTvLableName.setBackgroundColor(mCxt.getResources().getColor(R.color.Grey_2));
            holder.mTvAmount.setBackgroundColor(mCxt.getResources().getColor(R.color.Grey_2));
            holder.mRoot.setBackgroundColor(mCxt.getResources().getColor(R.color.Grey_2));
        }
    }


    private void setLightIcons(MyViewHolder holder, Label label) {
        String name = label.getName();

        if (name.equals((mDefLables[Const.ALL_CARDS]))) {
            holder.mIvLableIcon.setImageDrawable(mCxt.getResources().getDrawable(R.drawable.allcards_label_light));
        } else if (name.equals((mDefLables[Const.FAVORITES]))) {
            holder.mIvLableIcon.setImageDrawable(mCxt.getResources().getDrawable(R.drawable.favorite_label_light));
        } else if (name.equals((mDefLables[Const.UNLABELED]))) {
            holder.mIvLableIcon.setImageDrawable(mCxt.getResources().getDrawable(R.drawable.unlabled_label_light));
        } else if (name.equals((mDefLables[Const.WEAK_PASS]))) {
            holder.mIvLableIcon.setImageDrawable(mCxt.getResources().getDrawable(R.drawable.weakpass_label_light));
        } else {
            holder.mIvLableIcon.setImageDrawable(mCxt.getResources().getDrawable(R.drawable.user_label_light));
        }
    }


    private void setDarkIcons(MyViewHolder holder, Label label) {
        String name = label.getName();

        if (name.equals((mDefLables[Const.ALL_CARDS]))) {
            holder.mIvLableIcon.setImageDrawable(mCxt.getResources().getDrawable(R.drawable.allcards_lable_dark));
        } else if (name.equals((mDefLables[Const.FAVORITES]))) {
            holder.mIvLableIcon.setImageDrawable(mCxt.getResources().getDrawable(R.drawable.favorite_lable_dark));
        } else if (name.equals((mDefLables[Const.UNLABELED]))) {
            holder.mIvLableIcon.setImageDrawable(mCxt.getResources().getDrawable(R.drawable.unlabled_lable_dark));
        } else if (name.equals((mDefLables[Const.WEAK_PASS]))) {
            holder.mIvLableIcon.setImageDrawable(mCxt.getResources().getDrawable(R.drawable.weakpass_lable_dark));
        } else {
            holder.mIvLableIcon.setImageDrawable(mCxt.getResources().getDrawable(R.drawable.user_lable_dark));
        }
    }

}
