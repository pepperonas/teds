package com.pepperonas.teds.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pepperonas.teds.R;
import com.pepperonas.teds.model.DataPair;
import com.pepperonas.teds.utils.Const;
import com.pepperonas.teds.utils.Logic;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pepperonas on 05/07/2015.
 * ShowCardListAdapter
 */
public class ShowCardListAdapter extends BaseAdapter {

    private static final String TAG = "ShowCardListAdapter";

    private Context mCtx;
    private List<DataPair> mDataPairs = new ArrayList<DataPair>();
    private static LayoutInflater sInflater = null;
    private int mThemeId;


    public ShowCardListAdapter(Context context, List<DataPair> dataPair) {
        this.mCtx = context;

        if (Logic.isDarkTheme(mCtx)) mThemeId = R.style.AppTheme_Dark;
        else mThemeId = R.style.AppTheme;

        for (DataPair pair : dataPair) mDataPairs.add(pair);

        sInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() { return mDataPairs.size(); }


    @Override
    public Object getItem(int position) { return mDataPairs.get(position); }


    @Override
    public long getItemId(int position) { return position; }


    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {

        if (convertView == null) {
            if (Logic.isSecretField(mCtx, mDataPairs.get(pos).getDescription())) {
                convertView = sInflater.inflate(R.layout.show_card_datapair_password_layout, null);

                final ImageView btnToggleVisi = (ImageView) convertView.findViewById(R.id.btn_toggle_password_visibility);

                final TextView tvSecret = (TextView) convertView.findViewById(R.id.datapair_value);
                tvSecret.setText(mDataPairs.get(pos).getValue());
                tvSecret.setTransformationMethod(PasswordTransformationMethod.getInstance());

                TypedArray visib = mCtx.getTheme().obtainStyledAttributes(mThemeId, new int[]{R.attr.theme_dependent_visibility_off});
                int visii = visib.getResourceId(0, 0);
                final Drawable visibleIcon = mCtx.getResources().getDrawable(visii);

                TypedArray invisib = mCtx.getTheme().obtainStyledAttributes(mThemeId, new int[]{R.attr.theme_dependent_visibility});
                int invisii = invisib.getResourceId(0, 0);
                final Drawable visibleOffIcon = mCtx.getResources().getDrawable(invisii);

                btnToggleVisi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (!tvSecret.getTransformationMethod().equals(HideReturnsTransformationMethod.getInstance())) {
                            tvSecret.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                            btnToggleVisi.setBackground(visibleIcon);
                        } else {
                            tvSecret.setTransformationMethod(PasswordTransformationMethod.getInstance());
                            btnToggleVisi.setBackground(visibleOffIcon);
                        }
                    }
                });

                parent.post(new Runnable() {
                    public void run() {
                        Rect delegateArea = new Rect();
                        ImageView delegate = btnToggleVisi;
                        delegate.getHitRect(delegateArea);
                        delegateArea.top -= Logic.mkDiP(mCtx, Const.TOUCH_OFFSET_CARD_LIST_FAV_STAR);
                        delegateArea.bottom += Logic.mkDiP(mCtx, Const.TOUCH_OFFSET_CARD_LIST_FAV_STAR);
                        delegateArea.left -= Logic.mkDiP(mCtx, Const.TOUCH_OFFSET_CARD_LIST_FAV_STAR);
                        delegateArea.right += Logic.mkDiP(mCtx, Const.TOUCH_OFFSET_CARD_LIST_FAV_STAR);
                        TouchDelegate expandedArea = new TouchDelegate(delegateArea, delegate);
                        if (View.class.isInstance(delegate.getParent())) {
                            ((View) delegate.getParent())
                                    .setTouchDelegate(expandedArea);
                        }
                    }
                });

                TextView tvDesc = (TextView) convertView.findViewById(R.id.datapair_description);
                tvDesc.setText(mDataPairs.get(pos).getDescription());

            } else if (Logic.isUrlField(mCtx, mDataPairs.get(pos).getDescription())) {
                convertView = sInflater.inflate(R.layout.show_card_datapair, null);

                TextView tvDesc = (TextView) convertView.findViewById(R.id.datapair_description);
                tvDesc.setText(mDataPairs.get(pos).getDescription());
                TextView tvValue = (TextView) convertView.findViewById(R.id.datapair_value);
                tvValue.setText(mDataPairs.get(pos).getValue());
                tvValue.setTextAppearance(mCtx, R.style.hyperlink);
                tvValue.setTypeface(Typeface.createFromAsset(mCtx.getAssets(), "fonts/robotocondensed_regular.ttf"));
                tvValue.setPaintFlags(tvValue.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            } else {
                convertView = sInflater.inflate(R.layout.show_card_datapair, null);

                TextView tvDesc = (TextView) convertView.findViewById(R.id.datapair_description);
                tvDesc.setText(mDataPairs.get(pos).getDescription());
                TextView tvValue = (TextView) convertView.findViewById(R.id.datapair_value);
                tvValue.setText(mDataPairs.get(pos).getValue());
            }
        }

        return convertView;
    }

}