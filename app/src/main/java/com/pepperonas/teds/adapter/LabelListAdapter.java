package com.pepperonas.teds.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pepperonas.teds.R;
import com.pepperonas.teds.model.Database;
import com.pepperonas.teds.model.Label;
import com.pepperonas.teds.utils.Const;
import com.pepperonas.teds.utils.Logic;

import java.util.List;

/**
 * Created by pepperonas on 03/13/2015.
 * LableListAdapter
 */
public class LabelListAdapter extends BaseAdapter {

    private static final String TAG = "LabelListAdapter";

    private Context mContext;
    private List<Label> mLabels;


    public LabelListAdapter(Context ctx, List<Label> list) {
        mContext = ctx;
        mLabels = list;
    }


    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return mLabels.size();
    }


    @Override
    public Object getItem(int position) {
        return mLabels.get(position);
    }


    @Override
    public long getItemId(int position) {
        return mLabels.get(position).getId();
    }


    @Override
    public boolean hasStableIds() {
        return true;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if (convertView == null) {

            convertView = LayoutInflater.from(mContext).inflate(R.layout.lable_list_row, parent, false);

            viewHolder = new ViewHolder();

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Label label = mLabels.get(position);

        // Icons setzen...
        viewHolder.mIvIcon = (ImageView) convertView.findViewById(R.id.iv_label_icon);

        // Titel setzen (Item-Click-Listener gilt "hauptsächlich" für die Karten-Titel,
        // somit überwacht das Fragment den Listener)
        viewHolder.mTvTitle = (TextView) convertView.findViewById(R.id.tv_label_name);
        viewHolder.mTvTitle.setText(label.getName());

        if (Logic.isDarkTheme(mContext)) setLightIcons(viewHolder, label);
        else setDarkIcons(viewHolder, label);


        return convertView;
    }


    private void setLightIcons(ViewHolder holder, Label label) {
        String[] defData = mContext.getResources().getStringArray(R.array.logical_label);

        String name = label.getName();

        if (name.equals((defData[Const.ALL_CARDS]))) {
            holder.mIvIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.allcards_label_light));
        } else if (name.equals((defData[Const.FAVORITES]))) {
            holder.mIvIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.favorite_label_light));
        } else if (name.equals((defData[Const.UNLABELED]))) {
            holder.mIvIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.unlabled_label_light));
        } else if (name.equals((defData[Const.WEAK_PASS]))) {
            holder.mIvIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.weakpass_label_light));
        } else {
            holder.mIvIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.user_label_light));
        }
    }


    private void setDarkIcons(ViewHolder holder, Label label) {
        String[] defData = mContext.getResources().getStringArray(R.array.logical_label);

        String name = label.getName();

        if (name.equals((defData[Const.ALL_CARDS]))) {
            holder.mIvIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.allcards_lable_dark));
        } else if (name.equals((defData[Const.FAVORITES]))) {
            holder.mIvIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.favorite_lable_dark));
        } else if (name.equals((defData[Const.UNLABELED]))) {
            holder.mIvIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.unlabled_lable_dark));
        } else if (name.equals((defData[Const.WEAK_PASS]))) {
            holder.mIvIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.weakpass_lable_dark));
        } else {
            holder.mIvIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.user_lable_dark));
        }
    }


    public void notifyDataSetChanged(Database mDatabase) {
        mLabels = mDatabase.getAllLabels();
        notifyDataSetChanged();
    }


    private static class ViewHolder {

        public ImageView mIvIcon;
        public TextView mTvTitle;

    }

}
