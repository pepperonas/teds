package com.pepperonas.teds.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.pepperonas.andbasx.base.ToastUtils;
import com.pepperonas.teds.R;
import com.pepperonas.teds.utils.Const;
import com.pepperonas.teds.utils.Logic;

public class ClipboardReceiverLogin extends BroadcastReceiver {

    private static final String TAG = "ClipReceiverLogin";


    @Override
    public void onReceive(Context ctx, Intent intent) {
        String copied = intent.getStringExtra(Const.BNL_COPY_CLIPBOARD);
        Log.d(TAG, "onReceive copied=" + copied);

        ClipboardManager clipboard = (ClipboardManager) ctx.getSystemService(Context.CLIPBOARD_SERVICE);

        ClipData clip = ClipData.newPlainText(ctx.getString(R.string.login), copied);
        clipboard.setPrimaryClip(clip);

        Logic.ensureClearClipboard(ctx, clipboard);

        ToastUtils.toastShort(R.string.data_copied_to_clipboard);
    }

}

