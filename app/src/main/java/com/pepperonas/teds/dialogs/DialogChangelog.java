package com.pepperonas.teds.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.mikepenz.community_material_typeface_library.CommunityMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.pepperonas.teds.R;
import com.pepperonas.teds.settings.SettingsActivity;
import com.pepperonas.teds.utils.Logic;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class DialogChangelog {

    public DialogChangelog(final SettingsActivity act, int color) {
        final String[] titles = act.getResources().getStringArray(R.array.changeTitles);
        final String[] subTitles = act.getResources().getStringArray(R.array.changeSubTitles);
        final String[] contents = act.getResources().getStringArray(R.array.changeContents);

        final LinearLayout.LayoutParams
                layoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        layoutParams.setMargins(0, Logic.mkDiP(act, 24), 0, 0);

        new MaterialDialog.Builder(act)
                .title(R.string.dialog_changelog)
                .customView(R.layout.dialog_changelog, true)
                .icon(new IconicsDrawable(act, CommunityMaterial.Icon.cmd_xml).colorRes(color).sizeDp(24))
                .positiveText(R.string.ok)
                .autoDismiss(true)
                .showListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(final DialogInterface dialog) {
                        final MaterialDialog matDia = (MaterialDialog) dialog;

                        LinearLayout container = (LinearLayout) matDia
                                .findViewById(R.id.container_changelog);

                        for (int i = 0; i < titles.length; i++) {
                            LayoutInflater inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                            View dataSet = inflater.inflate(R.layout.changelog_dataset_template, null);

                            TextView title = (TextView) dataSet.findViewById(R.id.tvChangeTitle);
                            TextView subTitle = (TextView) dataSet.findViewById(R.id.tvChangeSubTitle);
                            TextView content = (TextView) dataSet.findViewById(R.id.tvChangeContent);

                            title.setText(titles[i]);
                            subTitle.setText(subTitles[i]);
                            content.setText(contents[i]);

                            if (i >= 1) dataSet.setLayoutParams(layoutParams);

                            container.addView(dataSet);
                        }
                    }
                })
                .show();
    }
}
