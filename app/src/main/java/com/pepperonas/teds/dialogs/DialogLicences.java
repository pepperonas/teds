package com.pepperonas.teds.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.pepperonas.teds.R;
import com.pepperonas.teds.settings.SettingsActivity;
import com.pepperonas.teds.utils.Logic;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class DialogLicences {

    public DialogLicences(final SettingsActivity act, int color) {
        final String[] titles = act.getResources().getStringArray(R.array.licensesTitles);
        final String[] subTitles = act.getResources().getStringArray(R.array.licensesSubTitles);
        final String[] contents = act.getResources().getStringArray(R.array.licensesContents);
        final LinearLayout.LayoutParams layoutParams =
                new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, Logic.mkDiP(act, 24), 0, 0);

        MaterialDialog.Builder builder = new MaterialDialog.Builder(act)
                .title(R.string.pref_title_pr_licenses)
                .customView(R.layout.dialog_licenses, true)
                .positiveText(R.string.ok)
                .autoDismiss(true)
                .icon(new IconicsDrawable(act, GoogleMaterial.Icon.gmd_subject).colorRes(color).sizeDp(24))
                .showListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        final MaterialDialog matDia = (MaterialDialog) dialog;
                        LinearLayout container = (LinearLayout) matDia.findViewById(R.id.container_licenses);
                        for (int i = 0; i < titles.length; i++) {
                            LayoutInflater inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View dataSet = inflater.inflate(R.layout.licenses_dataset_template, null);
                            TextView title = (TextView) dataSet.findViewById(R.id.tvLicensesTitle);
                            TextView subTitle = (TextView) dataSet.findViewById(R.id.tvLicensesSubTitle);
                            TextView content = (TextView) dataSet.findViewById(R.id.tvLicensesContent);
                            title.setText(titles[i]);
                            subTitle.setText(subTitles[i]);
                            content.setText(contents[i]);
                            if (i >= 1) dataSet.setLayoutParams(layoutParams);
                            container.addView(dataSet);
                        }
                    }
                })
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }
                });
        MaterialDialog dialog = builder.build();
        dialog.show();
    }
}
