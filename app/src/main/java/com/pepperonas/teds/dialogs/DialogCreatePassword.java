package com.pepperonas.teds.dialogs;

import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.pepperonas.teds.R;
import com.pepperonas.teds.activities.LoginActivity;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class DialogCreatePassword {

    public DialogCreatePassword(final LoginActivity act) {

        MaterialDialog.Builder builder = new MaterialDialog.Builder(act)
                .title(act.getString(R.string.dialog_title_create_masterpwd))
                .customView(R.layout.dialog_main_login, true)
                .autoDismiss(false)
                        //                .cancelable(false)
                .cancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        act.finish();
                    }
                })
                .showListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(final DialogInterface dialog) {

                        final MaterialDialog materialDialog = (MaterialDialog) dialog;

                        materialDialog
                                .getWindow().setSoftInputMode(
                                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

                        materialDialog.getActionButton(DialogAction.POSITIVE).setClickable(false);
                        materialDialog.getActionButton(DialogAction.POSITIVE).setEnabled(false);

                        TextView tv = (TextView) materialDialog.findViewById(R.id.tv_masterpwd_msg);
                        tv.setText(act.getString(R.string.dialog_msg_create_masterpwd));

                        EditText etCreate = (EditText) materialDialog.findViewById(R.id.et_masterpwd_login);
                        etCreate.requestFocus();

                        EditText etAcc = (EditText) materialDialog
                                .findViewById(R.id.et_masterpwd_create);

                        etAcc.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                            @Override
                            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                act.createPasswordFired(materialDialog);
                                dialog.dismiss();
                                return true;
                            }

                        });

                        Button btnLogin = (Button) materialDialog.findViewById(R.id.btn_login);
                        btnLogin.setVisibility(View.GONE);

                        Button btnOKCreate = (Button) materialDialog.findViewById(R.id.btn_create);
                        btnOKCreate.setEnabled(false);
                        btnOKCreate.setClickable(false);
                        btnOKCreate.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                act.createPasswordFired(materialDialog);
                            }
                        });

                        act.createShowHidePwdCheckBox(materialDialog, etAcc, etCreate);

                        act.checkPwdLength(materialDialog, etAcc, etCreate, true, btnOKCreate);

                        CheckBox chbx = (CheckBox) materialDialog.findViewById(R.id.cbx_hide_password);
                        chbx.setText(act.getString(R.string.dialog_show_passwords));

                    }
                });

        final MaterialDialog dialog = builder.build();

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        dialog.show();
    }

}
