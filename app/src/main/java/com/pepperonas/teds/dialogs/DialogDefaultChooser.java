package com.pepperonas.teds.dialogs;

import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.pepperonas.teds.R;
import com.pepperonas.teds.activities.CardEditorActivity;
import com.pepperonas.teds.model.FieldType;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class DialogDefaultChooser {

    public DialogDefaultChooser(final CardEditorActivity cea) {
        final String typeItems[] = cea.getResources().getStringArray(R.array.content_description_presets);
        new MaterialDialog.Builder(cea)
                .title(R.string.dialog_title_choose_content)
                .negativeText(R.string.cancel)
                .autoDismiss(true)
                .cancelable(true)
                .items(typeItems)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void
                    onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        switch (which) {
                            case FieldType.TYPE_TEXT:
                                cea.addContent(typeItems[FieldType.TYPE_TEXT], "", CardEditorActivity.FOCUSED);
                                break;

                            case FieldType.TYPE_NUMBER:
                                cea.addContent(typeItems[FieldType.TYPE_NUMBER], "", CardEditorActivity.FOCUSED);
                                break;

                            case FieldType.TYPE_LOGIN:
                                cea.addLoginContent(typeItems[FieldType.TYPE_LOGIN], "", CardEditorActivity.FOCUSED);
                                break;

                            case FieldType.TYPE_PASSWORD:
                                cea.addPwdContent(typeItems[FieldType.TYPE_PASSWORD], "", CardEditorActivity.FOCUSED);
                                break;

                            case FieldType.TYPE_URL:
                                cea.addContent(typeItems[FieldType.TYPE_URL], "", CardEditorActivity.FOCUSED);
                                break;

                            case FieldType.TYPE_MAIL:
                                cea.addContent(typeItems[FieldType.TYPE_MAIL], "", CardEditorActivity.FOCUSED);
                                break;

                            case FieldType.TYPE_PHONE:
                                cea.addContent(typeItems[FieldType.TYPE_PHONE], "", CardEditorActivity.FOCUSED);
                                break;

                            case FieldType.TYPE_PIN:
                                cea.addContent(typeItems[FieldType.TYPE_PIN], "", CardEditorActivity.FOCUSED);
                                break;

                            case FieldType.USERS_CUSTOM:
                                cea.showAddCustomDatapairDialog();
                                break;
                        }
                    }
                })
                .show();
    }
}
