package com.pepperonas.teds.dialogs;

import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.pepperonas.teds.R;
import com.pepperonas.teds.activities.LoginActivity;
import com.pepperonas.teds.utils.Const;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class DialogLogin {

    private static final String TAG = "DialogLogin";

    private boolean mCloseApp = true;


    public DialogLogin(final LoginActivity loginActivity) {
        final MaterialDialog.Builder builder = new MaterialDialog.Builder(loginActivity)
                .title(loginActivity.getString(R.string.dialog_title_pwdlogin))
                .customView(R.layout.dialog_main_login, true)
                .autoDismiss(false)
                .showListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {

                        final MaterialDialog materialDialog = (MaterialDialog) dialog;

                        ((TextView) materialDialog.findViewById(R.id.tv_masterpwd_msg))
                                .setText(loginActivity.getString(R.string.dialog_msg_pwdlogin));

                        final Button btnLogin = (Button) materialDialog.findViewById(R.id.btn_login);
                        btnLogin.setClickable(false);
                        btnLogin.setEnabled(false);
                        btnLogin.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                mCloseApp = false;
                                loginActivity.loginFired(materialDialog);
                            }
                        });

                        LinearLayout horizontalCreate = (LinearLayout) materialDialog.findViewById(R.id.horizontal_create);

                        horizontalCreate.setVisibility(View.GONE);

                        final EditText et = (EditText) materialDialog.findViewById(R.id.et_masterpwd_login);
                        et.setHint(loginActivity.getString(R.string.password));
                        et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                            @Override
                            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                btnLogin.callOnClick();
                                return true;
                            }
                        });

                        if (Const.BYPASS_LOGIN) {
                            mCloseApp = false;
                            et.setText("mm");
                            loginActivity.loginFired(materialDialog);
                        }

                        loginActivity.createShowHidePwdCheckBox(materialDialog, et, null);

                        loginActivity.checkPwdLength(materialDialog, et, null, false, btnLogin);

                    }
                })
                .dismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (mCloseApp) {
                            loginActivity.closeAllActivities();
                            loginActivity.finish();
                        }
                    }
                });

        final MaterialDialog dialog = builder.build();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.show();
    }

}
