package com.pepperonas.teds.dialogs;

import android.content.DialogInterface;

import com.afollestad.materialdialogs.MaterialDialog;
import com.pepperonas.teds.R;
import com.pepperonas.teds.settings.SettingsActivity;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class DialogPremiumFeatures {

    public DialogPremiumFeatures(SettingsActivity act) {
        new MaterialDialog.Builder(act)
                .title(R.string.pref_title_pr_premium)
                .customView(R.layout.dialog_premium_features, true)
                .positiveText(R.string.ok)
                .autoDismiss(true)
                .showListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {

                    }
                })
                .callback(new MaterialDialog.ButtonCallback() {

                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }

                })
                .show();
    }
}
