package com.pepperonas.teds.dialogs;

import android.content.DialogInterface;

import com.afollestad.materialdialogs.MaterialDialog;
import com.pepperonas.teds.R;
import com.pepperonas.teds.activities.LoginActivity;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class DialogFingerprintFailed {

    public static final int SAMSUNG_F_PRINT = 0;
    public static final int GOOGLE_F_PRINT = 1;

    private boolean mCloseApp = true;


    public DialogFingerprintFailed(final LoginActivity loginActivity, String reason, final int fingerPrintMode) {
        new MaterialDialog.Builder(loginActivity)
                .title(loginActivity.getString(R.string.dialog_title_fpl_login_failed))
                .content(reason)
                .positiveText(loginActivity.getString(R.string.yes))
                .neutralText(loginActivity.getString(R.string.password))
                .negativeText(loginActivity.getString(R.string.no))
                .autoDismiss(true)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                        mCloseApp = false;

                        if (fingerPrintMode == SAMSUNG_F_PRINT) {
                            loginActivity.startFingerprintScanner();
                        } else if (fingerPrintMode == GOOGLE_F_PRINT) {
                            loginActivity.startGoogleFingerprintScanner();
                        }
                    }


                    @Override
                    public void onNeutral(MaterialDialog dialog) {
                        // optional Fingerprinter in Einstellungen deaktivieren
                        /*disableFingerprintInPrefs();*/

                        mCloseApp = false;

                        new DialogLogin(loginActivity);
                    }


                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        loginActivity.closeAllActivities();
                        loginActivity.finish();
                    }
                })
                .dismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (mCloseApp) {
                            loginActivity.closeAllActivities();
                            loginActivity.finish();
                        }
                    }
                })
                .show();
    }

}
