package com.pepperonas.teds.dialogs;

import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.pepperonas.andbasx.base.ToastUtils;
import com.pepperonas.teds.R;
import com.pepperonas.teds.activities.ShowCardActivity;
import com.pepperonas.teds.model.Label;
import com.pepperonas.teds.utils.Logic;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class DialogAddNewLabel {

    public DialogAddNewLabel(final ShowCardActivity activity) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(activity)
                .title(R.string.dialog_title_new_label)
                .customView(R.layout.dialog_data_editor, true)
                .positiveText(R.string.ok)
                .negativeText(R.string.cancel)
                .autoDismiss(true)
                .showListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        final MaterialDialog matDia = (MaterialDialog) dialogInterface;
                        final EditText et = (EditText) matDia.findViewById(R.id.et_rename_data);
                        Logic.applyLengthCheck(et, matDia);
                        et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                            @Override
                            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                matDia.getActionButton(DialogAction.POSITIVE).callOnClick();
                                return true;
                            }
                        });

                    }
                })
                .callback(new MaterialDialog.ButtonCallback() {

                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        EditText et = (EditText) dialog.findViewById(R.id.et_rename_data);
                        if (et.getText().toString().isEmpty()) {
                            ToastUtils.toastLong(R.string.toast_label_editor_empty_inputs_are_not_allowed);
                        } else {
                            // in DB einfügen
                            Label label = new Label(0, et.getText().toString(), String.valueOf(System.currentTimeMillis()));
                            activity.getDatabase().createLabel(label);
                            activity.showLabelSelectionDialog();
                        }
                    }


                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }
                });
        MaterialDialog dialog = builder.build();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.show();
    }
}
