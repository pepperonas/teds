package com.pepperonas.teds.dialogs;

import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.pepperonas.teds.R;
import com.pepperonas.teds.activities.AccountManagerActivity;
import com.pepperonas.teds.utils.Logic;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class DialogAccountSetup {

    public DialogAccountSetup(final AccountManagerActivity ama, final String name) {
        final String title;
        if (!name.isEmpty()) {
            title = ama.getString(R.string.rename);
        } else title = ama.getString(R.string.dialog_title_new_account);

        MaterialDialog.Builder builder = new MaterialDialog.Builder(ama)
                .title(title)
                .customView(R.layout.dialog_data_editor, true)
                .positiveText(R.string.ok)
                .negativeText(R.string.cancel)
                .autoDismiss(true)
                .showListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(final DialogInterface dialog) {
                        final MaterialDialog matDia = (MaterialDialog) dialog;
                        final EditText et = (EditText) matDia.findViewById(R.id.et_rename_data);

                        et.setText(name);
                        if (!name.isEmpty()) {
                            et.setSelection(name.length());
                        }

                        if (et.getText().toString().length() < 2) {
                            matDia.getActionButton(DialogAction.POSITIVE).setEnabled(false);
                            matDia.getActionButton(DialogAction.POSITIVE).setClickable(false);
                        }

                        Logic.applyLengthCheck(et, matDia);

                        et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                            @Override
                            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                matDia.getActionButton(DialogAction.POSITIVE).callOnClick();
                                return true;
                            }
                        });
                    }
                })
                .callback(new MaterialDialog.ButtonCallback() {

                    // OK...
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                        EditText et = (EditText) dialog.findViewById(R.id.et_rename_data);

                        String input = et.getText().toString();

                        if (name.isEmpty()) ama.addNewAccount(input);

                        else ama.renameAccount(name, input);
                    }


                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }

                });

        MaterialDialog dialog = builder.build();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.show();
    }
}
