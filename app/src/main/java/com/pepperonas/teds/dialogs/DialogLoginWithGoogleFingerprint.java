package com.pepperonas.teds.dialogs;

import com.afollestad.materialdialogs.MaterialDialog;
import com.pepperonas.teds.R;
import com.pepperonas.teds.activities.LoginActivity;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class DialogLoginWithGoogleFingerprint {

    private MaterialDialog mMaterialDialog;


    public DialogLoginWithGoogleFingerprint(final LoginActivity act) {
        mMaterialDialog = new MaterialDialog.Builder(act)
                .title(R.string.dialog_title_pwdlogin)
                .content(R.string.dialog_msg_login_with_google_fingerprint)
                .progress(true, 0)
                .negativeText(R.string.cancel)
                .autoDismiss(true)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        new DialogFingerprintFailed(
                                act, act.getString(R.string.dialog_msg_fpl_login_failed_ask_retry),
                                DialogFingerprintFailed.GOOGLE_F_PRINT);
                    }
                })
                .show();
    }


    public void dismiss() {
        mMaterialDialog.dismiss();
    }
}
