package com.pepperonas.teds.dialogs;

import com.afollestad.materialdialogs.MaterialDialog;
import com.pepperonas.teds.R;
import com.pepperonas.teds.activities.AccountManagerActivity;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class DialogAddAccount {

    public DialogAddAccount(final AccountManagerActivity ama) {
        new MaterialDialog.Builder(ama)
                .title(R.string.dialog_title_account_loader)
                .content(R.string.dialog_msg_account_loader)
                .positiveText(R.string.ok)
                .negativeText(R.string.cancel)
                .autoDismiss(true)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        ama.writeAccountsIntoDb();
                        ama.getAdapter().refreshAccountList(ama.getDatabase());
                    }


                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }

                })
                .show();
    }
}
