package com.pepperonas.teds.dialogs;

import android.view.WindowManager;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.pepperonas.andbasx.base.ToastUtils;
import com.pepperonas.teds.R;
import com.pepperonas.teds.activities.CardEditorActivity;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class DialogAddCustomDatapair {

    public DialogAddCustomDatapair(final CardEditorActivity cea) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(cea)
                .title(R.string.dialog_title_custom_datapair)
                .customView(R.layout.dialog_custom_datapair_card_editor, true)
                .positiveText(R.string.ok)
                .negativeText(R.string.cancel)
                .autoDismiss(true)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        EditText et = (EditText) dialog
                                .findViewById(R.id.et_custom_datapair_description);

                        if (et.getText().toString().isEmpty()) {
                            ToastUtils.toastLong(R.string.toast_empty_inputs_are_not_allowed);
                        } else {
                            // benutzerdefiniertes Datenpaar hinzufügen
                            cea.addContent(et.getText().toString(), "", CardEditorActivity.FOCUSED);
                        }
                    }


                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }
                });
        MaterialDialog dialog = builder.build();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.show();
    }
}
