package com.pepperonas.teds.dialogs;

import android.content.DialogInterface;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.pepperonas.teds.R;
import com.pepperonas.teds.settings.SettingsActivity;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class DialogExportInfo {

    public DialogExportInfo(final SettingsActivity act) {
        new MaterialDialog.Builder(act)
                .title(R.string.dialog_title_export_success)
                .customView(R.layout.dialog_info, true)
                .positiveText(R.string.ok)
                .autoDismiss(true)
                .showListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        MaterialDialog materialDialog = (MaterialDialog) dialog;
                        TextView tvInfo = (TextView) materialDialog.findViewById(R.id.tv_dialog_info);
                        tvInfo.setText(act.getString(R.string.dialog_msg_export_success));

                        TextView tvInfoAdditional = (TextView) materialDialog.findViewById(R.id.tv_dialog_info_additional);
                        tvInfoAdditional.setText(act.getString(R.string.dialog_msg_export_success_file_location));
                    }
                })
                .callback(new MaterialDialog.ButtonCallback() {

                    @Override
                    public void onPositive(MaterialDialog dialog) {
                    }

                })
                .show();
    }
}
