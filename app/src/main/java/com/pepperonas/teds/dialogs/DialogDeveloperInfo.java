package com.pepperonas.teds.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.mikepenz.community_material_typeface_library.CommunityMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.pepperonas.teds.R;
import com.pepperonas.teds.settings.SettingsActivity;
import com.pepperonas.teds.utils.Const;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class DialogDeveloperInfo {

    public DialogDeveloperInfo(final SettingsActivity act, int color) {
        new MaterialDialog.Builder(act)
                .title(R.string.STG_T_developer)
                .customView(R.layout.dialog_developer, true)
                .positiveText(R.string.OK)
                .autoDismiss(true)
                .icon(new IconicsDrawable(act, CommunityMaterial.Icon.cmd_leaf).colorRes(color).sizeDp(Const.DIALOG_ICON_SIZE))
                .showListener(new MaterialDialog.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        Dialog dialog = (Dialog) dialogInterface;
                        TextView msg1 = (TextView) dialog.findViewById(R.id.dev_dia_tv_01);
                        TextView msg2 = (TextView) dialog.findViewById(R.id.dev_dia_tv_02);
                        TextView msg3 = (TextView) dialog.findViewById(R.id.dev_dia_tv_03);
                        msg1.setText(Html.fromHtml(act.getThemedAppName(false) + " " + act.getString(R.string.u_app_description)));
                        msg1.setMovementMethod(LinkMovementMethod.getInstance());
                        msg2.setText(Html.fromHtml(act.getString(R.string.u_dialog_developer_web_presentation_message)));
                        msg2.setMovementMethod(LinkMovementMethod.getInstance());
                        msg3.setText(Html.fromHtml(act.getString(R.string.u_dialog_developer_translate_message)));
                        msg3.setMovementMethod(LinkMovementMethod.getInstance());
                    }
                })
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                        dialog.dismiss();
                    }
                })
                .show();
    }
}
