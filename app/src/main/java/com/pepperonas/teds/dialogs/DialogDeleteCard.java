package com.pepperonas.teds.dialogs;

import com.afollestad.materialdialogs.MaterialDialog;
import com.pepperonas.teds.R;
import com.pepperonas.teds.activities.ShowCardActivity;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class DialogDeleteCard {

    public DialogDeleteCard(final ShowCardActivity activity) {
        new MaterialDialog.Builder(activity)
                .title(R.string.hint)
                .content(R.string.dialog_msg_delete_card)
                .positiveText(R.string.ok)
                .negativeText(R.string.cancel)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        activity.deleteCard();
                        activity.returnToMain();
                    }


                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }
                })
                .show();
    }
}
