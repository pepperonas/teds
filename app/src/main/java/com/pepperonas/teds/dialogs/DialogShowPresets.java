package com.pepperonas.teds.dialogs;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.transition.Slide;
import android.view.Gravity;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.pepperonas.teds.R;
import com.pepperonas.teds.activities.CardEditorActivity;
import com.pepperonas.teds.activities.MainActivity;
import com.pepperonas.teds.utils.Const;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class DialogShowPresets {

    public DialogShowPresets(final MainActivity act) {
        final String typeItems[] = act.getResources().getStringArray(R.array.card_types);
        new MaterialDialog.Builder(act)
                .title(R.string.dialog_title_add_card_preset)
                .items(typeItems)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void
                    onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {

                        Intent cardCreateIntent = new Intent(act, CardEditorActivity.class);
                        cardCreateIntent.putExtra(Const.BNL_PW, act.getPwd());
                        cardCreateIntent.putExtra(Const.BNL_SHOW_ANIMATION, false);

                        switch (which) {
                            case 0:
                                cardCreateIntent.putExtra(Const.BNL_CARD_PRESET_MODE, Const.BNL_PRESET_ACCOUNT);
                                break;
                            case 1:
                                cardCreateIntent.putExtra(Const.BNL_CARD_PRESET_MODE, Const.BNL_PRESET_MAIL);
                                break;
                            case 2:
                                cardCreateIntent.putExtra(Const.BNL_CARD_PRESET_MODE, Const.BNL_PRESET_CODE);
                                break;
                            case 3:
                                cardCreateIntent.putExtra(Const.BNL_CARD_PRESET_MODE, Const.BNL_PRESET_PIN);
                                break;
                            case 4:
                                cardCreateIntent.putExtra(Const.BNL_CARD_PRESET_MODE, Const.BNL_PRESET_NOTE);
                                break;
                            case 5:
                                cardCreateIntent.putExtra(Const.BNL_CARD_PRESET_MODE, Const.BNL_PRESET_PGP);
                                break;
                        }

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            act.getWindow().setExitTransition(new Slide(Gravity.BOTTOM));
                            act.startActivity(cardCreateIntent, ActivityOptions.makeSceneTransitionAnimation(act).toBundle());

                        } else act.startActivity(cardCreateIntent);
                    }
                })
                .show();
    }
}
