package com.pepperonas.teds.dialogs;

import android.content.DialogInterface;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.mikepenz.community_material_typeface_library.CommunityMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.pepperonas.teds.R;
import com.pepperonas.teds.settings.SettingsActivity;
import com.pepperonas.teds.utils.Logic;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class DialogFaqs {

    public DialogFaqs(SettingsActivity act, int color) {
        final String[] titles = act.getResources().getStringArray(R.array.faqTitles);
        final String[] subTitles = act.getResources().getStringArray(R.array.faqSubTitles);
        final String[] contents = act.getResources().getStringArray(R.array.faqContents);
        final LinearLayout.LayoutParams
                layoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, Logic.mkDiP(act, 24), 0, 0);

        new MaterialDialog.Builder(act)
                .title(R.string.pref_title_pr_faqs)
                .customView(R.layout.dialog_faq, true)
                .positiveText(R.string.ok)
                .icon(new IconicsDrawable(act, CommunityMaterial.Icon.cmd_help_circle).colorRes(color).sizeDp(24))
                .autoDismiss(true)
                .showListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {

                    }
                })
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                    }
                })
                .show();
    }
}
