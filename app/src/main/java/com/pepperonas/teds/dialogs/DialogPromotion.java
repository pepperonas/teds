package com.pepperonas.teds.dialogs;

import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.pepperonas.teds.R;
import com.pepperonas.teds.settings.SettingsActivity;

/**
 * @author Martin Pfeffer (pepperonas)
 */
public class DialogPromotion {

    public DialogPromotion(final SettingsActivity act) {
        new MaterialDialog.Builder(act)
                .title(R.string.dialog_promo_code)
                .customView(R.layout.dialog_promo, true)
                .autoDismiss(false)
                .cancelable(false)
                .showListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(final DialogInterface dialog) {
                        final MaterialDialog matDia = (MaterialDialog) dialog;
                        final EditText etCode = (EditText) matDia.findViewById(R.id.etPromoCode);
                        Button btnOk = (Button) matDia.findViewById(R.id.btnOk);
                        btnOk.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                act.checkPromoCode(etCode, matDia, act.getDatabase());
                            }
                        });
                    }
                })
                .show();
    }
}
