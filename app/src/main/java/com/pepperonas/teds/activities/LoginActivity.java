package com.pepperonas.teds.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseArray;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.pepperonas.andbasx.base.ToastUtils;
import com.pepperonas.teds.R;
import com.pepperonas.teds.model.CryptaAccessor;
import com.pepperonas.teds.model.Database;
import com.pepperonas.teds.dbx.DbxHelper;
import com.pepperonas.teds.dbx.DbxLoaderTask;
import com.pepperonas.teds.dialogs.DialogCreatePassword;
import com.pepperonas.teds.dialogs.DialogFingerprintFailed;
import com.pepperonas.teds.dialogs.DialogLogin;
import com.pepperonas.teds.dialogs.DialogLoginWithGoogleFingerprint;
import com.pepperonas.teds.interfaces.TaskListener;
import com.pepperonas.teds.settings.Pref;
import com.pepperonas.teds.settings.Setup;
import com.pepperonas.teds.utils.AesCryptIV;
import com.pepperonas.teds.utils.App;
import com.pepperonas.teds.utils.Const;
import com.pepperonas.teds.utils.Logic;
import com.pepperonas.teds.utils.Passgen;
import com.samsung.android.sdk.SsdkUnsupportedException;
import com.samsung.android.sdk.pass.Spass;
import com.samsung.android.sdk.pass.SpassFingerprint;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by pepperonas on 28.02.15.
 * LoginActivity
 */
public class LoginActivity
        extends AppCompatActivity
        implements TaskListener {

    public static final String TAG = "Login";

    // Fingerprint
    private SpassFingerprint mSpassFingerprint;

    private FingerprintManager mFingerprintManager;

    /*
     * Einstellungen
     */
    private boolean mIsFingerprintEnabled = false;
    private boolean mIsFingerprintGoogleEnabled = false;
    private boolean mIsDirectLock = true;
    private int mDelayedLock = Integer.parseInt(Pref.DELAYED_LOCK_DEFAULT);

    private boolean mDoNotLock = false;

    private String mPwd;

    private DbxLoaderTask mDbxLoaderTask;

    private BroadcastReceiver mMsgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctx, Intent intent) {
            finish();
        }
    };

    private DialogLoginWithGoogleFingerprint mDialogLoginWithGoogleFingerprint;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        loadConfigPrefs();
        Logic.applyTheme(this);
        supportRequestWindowFeature(Window.FEATURE_PROGRESS);

        super.onCreate(savedInstanceState);

        LocalBroadcastManager.getInstance(this).registerReceiver(mMsgReceiver, new IntentFilter(Const.LBC_INTENT_TO_FINISH));

        setContentView(R.layout.activity_login);

    }


    private void ensureBypass() {
        if (Const.BYPASS_LOGIN) {
            launchAppBypassed();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        App app = (App) this.getApplication();
        app.stopActivityTransitionTimer();

        ensureBypass();

        loginRoutine();
    }


    @Override
    protected void onPause() {
        if (mDbxLoaderTask != null) mDbxLoaderTask.dismissProgressDialog();
        if (!mDoNotLock) {
            if (mIsDirectLock) {
                ((App) this.getApplication()).startActivityTransitionTimer(0);
            } else ((App) this.getApplication()).startActivityTransitionTimer(mDelayedLock);
        }

        if (mDialogLoginWithGoogleFingerprint != null) {
            mDialogLoginWithGoogleFingerprint.dismiss();
        }

        mDoNotLock = false;

        super.onPause();
    }


    @Override
    protected void onDestroy() {
        mPwd = null;
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMsgReceiver);

        super.onDestroy();
    }


    @Override
    public void onPassed(Context ctx, String arg) {
        Log.d(TAG, arg);
        launchApp(ctx);
    }


    @Override
    public void onFailed(Context ctx, String arg) {
        Log.d(TAG, arg);
        launchApp();
    }


    /**
     * Wenn Synchronisation aktiv, erst synchronisieren und per Callback MainActivity starten.
     * Anderenfalls nur MainActivity starten.
     */
    private void ensureSyncAndLaunch() {
        if (Setup.isSyncEnabled()) {
            AndroidAuthSession session = DbxHelper.buildSession();
            DropboxAPI<AndroidAuthSession> dbxApi = new DropboxAPI<AndroidAuthSession>(session);
            mDbxLoaderTask = new DbxLoaderTask(this, dbxApi, true);
            mDbxLoaderTask.setTaskListener(this);
            mDbxLoaderTask.execute("download");
        } else {
            launchApp();
            Log.d(TAG, "Auto Sync deaktiviert (Synchronisierung übersprungen).");
        }
    }


    /**
     * Stellt die zuletzt geöffnete Activity wieder her, wenn die App gesperrt wurde.
     * ACHTUNG: Wenn MainActivity noch nicht vorhanden (also Neustart...), wird diese gestartet.
     */
    public void launchApp() {
        Intent mainActivity = new Intent(LoginActivity.this, MainActivity.class);
        mainActivity.putExtra(Const.BNL_PW, mPwd);
        if (!MainActivity.sIsActive) startActivity(mainActivity);
        finish();
    }


    /**
     * Siehe launchApp(), jedoch wird hier der Context mitübergeben.
     * Somit kann MainActivity auch vom Callback-kommend gestartet werden.
     */
    public void launchApp(Context context) {
        Intent mainActivity = new Intent(context, MainActivity.class);
        mainActivity.putExtra(Const.BNL_PW, mPwd);
        if (!MainActivity.sIsActive) context.startActivity(mainActivity);
        LoginActivity loginActivity = (LoginActivity) context;
        loginActivity.finish();
    }


    /**
     * Stellt die zuletzt geöffnete Activity wieder her, wenn die App gesperrt wurde.
     * ACHTUNG: Wenn MainActivity noch nicht vorhanden (also Neustart...), wird diese gestartet.
     */
    public void launchAppBypassed() {
        Intent mainActivity = new Intent(LoginActivity.this, MainActivity.class);
        mainActivity.putExtra(Const.BNL_PW, Const.BYPASS_PW);
        if (!MainActivity.sIsActive) startActivity(mainActivity);
        finish();
    }


    /**
     * Einstellungen laden.
     */
    private void loadConfigPrefs() {
        SharedPreferences prefs = getSharedPreferences(Pref.CONFIG_FILE, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        if (prefs.contains(Pref.PK_CB_FINGERPRINT)) {
            mIsFingerprintEnabled = prefs.getBoolean(Pref.PK_CB_FINGERPRINT, false);
        } else editor.putBoolean(Pref.PK_CB_FINGERPRINT, false);

        if (prefs.contains(Pref.PK_CB_FINGERPRINT_GOOGLE)) {
            mIsFingerprintGoogleEnabled = prefs.getBoolean(Pref.PK_CB_FINGERPRINT_GOOGLE, false);
        } else editor.putBoolean(Pref.PK_CB_FINGERPRINT_GOOGLE, false);

        if (prefs.contains(Pref.PK_CB_DIRECT_LOCK)) {
            mIsDirectLock = prefs.getBoolean(Pref.PK_CB_DIRECT_LOCK, true);
        } else editor.putBoolean(Pref.PK_CB_DIRECT_LOCK, true);

        if (prefs.contains(Pref.PK_LIST_DELAYED_LOCK)) {
            mDelayedLock = Integer.parseInt(prefs.getString(Pref.PK_LIST_DELAYED_LOCK, Pref.DELAYED_LOCK_DEFAULT));
        } else editor.putString(Pref.PK_LIST_DELAYED_LOCK, Pref.DELAYED_LOCK_DEFAULT);

        if (!prefs.contains(Pref.PK_LIST_AUTO_CLEAR_CLIPBOARD)) {
            editor.putString(Pref.PK_LIST_AUTO_CLEAR_CLIPBOARD, Pref.DELAYED_AUTO_CLEAR_DEFAULT);
        }

        if (!prefs.contains(Pref.PK_CB_SAVE_INSTANT)) editor.putBoolean(Pref.PK_CB_SAVE_INSTANT, false);
        if (!prefs.contains(Pref.PK_CB_OPEN_KEYBOARD)) editor.putBoolean(Pref.PK_CB_OPEN_KEYBOARD, false);
        if (!prefs.contains(Pref.PK_CB_HIDE_RECENTS)) editor.putBoolean(Pref.PK_CB_HIDE_RECENTS, true);
        if (!prefs.contains(Pref.PK_CB_APPLY_DELETE)) editor.putBoolean(Pref.PK_CB_APPLY_DELETE, true);
        if (!prefs.contains(Pref.PK_LIST_THEME)) editor.putString(Pref.PK_LIST_THEME, Pref.LIGHT);

        editor.apply();
    }


    /**
     * Login einleiten.
     */
    public void loginRoutine() {
        boolean isFingerprintAvailable = false;
        boolean isFingerRegistered = false;

        mSpassFingerprint = new SpassFingerprint(LoginActivity.this);
        Spass spass = new Spass();
        try {
            spass.initialize(this);
            isFingerRegistered = mSpassFingerprint.hasRegisteredFinger();
            isFingerprintAvailable = spass.isFeatureEnabled(Spass.DEVICE_FINGERPRINT);

        } catch (SsdkUnsupportedException e) {
            e.printStackTrace();
        }

        if (isFingerprintAvailable
            && isFingerRegistered
            && mIsFingerprintEnabled
            && isMasterPasswordSet()) {

            setFpLBgTransparency(spass);
            mSpassFingerprint.setCanceledOnTouchOutside(false);
            startFingerprintScanner();
        } else if (mIsFingerprintGoogleEnabled && (Build.VERSION.SDK_INT >= 23)) {

            startGoogleFingerprintScanner();

        } else {

            if (isMasterPasswordSet()) {
                // Passwort-Login
                new DialogLogin(this);
            } else {
                // Master-Passwort erstellen
                new DialogCreatePassword(this);
            }
        }
    }


    @SuppressLint({"NewApi", "StringFormatInvalid"})
    public void startGoogleFingerprintScanner() {
        mFingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

        if (!mFingerprintManager.hasEnrolledFingerprints()) {
            ToastUtils.toastShort(getString(R.string.google_fingerprint_requires_at_least_one_finger));
        } else {
            mDialogLoginWithGoogleFingerprint = new DialogLoginWithGoogleFingerprint(this);
            // Speicherung des Fingerprint-Scanners für Login
            if (ActivityCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {

            }
            CancellationSignal cs = new CancellationSignal();

            mFingerprintManager.authenticate(null, cs, 0, new FingerprintManager.AuthenticationCallback() {
                @Override
                public void onAuthenticationError(int errorCode, CharSequence errString) {
                    super.onAuthenticationError(errorCode, errString);
                }


                @Override
                public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
                    super.onAuthenticationHelp(helpCode, helpString);
                }


                @Override
                public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
                    super.onAuthenticationSucceeded(result);
                    mDialogLoginWithGoogleFingerprint.dismiss();

                    SharedPreferences prefs = getSharedPreferences(Const.DATA_FILE, Context.MODE_PRIVATE);

                    mPwd = AesCryptIV.decrypt
                            ("google_fingerPr!nt",
                             prefs.getString(Const.PR_FGOOGLEVALUE, ""),
                             prefs.getString(Const.PR_FGOOGLEVALUE_IV, ""));

                    ensureSyncAndLaunch();
                }


                @Override
                public void onAuthenticationFailed() {
                    super.onAuthenticationFailed();
                    mDialogLoginWithGoogleFingerprint.dismiss();

                    new DialogFingerprintFailed
                            (LoginActivity.this,
                             getString(R.string.dialog_msg_fpl_login_failed_ask_retry),
                             DialogFingerprintFailed.GOOGLE_F_PRINT);


                    Log.d(TAG, "onAuthenticationFailed  " + "");
                }
            }, null);
        }
    }


    /**
     * Fingerprint - SAMSUNG only...
     */
    private SpassFingerprint.IdentifyListener mFpListener =
            new SpassFingerprint.IdentifyListener() {

                @Override
                public void onFinished(int eventStatus) {

                    mSpassFingerprint.cancelIdentify();

                    if (eventStatus == SpassFingerprint.STATUS_AUTHENTIFICATION_SUCCESS) {

                        SharedPreferences prefs = getSharedPreferences(Const.DATA_FILE, Context.MODE_PRIVATE);

                        SparseArray sparseArray = mSpassFingerprint.getRegisteredFingerprintUniqueID();

                        mPwd = AesCryptIV.decrypt
                                (sparseArray.valueAt(0).toString(),
                                 prefs.getString(Const.PR_FVALUE, ""),
                                 prefs.getString(Const.PR_FVALUE_IV, ""));

                        ensureSyncAndLaunch();

                    } else {
                        // Fehler-Behandlung
                        new DialogFingerprintFailed
                                (LoginActivity.this,
                                 getString(R.string.dialog_msg_fpl_login_failed_ask_retry),
                                 DialogFingerprintFailed.SAMSUNG_F_PRINT);
                    }
                }


                @Override
                public void onReady() { }


                @Override
                public void onStarted() { }
            };


    /**
     * Master-Passwort ist zwingend notwendig, um die App starten zu können.
     * False, wenn Nutzer noch kein Master-Passwort gesetzt hat.
     */
    public boolean isMasterPasswordSet() {
        SharedPreferences p = getSharedPreferences(Const.DATA_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor e = p.edit();

        if (p.contains(Const.PR_MASTR_PWD)
            && p.contains(Const.PR_MASTR_PWD_ACC)) {

            if (!p.getString(Const.PR_MASTR_PWD, "unset").equals("")
                && !p.getString(Const.PR_MASTR_PWD_ACC, "unset").equals("")) {
                return true;
            }
        }

        e.putString(Const.PR_MASTR_PWD, "").apply();
        e.putString(Const.PR_MASTR_PWD_ACC, "").apply();
        return false;
    }


    /**
     * Passwort wurde eingegeben und Ok gedrückt.
     */
    public void loginFired(MaterialDialog dialog) {
        EditText et = (EditText) dialog.findViewById(R.id.et_masterpwd_login);
        et.requestFocus();

        Database tmpDatabase = new Database(this, "");
        CryptaAccessor accessor = tmpDatabase.getEncryptedPassword();
        String contentKey = accessor.getPassword();
        String iv = accessor.getIv();

        contentKey = AesCryptIV.decrypt(et.getText().toString(), contentKey, iv);

        CryptaAccessor testDataAccessor = tmpDatabase.getPasswordTestData();
        String testData = testDataAccessor.getPassword();
        String testDataIv = testDataAccessor.getIv();

        String pwdCheck = AesCryptIV.decrypt(contentKey, testData, testDataIv);

        if (Const.TEST_PWD_SOME_VALUE.equals(pwdCheck)) {
            // Passwort richtig
            mPwd = contentKey;
            mDoNotLock = true;
            ensureSyncAndLaunch();
            dialog.dismiss();

        } else {
            // Passwort falsch...
            ToastUtils.toastShort(R.string.toast_pwdlogin_wrong_masterpwd);
        }
    }


    /**
     * neues Passwort erstellen (Eingabe der Passwörter ist erfolgt).
     */
    public void createPasswordFired(MaterialDialog dialog) {
        final SharedPreferences prefs =
                getSharedPreferences(Const.DATA_FILE, Context.MODE_PRIVATE);

        final SharedPreferences.Editor editor = prefs.edit();

        EditText etCreate = (EditText) dialog.findViewById(R.id.et_masterpwd_login);
        EditText etLogin = (EditText) dialog.findViewById(R.id.et_masterpwd_create);

        String inputCreate = etCreate.getText().toString();
        String password = etLogin.getText().toString();

        if (inputCreate.equals(password)) {

            String iv = String.valueOf(System.currentTimeMillis());

            String contentKey = Passgen.generatePassword();

            editor.putString(Const.PR_MASTR_PWD, "...").apply();

            editor.putString(Const.PR_MASTR_PWD_ACC, "...").apply();

            Database db = new Database(this, contentKey);

            db.storeEncryptedPassword(password, contentKey, iv);
            db.storePwdCheckData(contentKey, Const.TEST_PWD_SOME_VALUE, iv);

            mPwd = contentKey;
            mDoNotLock = true;
            launchApp();
            dialog.dismiss();
        }
    }


    /**
     * Passwort-Länge prüfen.
     */
    public void checkPwdLength(
            final MaterialDialog dialog, final EditText et, EditText et2,
            final boolean create, final Button btnOk) {

        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }


            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }


            @Override
            public void afterTextChanged(Editable s) {
                checkPwdLength(s, dialog, create, btnOk);
            }
        });
        if (et2 != null) {
            et2.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) { }


                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) { }


                @Override
                public void afterTextChanged(Editable s) {
                    if (!s.toString().equals(et.getText().toString())) {
                        btnOk.setEnabled(false);
                        btnOk.setClickable(false);
                    } else {
                        checkPwdLength(s, dialog, create, btnOk);
                    }
                }
            });
        }
    }


    /**
     * Checkbox anzeigen um Passwort in Klartext anzuzeigen.
     */
    public void createShowHidePwdCheckBox(
            MaterialDialog materialDialog, final EditText et, final EditText et2) {
        et.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        if (et2 != null) {
            et2.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
        CheckBox cbxHidePwd = (CheckBox) materialDialog.findViewById(R.id.cbx_hide_password);
        cbxHidePwd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    et.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    if (et2 != null) {
                        et2.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    }
                    if (et2 == null) et.setSelection(et.getText().length());
                } else {
                    et.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    if (et2 != null) {
                        et2.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    }
                }
                if (et.isFocused()) et.setSelection(et.getText().length());
                if (et2 != null && et2.isFocused()) et2.setSelection(et2.getText().length());
            }
        });
    }


    private void checkPwdLength(
            Editable s, MaterialDialog materialDialog, boolean create, Button btnOk) {
        if (s.length() > Const.MIN_MASTER_PWD_LENGTH) {
            btnOk.setEnabled(true);
            btnOk.setClickable(true);
        }

        if (s.length() <= Const.MIN_MASTER_PWD_LENGTH + 1) {
            btnOk.setEnabled(false);
            btnOk.setClickable(false);
        }

        if (create) {
            EditText etCreate = (EditText) materialDialog.findViewById(R.id.et_masterpwd_login);
            if (!etCreate.getText().toString().equals(s.toString())) {
                btnOk.setEnabled(false);
                btnOk.setClickable(false);
            }
        }
    }


    /**
     * Schließt alle anderen Activities.
     */
    public void closeAllActivities() {
        Intent intent = new Intent(Const.LBC_INTENT_TO_FINISH);
        intent.putExtra(Const.LBC_MESSAGE, "This is a locked message!");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    private void setFpLBgTransparency(Spass spass) {
        if (spass.isFeatureEnabled(Spass.DEVICE_FINGERPRINT_CUSTOMIZED_DIALOG)) {
            mSpassFingerprint.setDialogBgTransparency(255);
        }
    }


    public void startFingerprintScanner() {
        mSpassFingerprint.startIdentifyWithDialog(LoginActivity.this, mFpListener, false);
    }


    /**
     * Custom-Font über Application laden
     */
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
