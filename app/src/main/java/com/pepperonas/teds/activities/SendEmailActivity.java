package com.pepperonas.teds.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.pepperonas.andbasx.base.ToastUtils;
import com.pepperonas.teds.R;
import com.pepperonas.teds.settings.SettingsActivity;
import com.pepperonas.teds.utils.Const;
import com.pepperonas.teds.utils.Logic;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by pepperonas on 03/30/2015.
 * SendEmailActivity
 */
public class SendEmailActivity extends AppCompatActivity {

    private static final String TAG = "SendEmail";

    private TextView mTvTo;
    private EditText mEtSubject, mEtMessage;
    private String mPwd;
    private boolean mClickedToSend = false;

    private BroadcastReceiver mMsgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctx, Intent intent) {
            finish();
        }
    };

    private BroadcastReceiver mScreenOffReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctx, Intent intent) {
            Logic.ensureShowLoginScreen((Activity) ctx);
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {

        Logic.applyTheme(this);

        Logic.ensureHideFromRecents(this);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_send_email);

        mPwd = getIntent().getStringExtra(Const.BNL_PW);

        toolbarSetup();

        Button btnSend = (Button) findViewById(R.id.send_mail_btn_send);
        mTvTo = (TextView) findViewById(R.id.send_mail_et_pepperonas);
        mEtSubject = (EditText) findViewById(R.id.send_mail_et_subject);
        mEtMessage = (EditText) findViewById(R.id.send_mail_et_message);

        btnSend.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                mClickedToSend = true;

                String to = mTvTo.getText().toString();
                String subject = mEtSubject.getText().toString();
                String message = mEtMessage.getText().toString();

                if (message.length() < 10) {
                    ToastUtils.toastLong(R.string.cant_send_empty_message);
                    return;
                }

                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
                email.putExtra(Intent.EXTRA_SUBJECT, subject);
                email.putExtra(Intent.EXTRA_TEXT, message);

                email.setType("message/rfc822");

                startActivity(Intent.createChooser(email, getString(R.string.mail_act_choose_email_client)));
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(mMsgReceiver, new IntentFilter(Const.LBC_INTENT_TO_FINISH));

        registerReceiver(mScreenOffReceiver, new IntentFilter(Intent.ACTION_SCREEN_OFF));

        if (mClickedToSend) finish();
        Logic.ensureShowLoginScreen(this);
        mClickedToSend = false;
    }


    @Override
    protected void onPause() {
        Logic.ensureShowLoginIfLocked(this);
        Logic.ensureEnableLockTimer(this, this);

        if (mScreenOffReceiver != null) {
            unregisterReceiver(mScreenOffReceiver);
        }

        super.onPause();
    }


    @Override
    protected void onDestroy() {
        mPwd = null;
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMsgReceiver);

        super.onDestroy();
    }


    @Override
    public void startActivity(Intent intent) {
        if (!mClickedToSend) {
            intent = new Intent(this, SettingsActivity.class);
            intent.putExtra(Const.BNL_PW, mPwd);
        }

        super.startActivity(intent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Intent backToSettings = new Intent(SendEmailActivity.this, SettingsActivity.class);
        backToSettings.putExtra(Const.BNL_PW, mPwd);
        startActivity(backToSettings);
    }


    private Toolbar toolbarSetup() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_sendemail);
        Logic.themeToolbar(this, toolbar, null);
        TextView tvToolbar = (TextView) findViewById(R.id.tv_toolbar_title_toolbar);
        tvToolbar.setText(getString(R.string.toolbar_title_contact));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return toolbar;
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
