package com.pepperonas.teds.activities;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.transition.Slide;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.pepperonas.aesprefs.AesPrefs;
import com.pepperonas.andbasx.base.ToastUtils;
import com.pepperonas.andbasx.system.AppUtils;
import com.pepperonas.jbasx.div.MaterialColor;
import com.pepperonas.teds.R;
import com.pepperonas.teds.adapter.NavDrawerAdapter;
import com.pepperonas.teds.model.Card;
import com.pepperonas.teds.model.CardIcon;
import com.pepperonas.teds.model.DataPair;
import com.pepperonas.teds.model.Database;
import com.pepperonas.teds.model.Label;
import com.pepperonas.teds.dbx.DbxHelper;
import com.pepperonas.teds.dbx.DbxLoaderTask;
import com.pepperonas.teds.dialogs.DialogShowPresets;
import com.pepperonas.teds.fragments.CardListFragment;
import com.pepperonas.teds.fragments.NavDrawerFragment;
import com.pepperonas.teds.interfaces.DatabaseWatchdog;
import com.pepperonas.teds.interfaces.TaskListener;
import com.pepperonas.teds.settings.Pref;
import com.pepperonas.teds.settings.SettingsActivity;
import com.pepperonas.teds.settings.Setup;
import com.pepperonas.teds.utils.App;
import com.pepperonas.teds.utils.Const;
import com.pepperonas.teds.utils.Logic;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by pepperonas on 04.02.15.
 * MainActivity
 */
public class MainActivity
        extends AppCompatActivity
        implements View.OnClickListener,
                   DatabaseWatchdog,
                   TaskListener {

    public static final String TAG = "Main";

    static boolean sIsActive = false;

    private Toolbar mToolbar;
    private TextView mTvToolbar;

    private static CardListFragment mCardListFragment;

    private static List<Label> mLabelList = new ArrayList<Label>();
    private static ListView mNavListView;
    private static NavDrawerFragment mNavDrawerFragment;
    private static NavDrawerAdapter mNavDrawerAdapter;

    private static Database mDatabase;

    private Bundle mArgs;

    private String mPwd;

    private BroadcastReceiver mMsgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctx, Intent intent) {
            finish();
        }
    };

    private BroadcastReceiver mScreenOffReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctx, Intent intent) {
            Logic.ensureShowLoginScreen((Activity) ctx);
        }
    };

    private boolean mWriteOnlyOnce = true;

    private DbxLoaderTask mDbxLoaderTask;

    private DropboxAPI<AndroidAuthSession> mDbxApi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Logic.applyTheme(this);

        supportRequestWindowFeature(Window.FEATURE_PROGRESS);

        super.onCreate(savedInstanceState);

        Logic.ensureHideFromRecents(this);

        setContentView(R.layout.activity_main);

        mToolbar = toolbarSetup();
        mPwd = getIntent().getStringExtra(Const.BNL_PW);
        mDatabase = new Database(this, mPwd);
        mArgs = new Bundle();

        loadNavDrawer();

        navDrawerSetup();

        // nur beim Starten der App einmal die Liste automatisch laden
        ensureFirstTransaction(savedInstanceState);

        ensureDbxConnection();
    }


    private void ensureDbxConnection() {
        if (Setup.isSyncEnabled()) {
            AndroidAuthSession session = DbxHelper.buildSession();
            mDbxApi = new DropboxAPI<>(session);

            mDbxLoaderTask = new DbxLoaderTask(this, mDbxApi, true);
            mDbxLoaderTask.setTaskListener(this);
        }
    }


    private void ensureSync(boolean showProgress) {
        if (Setup.isSyncEnabled() && mDbxLoaderTask != null) {
            try {
                mDbxLoaderTask = new DbxLoaderTask(this, mDbxApi, showProgress);
                mDbxLoaderTask.setTaskListener(this);
                mDbxLoaderTask.execute("upload");
            } catch (Exception e) {
                Log.e(TAG, "ensureSync " + e.getMessage());
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        Logic.ensureShowLoginScreen(this);

        LocalBroadcastManager.getInstance(this).registerReceiver(mMsgReceiver, new IntentFilter(Const.LBC_INTENT_TO_FINISH));

        registerReceiver(mScreenOffReceiver, new IntentFilter(Intent.ACTION_SCREEN_OFF));

        sIsActive = true;

        // drin lassen, oder rausnehmen?
        if (mDatabase == null) mDatabase = new Database(this, mPwd);

        if (!mDatabase.getIsPremium()) {
            mDatabase.setVersionInfoAndPremiumState(AppUtils.getVersionName(), "false");
        } else mDatabase.setVersionInfoAndPremiumState(AppUtils.getVersionName(), "true");

        loadNavDrawer();

        // nach Wiedereintritt Änderungen übernehmen
        if (mNavDrawerAdapter != null) mNavDrawerAdapter.notifyDataSetChanged();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setExitTransition(null);
        }

        if (mCardListFragment != null && mCardListFragment.getAdapter() != null) {
            mCardListFragment.getAdapter().notifyDataSetChanged();
        }
    }


    @Override
    protected void onPause() {
        if (mDbxLoaderTask != null) {
            ensureSync(false);
            mDbxLoaderTask.dismissProgressDialog();
        }
        mDatabase.close();

        Logic.ensureShowLoginIfLocked(this);
        Logic.ensureEnableLockTimer(this, this);

        if (mScreenOffReceiver != null) {
            unregisterReceiver(mScreenOffReceiver);
        }

        super.onPause();
    }


    @Override
    protected void onDestroy() {
        mDatabase.close();
        mPwd = null;
        sIsActive = false;
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMsgReceiver);

        super.onDestroy();
    }


    protected void onActivityResult(int request, int result, Intent data) {
        switch (request) {
            case Const.CHANGE_THEME_REQUEST_CODE:
                if (result == Const.RESULT_THEME_CHANGED) {
                    Intent restartIntent = getIntent();
                    restartIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(restartIntent);
                    finish();
                }
                break;
        }
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        if (mNavDrawerFragment != null) mNavDrawerFragment.toggleDrawerSyncState();
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.fab_add_card: {
                if (!mDatabase.getIsPremium() && hasReachedMaxCardsInFreeVersion()) {
                    return;
                }
                showCardPresets();
                break;
            }

            case R.id.navdr_img_btn_edit_labels: {
                startLabelEditorActivity();
                break;
            }

            case R.id.navdr_img_btn_sync: {
                if (Setup.isSyncEnabled()) {
                    ensureSync(true);
                } else ToastUtils.toastShort(R.string.toast_sync_not_registered_check_prefs);
                break;
            }

            case R.id.navdr_img_btn_lock: {
                lockApp();
                break;
            }

            case R.id.navdr_img_btn_settings: {
                startSettingsActivity(true);
                break;
            }
        }
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        SharedPreferences prefs = getSharedPreferences(Pref.CONFIG_FILE, MODE_PRIVATE);

        MenuItem item = null;
        switch (prefs.getInt(Pref.PK_SORT_LIST, 0)) {
            case Pref.SORT_BY_ALPHA_A_Z:
                item = menu.findItem(R.id.menu_sort_by_name_a_z);
                break;
            case Pref.SORT_BY_ALPHA_Z_A:
                item = menu.findItem(R.id.menu_sort_by_name_z_a);
                break;
            case Pref.SORT_BY_LAST_EDIT_NEW_ON_TOP:
                item = menu.findItem(R.id.menu_sort_by_last_edit_new_on_top);
                break;
            case Pref.SORT_BY_LAST_EDIT_OLD_ON_TOP:
                item = menu.findItem(R.id.menu_sort_by_last_edit_old_on_top);
                break;
        }
        if (item != null) item.setChecked(true);

        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        final SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String s) {
                if (mCardListFragment != null) searchIt(s);
                return true;
            }


            @Override
            public boolean onQueryTextChange(String s) {
                if (mNavDrawerFragment != null) mNavDrawerFragment.closeDrawer();
                if (mCardListFragment != null) searchIt(s);
                return true;
            }
        });

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        item.setChecked(true);

        switch (item.getItemId()) {

            case R.id.menu_sort_by_name_a_z:
                AesPrefs.putInt(Pref.PK_SORT_LIST, Pref.SORT_BY_ALPHA_A_Z);
                break;

            case R.id.menu_sort_by_name_z_a:
                AesPrefs.putInt(Pref.PK_SORT_LIST, Pref.SORT_BY_ALPHA_Z_A);
                break;

            case R.id.menu_sort_by_last_edit_new_on_top:
                AesPrefs.putInt(Pref.PK_SORT_LIST, Pref.SORT_BY_LAST_EDIT_NEW_ON_TOP);
                break;

            case R.id.menu_sort_by_last_edit_old_on_top:
                AesPrefs.putInt(Pref.PK_SORT_LIST, Pref.SORT_BY_LAST_EDIT_OLD_ON_TOP);
                break;
        }

        if (mCardListFragment != null && mCardListFragment.getAdapter() != null) {
            mCardListFragment.getAdapter().sort();
            mCardListFragment.initializeTable();

        } else System.out.println("-> CardListFragment.getAdapter() == NULL!!");

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDataChanged(Context ctx) {
        Log.d(TAG, "onDataChanged " + "" + String.format("-> %s. (%s.java:%d)", getClass().getName(), getClass().getSimpleName(), 328));
        if (mDbxLoaderTask != null) {
            ensureSync(false);
        }
    }


    @Override
    public void onPassed(Context ctx, String arg) {
        Log.d(TAG, "onPassed " + "" + String.format("-> %s. (%s.java:%d)", getClass().getName(), getClass().getSimpleName(), 334));
    }


    @Override
    public void onFailed(Context ctx, String arg) {
        Log.d(TAG, "onFailed " + "" + String.format("-> %s. (%s.java:%d)", getClass().getName(), getClass().getSimpleName(), 341));
    }


    private Toolbar toolbarSetup() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        Logic.themeToolbar(this, toolbar, null);

        mTvToolbar = (TextView) findViewById(R.id.tv_toolbar_title_main);
        mTvToolbar.setText(getString(R.string.toolbar_default_title));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        return toolbar;
    }


    private void navDrawerSetup() {
        mNavDrawerFragment = (NavDrawerFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_navigation_drawer);

        mNavDrawerFragment.setUp
                (R.id.fragment_navigation_drawer,
                 (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);

        mNavListView = (ListView) findViewById(R.id.left_drawer);

        mNavDrawerAdapter = new NavDrawerAdapter(this, mLabelList);

        mNavListView.setAdapter(mNavDrawerAdapter);
        mNavListView.setOnItemClickListener(new DrawerItemClickListener());
    }


    private void startLabelEditorActivity() {
        mNavDrawerFragment.closeDrawer();
        Intent labelEditorIntent = new Intent(this, LabelEditorActivity.class);
        labelEditorIntent.putExtra(Const.BNL_PW, mPwd);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setExitTransition(new Explode());
            startActivity(labelEditorIntent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else startActivity(labelEditorIntent);
    }


    private void startCardEditorActivity() {
        Intent cardEditorIntent = new Intent(this, CardEditorActivity.class);
        cardEditorIntent.putExtra(Const.BNL_PW, mPwd);
        cardEditorIntent.putExtra(Const.BNL_SHOW_ANIMATION, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            MainActivity.this.getWindow().setExitTransition(new Slide(Gravity.BOTTOM));
            startActivityForResult(
                    cardEditorIntent,
                    Const.ADD_REQUEST,
                    ActivityOptions.makeSceneTransitionAnimation(this).toBundle());

        } else startActivityForResult(cardEditorIntent, Const.ADD_REQUEST);
    }


    private void startSettingsActivity(boolean explode) {
        mNavDrawerFragment.closeDrawer();
        Intent settingsIntent = new Intent(new Intent(this, SettingsActivity.class));
        settingsIntent.putExtra(Const.BNL_PW, mPwd);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setExitTransition(explode ? new Explode()
                                                  : new Slide(Gravity.BOTTOM));
            startActivityForResult
                    (settingsIntent,
                     Const.CHANGE_THEME_REQUEST_CODE,
                     ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else startActivityForResult(settingsIntent, Const.CHANGE_THEME_REQUEST_CODE);
    }


    public void collectLogicalLabelSizes() {
        int[] logicalLabelEntries = new int[4];

        logicalLabelEntries[Const.ALL_CARDS] = mDatabase.countAllCards();
        logicalLabelEntries[Const.FAVORITES] = mDatabase.countFavoriteCards();
        logicalLabelEntries[Const.UNLABELED] = mDatabase.countUnlabeledCards();
        logicalLabelEntries[Const.WEAK_PASS] = mDatabase.countWeakPasswords();

        mNavDrawerAdapter.updateLogicalLabels(logicalLabelEntries);
    }


    private void showCardPresets() {
        new DialogShowPresets(this);
    }


    public String getPwd() {
        return mPwd;
    }


    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void
        onItemClick(AdapterView parent, View view, int position, long id) {
            navDrawerItemClicked(position);
        }

    }


    /**
     * Label im Navigation Drawer ausgewählt.
     */
    private void navDrawerItemClicked(int position) {

        String labelName = mLabelList.get(position).getName();

        if (labelName.equals(getString(R.string.action_settings))) {
            startSettingsActivity(true);
            return;
        } else if (labelName.equals(getString(R.string.action_sync))) {
            // neu: showProgress = true
            if (Setup.isSyncEnabled()) {
                ensureSync(true);
            } else ToastUtils.toastShort(R.string.toast_sync_not_registered_check_prefs);
            return;
        } else if (labelName.equals(getString(R.string.action_lock))) {
            lockApp();
            return;
        } else if (labelName.equals(getString(R.string.action_edit_label))) {
            startLabelEditorActivity();
            return;
        } else if (selectedLabelWithoutContent(position)) {
            ToastUtils.toastShort(R.string.no_cards_found);
            return;
        }

        mCardListFragment = new CardListFragment();

        if (mTvToolbar != null) mTvToolbar.setText(mLabelList.get(position).getName());

        mNavListView.setItemChecked(position, true);

        mNavDrawerAdapter.markItemAsSelected(position);

        mArgs = new Bundle();
        mArgs.putString(Const.BNL_PW, mPwd);
        mArgs.putString(Const.BNL_FAVORITE_OBJECT_ID, String.valueOf(mLabelList.get(position).getName()));

        mArgs.putString(Const.BNL_FAVORITE_OBJECT_IV, String.valueOf(mLabelList.get(position).getIV()));

        mCardListFragment.setArguments(mArgs);

        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();

        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.main_frame_container, mCardListFragment);
        fragmentTransaction.commit();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mNavDrawerFragment.closeDrawer();
            }
        }, Const.NAVDRAWER_CLOSING_DELAY);
    }


    private boolean selectedLabelWithoutContent(int position) {
        return mLabelList.get(position).getSize() <= 0;
    }


    private boolean searchIt(String s) {

        mCardListFragment = new CardListFragment();
        Bundle args = new Bundle();
        args.putString(Const.BNL_PW, mPwd);
        args.putString(Const.BNL_FAVORITE_OBJECT_ID, Const.SEARCH_MASK);
        args.putString(Const.BNL_SEARCH_ID, s);

        mCardListFragment.setArguments(args);

        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();

        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.main_frame_container, mCardListFragment);
        fragmentTransaction.commit();
        return false;
    }


    private boolean hasReachedMaxCardsInFreeVersion() {
        if (mDatabase.countAllCards() > Const.MAX_AMOUNT_CARDS_IN_FREE_VERSION) {
            ToastUtils.toastShort(R.string.toast_get_pro_to_unlock_unlimited_cards);
            return true;
        }
        return false;
    }


    private void lockApp() {
        ((App) this.getApplication()).startActivityTransitionTimer(0);
        closeAllActivities();
    }


    private void closeAllActivities() {
        Intent intent = new Intent(Const.LBC_INTENT_TO_FINISH);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    public void loadNavDrawer() {
        long startTotal = System.currentTimeMillis();

        mLabelList.clear();

        String[] protectedLabels = getResources().getStringArray(R.array.logical_label);

        /*
         * "Alle Karten" & "Favoriten" anfügen
         */
        Label allCards = new Label(0, protectedLabels[Const.ALL_CARDS], String.valueOf(System.currentTimeMillis()));
        allCards.setSize(mDatabase.countAllCards());
        mLabelList.add(allCards);

        Label favorites = new Label(0, protectedLabels[Const.FAVORITES], String.valueOf(System.currentTimeMillis()));
        favorites.setSize(mDatabase.countFavoriteCards());
        mLabelList.add(favorites);

        if ((isFirstAppStart() || isDatabaseEmpty()) && mWriteOnlyOnce) writeSomeDefaultData();

        /*
         * Benutzerdefinierte Labels mittels Liste aus DB kopieren und anfügen
         */
        List<Label> tmpLabelList = mDatabase.getAllLabels();
        for (Label label : tmpLabelList) {
            label.setSize(mDatabase.countAllCardsByLabel(label));
            mLabelList.add(label);
        }

        /*
         * "Ohne Label" und "schwaches Passwort"-Label an den Drawer anfügen
         */
        Label unlabeled = new Label
                (0, protectedLabels[Const.UNLABELED], String.valueOf(System.currentTimeMillis()));

        unlabeled.setSize(mDatabase.countUnlabeledCards());
        mLabelList.add(unlabeled);

        Label weakPasswords = new Label
                (0, protectedLabels[Const.WEAK_PASS], String.valueOf(System.currentTimeMillis()));

        weakPasswords.setSize(mDatabase.countWeakPasswords());
        mLabelList.add(weakPasswords);

        if (mNavDrawerAdapter == null) mNavDrawerAdapter = new NavDrawerAdapter(this, mLabelList);

        mNavDrawerAdapter.notifyDataSetChanged();

        Log.i(TAG, "loadNavDrawer (" + (System.currentTimeMillis() - startTotal) + "ms)");
    }


    private void addActionIconsWithText() {
        mLabelList.add(new Label(0, getString(R.string.action_edit_labels), Logic.getIV()));
        mLabelList.add(new Label(0, getString(R.string.action_sync), Logic.getIV()));
        mLabelList.add(new Label(0, getString(R.string.action_lock), Logic.getIV()));
        mLabelList.add(new Label(0, getString(R.string.action_settings), Logic.getIV()));
    }


    private boolean isDatabaseEmpty() {
        return (mDatabase.countAllCards() == 0
                && mDatabase.countAllLabels() == 0
                && mDatabase.countAllDataPairs() == 0);
    }


    public void ensureFirstTransaction(Bundle savedInstanceState) {
        if (savedInstanceState != null) return;
        mCardListFragment = new CardListFragment();

        Bundle args = new Bundle();
        args.putString(Const.BNL_PW, mPwd);
        args.putInt(Const.OVERVIEW_MODE, Const.BNL_OVERVIEW_MODE_ALL);

        if (mTvToolbar != null) mTvToolbar.setText(mLabelList.get(0).getName());

        mCardListFragment.setArguments(args);

        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction
                fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.main_frame_container, mCardListFragment);
        fragmentTransaction.commit();
    }


    private void writeSomeDefaultData() {
        Log.i(TAG, "WRITING SOME DEFAULT DATA");

        mWriteOnlyOnce = false;

        String[] customDefaultLabels = getResources().getStringArray(R.array.custom_default_labels);

        Label important = new Label(2, customDefaultLabels[Const.IMPORTANT], String.valueOf(System.currentTimeMillis()));
        long label_important = mDatabase.createLabel(important);

        Label online = new Label(2, customDefaultLabels[Const.ONLINE], String.valueOf(System.currentTimeMillis()));
        long label_online = mDatabase.createLabel(online);

        Label shop = new Label(2, customDefaultLabels[Const.SHOP], String.valueOf(System.currentTimeMillis()));
        long label_shop = mDatabase.createLabel(shop);

        Label money = new Label(2, customDefaultLabels[Const.MONEY], String.valueOf(System.currentTimeMillis()));
        long label_money = mDatabase.createLabel(money);

        // Amazon
        long card_amazon = mDatabase.createCard(
                new Card(getString(R.string.preset_amazon), false, new CardIcon
                        (MaterialColor.YELLOW_A400 + Const.ICON_DIVIDER + "faw_shopping_cart" + Const.ICON_DIVIDER + CardIcon.SymbolColor.BLACK)));

        long login_amazon = mDatabase.createDataPair(
                new DataPair(getString(R.string.login), "TestAccount@gmail.com"));

        long pwd_amazon = mDatabase.createDataPair(
                new DataPair(getString(R.string.password), "2!3<=causalr*"));

        long note_amazon = mDatabase.createDataPair(
                new DataPair(getString(R.string.note), "Beispiel-Notiz"));

        mDatabase.createDataPairLink(card_amazon, login_amazon);
        mDatabase.createDataPairLink(card_amazon, pwd_amazon);
        mDatabase.createDataPairLink(card_amazon, note_amazon);
        mDatabase.createLabelLink(card_amazon, label_shop);

        // Google
        long card_google = mDatabase.createCard(
                new Card(getString(R.string.preset_google), true, new CardIcon
                        (MaterialColor.TEAL_A700 + Const.ICON_DIVIDER + "faw_google" + Const.ICON_DIVIDER + CardIcon.SymbolColor.WHITE)));

        long login_google = mDatabase.createDataPair(
                new DataPair(getString(R.string.login), "MaxMustermann@gmail.com"));

        long pwd_google = mDatabase.createDataPair(
                new DataPair(getString(R.string.password), "cW8=1WriterBloomIcema"));

        mDatabase.createDataPairLink(card_google, login_google);
        mDatabase.createDataPairLink(card_google, pwd_google);
        mDatabase.createLabelLink(card_google, label_important);
        mDatabase.createLabelLink(card_google, label_online);

        // Facebook
        long card_facebook = mDatabase.createCard(
                new Card(getString(R.string.preset_facebook), false, new CardIcon
                        (MaterialColor.INDIGO_A400 + Const.ICON_DIVIDER + "faw_facebook" + Const.ICON_DIVIDER + CardIcon.SymbolColor.WHITE)));

        long login_facebook = mDatabase.createDataPair(
                new DataPair(getString(R.string.login), "ErikaMustermann@facebook.com"));

        long pwd_facebook = mDatabase.createDataPair(
                new DataPair(getString(R.string.password), "25&;Shoo2ObOe{*"));

        mDatabase.createDataPairLink(card_facebook, login_facebook);
        mDatabase.createDataPairLink(card_facebook, pwd_facebook);
        mDatabase.createLabelLink(card_facebook, label_online);

        if (Const.FILL_LIST) fillTestList();
    }


    private boolean isFirstAppStart() {
        return !Logic.ensureWritePref(this, Const.PR_FIRST_START, Const.SOME_DEFAULT_X);
    }


    /**
     * Custom-Font über Application laden.
     */
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    public Database getDatabase() {
        return mDatabase;
    }


    private void fillTestList() {
        for (int i = 0; i < 330; i++) {
            Card card = new Card("Muster (" + String.format("%03d", i) + ")", false, new CardIcon(CardIcon.getUnset()));
            mDatabase.createCard(card);
        }
    }

}

