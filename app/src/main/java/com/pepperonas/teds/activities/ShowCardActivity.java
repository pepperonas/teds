package com.pepperonas.teds.activities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.pepperonas.andbasx.animation.FadeAnimation;
import com.pepperonas.andbasx.base.Loader;
import com.pepperonas.andbasx.base.ToastUtils;
import com.pepperonas.andbasx.graphic.DrawableCircleLetter;
import com.pepperonas.jbasx.base.BooleanUtils;
import com.pepperonas.jbasx.base.TextUtils;
import com.pepperonas.jbasx.color.ColorUtils;
import com.pepperonas.jbasx.div.MaterialColor;
import com.pepperonas.teds.R;
import com.pepperonas.teds.adapter.ShowCardListAdapter;
import com.pepperonas.teds.broadcastreceiver.ClipboardReceiverLogin;
import com.pepperonas.teds.broadcastreceiver.ClipboardReceiverPassword;
import com.pepperonas.teds.model.Card;
import com.pepperonas.teds.model.CardIcon;
import com.pepperonas.teds.model.DataPair;
import com.pepperonas.teds.model.Database;
import com.pepperonas.teds.model.Label;
import com.pepperonas.teds.dialogs.DialogAddNewLabel;
import com.pepperonas.teds.dialogs.DialogDeleteCard;
import com.pepperonas.teds.fragments.color_chooser_fragment.icon_chooser_fragment.FragmentColorChooser;
import com.pepperonas.teds.fragments.logo_chooser_fragment.FragmentSymbolChooser;
import com.pepperonas.teds.interfaces.IconChangedListener;
import com.pepperonas.teds.settings.Pref;
import com.pepperonas.teds.utils.Const;
import com.pepperonas.teds.utils.Logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ShowCardActivity
        extends AppCompatActivity
        implements View.OnClickListener,
                   IconChangedListener {

    private static final String TAG = "ShowCard";
    public static final int SYMBOL_SIZE = 40;
    public static final int DIAMETER_DP = 48;

    private Context mCtx;

    private Database mDatabase;

    private Card mCard;

    private FragmentColorChooser mFragmentColorChooser;
    private FragmentSymbolChooser mFragmentSymbolChooser;

    private String mPwd;

    private BroadcastReceiver mMsgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctx, Intent intent) {
            finish();
        }
    };

    private BroadcastReceiver mScreenOffReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctx, Intent intent) {
            Logic.ensureShowLoginScreen((Activity) ctx);
        }
    };

    private ListView mListView;
    private ShowCardListAdapter mAdapter;

    private AdView mAdView;
    private String mOldIcon;

    private FloatingActionMenu mFabMenu;
    private TextView mTvLastEdit;
    private Toolbar mToolbar;
    private RelativeLayout mToolbarExtension;

    private int mToolbarColor, mStatusbarColor;
    private TextView mTvToolbarTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (getIntent().getBooleanExtra(Const.BNL_SHOW_ANIMATION, false)) {
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }

        Logic.applyTheme(this);
        Logic.ensureHideFromRecents(this);

        super.onCreate(savedInstanceState);

        mCtx = this;

        setContentView(R.layout.activity_show_card);

        mPwd = getIntent().getStringExtra(Const.BNL_PW);

        mDatabase = new Database(this, mPwd);
        mCard = mDatabase.getCard(getIntent().getIntExtra(Const.BNL_CARD_ID, 0));
        mOldIcon = mCard.getRawIcon();

        mListView = (ListView) findViewById(R.id.show_card_list);

        displayWhenCardWasEdited();

        initToolbar();

        initFab();

        loadContent();

        Logic.ensureShowLoginScreen(this);

        if (getIntent().getBooleanExtra(Const.BNL_FROM_MAIN, false)) {
            Logic.ensureShowHint(this, Const.SHOW_ICON_HINT, getString(R.string.hint_change_icon), 3);
        }

        FrameLayout interceptorFrame = (FrameLayout) findViewById(R.id.fl_interceptor);
        interceptorFrame.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d(TAG, "onTouch  " + "");
                if (mFabMenu.isOpened()) {
                    mFabMenu.close(true);
                    return true;
                }
                return false;
            }
        });
    }


    private void initFab() {

        mFabMenu = (FloatingActionMenu) findViewById(R.id.fab_menu);

        FloatingActionButton fabChooseColor = (FloatingActionButton) findViewById(R.id.fab_action_choose_color);
        FloatingActionButton fabChooseSymbol = (FloatingActionButton) findViewById(R.id.fab_action_choose_symbol);
        final FloatingActionButton fabChangeSymbolColor = (FloatingActionButton) findViewById(R.id.fab_action_change_symbol_color);

        final FadeAnimation fadeAnimationRelativeLayout =
                new FadeAnimation(mListView, 1.0f, 0.1f, 400L, 0);
        final FadeAnimation fadeAnitmationTextView =
                new FadeAnimation(mTvLastEdit, 1.0f, 0.1f, 400L, 0);

        mFabMenu.setOnMenuToggleListener(new FloatingActionMenu.OnMenuToggleListener() {
            @Override
            public void onMenuToggle(boolean opened) {
                if (opened) {
                    fadeAnimationRelativeLayout.fadeOut();
                    fadeAnitmationTextView.fadeOut();
                } else {
                    fadeAnimationRelativeLayout.fadeIn();
                    fadeAnitmationTextView.fadeIn();
                }
            }
        });

        fabChooseColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showColorChooserDialog();
                mFabMenu.close(true);
            }
        });
        fabChooseSymbol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showIconChooserDialog();
                mFabMenu.close(true);
            }
        });
        fabChangeSymbolColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCard.getCardIcon().getSymbolColor() == CardIcon.SymbolColor.BLACK) {
                    mCard.getCardIcon().setSymbolColor(CardIcon.SymbolColor.WHITE);
                } else {
                    mCard.getCardIcon().setSymbolColor(CardIcon.SymbolColor.BLACK);
                }
                mDatabase.updateCard(mCard);
                mFabMenu.close(true);
                updateFab();
            }
        });

        checkInitialColor(mCard.getCardIcon().getColor());
        updateFab();
        applyCardTheme();
    }


    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(mMsgReceiver, new IntentFilter(Const.LBC_INTENT_TO_FINISH));

        registerReceiver(mScreenOffReceiver, new IntentFilter(Intent.ACTION_SCREEN_OFF));

        Logic.ensureShowLoginScreen(this);
        mPwd = getIntent().getStringExtra(Const.BNL_PW);

        if (!mDatabase.getIsPremium()) loadAds();
        else ensureDisposeAds();
    }


    @Override
    protected void onPause() {
        Logic.ensureShowLoginIfLocked(this);
        Logic.ensureEnableLockTimer(this, this);
        if (!mCard.getRawIcon().equals(mOldIcon)) {
            System.out.println("Symbol Changed!");
            broadcastIconChanged();
        }
        mDatabase.close();

        if (mScreenOffReceiver != null) {
            unregisterReceiver(mScreenOffReceiver);
        }

        super.onPause();
    }


    @Override
    protected void onDestroy() {
        mPwd = null;
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMsgReceiver);

        super.onDestroy();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 13:
                if (resultCode == RESULT_OK) loadContent();
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_show_card, menu);

        if (mCard.isFavorite()) {
            menu.findItem(R.id.action_favorite_card).setIcon(R.drawable.star);
        } else menu.findItem(R.id.action_favorite_card).setIcon(R.drawable.unstar_white);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                setResult(Const.SOME_RESULT);
                super.onBackPressed();
                return true;

            case R.id.action_copy_to_notificationbar:
                ToastUtils.toastShort(R.string.toast_hint_login_copied_to_notificationbar);
                addCredentialsToNotificationBar(true);
                addCredentialsToNotificationBar(false);
                break;

            case R.id.action_edit_card:
                Intent editCardIntent = makeEditCardIntent();
                startActivity(editCardIntent);
                finish();
                return super.onOptionsItemSelected(item);

            case R.id.action_favorite_card:
                if (mCard.isFavorite()) {
                    mCard.setFavorite(Card.IS_NOT_FAVORITE);
                    toggleFavoriteIcon(item, Card.IS_NOT_FAVORITE);
                } else {
                    mCard.setFavorite(Card.IS_FAVORITE);
                    toggleFavoriteIcon(item, Card.IS_FAVORITE);
                }
                changeFavoriteStatus();
                break;

            case R.id.action_set_lable:
                showLabelSelectionDialog();
                break;

            case R.id.action_send_card:
                sendCard();
                break;

            case R.id.action_delete_card:
                SharedPreferences prefs = getSharedPreferences(Pref.CONFIG_FILE, MODE_PRIVATE);
                if (prefs.getBoolean(Pref.PK_CB_APPLY_DELETE, true)) showDeleteDialog();
                else {
                    deleteCard();
                    returnToMain();
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View view) {
        Log.d(TAG, "onClick  ID: " + view.getId());
        Log.d(TAG, "onClick  TAG: " + view.getTag());

        switch (view.getId()) {
            case R.id.circle_red_a1:
                storeFabColor(MaterialColor.RED_A100);
                break;
            case R.id.circle_red_a2:
                storeFabColor(MaterialColor.RED_A200);
                break;
            case R.id.circle_red_a3:
                storeFabColor(MaterialColor.RED_A400);
                break;
            case R.id.circle_red_a4:
                storeFabColor(MaterialColor.RED_A700);
                break;

            case R.id.circle_pink_a1:
                storeFabColor(MaterialColor.PINK_A100);
                break;
            case R.id.circle_pink_a2:
                storeFabColor(MaterialColor.PINK_A200);
                break;
            case R.id.circle_pink_a3:
                storeFabColor(MaterialColor.PINK_A400);
                break;
            case R.id.circle_pink_a4:
                storeFabColor(MaterialColor.PINK_A700);
                break;

            case R.id.circle_deep_orange_a1:
                storeFabColor(MaterialColor.DEEP_ORANGE_A100);
                break;
            case R.id.circle_deep_orange_a2:
                storeFabColor(MaterialColor.DEEP_ORANGE_A200);
                break;
            case R.id.circle_deep_orange_a3:
                storeFabColor(MaterialColor.DEEP_ORANGE_A400);
                break;
            case R.id.circle_deep_orange_a4:
                storeFabColor(MaterialColor.DEEP_ORANGE_A700);
                break;

            case R.id.circle_orange_a1:
                storeFabColor(MaterialColor.ORANGE_A100);
                break;
            case R.id.circle_orange_a2:
                storeFabColor(MaterialColor.ORANGE_A200);
                break;
            case R.id.circle_orange_a3:
                storeFabColor(MaterialColor.ORANGE_A400);
                break;
            case R.id.circle_orange_a4:
                storeFabColor(MaterialColor.ORANGE_A700);
                break;

            case R.id.circle_yellow_a1:
                storeFabColor(MaterialColor.YELLOW_A100);
                break;
            case R.id.circle_yellow_a2:
                storeFabColor(MaterialColor.YELLOW_A200);
                break;
            case R.id.circle_yellow_a3:
                storeFabColor(MaterialColor.YELLOW_A400);
                break;
            case R.id.circle_yellow_a4:
                storeFabColor(MaterialColor.YELLOW_A700);
                break;

            case R.id.circle_amber_a1:
                storeFabColor(MaterialColor.AMBER_A100);
                break;
            case R.id.circle_amber_a2:
                storeFabColor(MaterialColor.AMBER_A200);
                break;
            case R.id.circle_amber_a3:
                storeFabColor(MaterialColor.AMBER_A400);
                break;
            case R.id.circle_amber_a4:
                storeFabColor(MaterialColor.AMBER_A700);
                break;

            case R.id.circle_deep_purple_a1:
                storeFabColor(MaterialColor.DEEP_PURPLE_A100);
                break;
            case R.id.circle_deep_purple_a2:
                storeFabColor(MaterialColor.DEEP_PURPLE_A200);
                break;
            case R.id.circle_deep_purple_a3:
                storeFabColor(MaterialColor.DEEP_PURPLE_A400);
                break;
            case R.id.circle_deep_purple_a4:
                storeFabColor(MaterialColor.DEEP_PURPLE_A700);
                break;

            case R.id.circle_indigo_a1:
                storeFabColor(MaterialColor.INDIGO_A100);
                break;
            case R.id.circle_indigo_a2:
                storeFabColor(MaterialColor.INDIGO_A200);
                break;
            case R.id.circle_indigo_a3:
                storeFabColor(MaterialColor.INDIGO_A400);
                break;
            case R.id.circle_indigo_a4:
                storeFabColor(MaterialColor.INDIGO_A700);
                break;

            case R.id.circle_blue_a1:
                storeFabColor(MaterialColor.BLUE_A100);
                break;
            case R.id.circle_blue_a2:
                storeFabColor(MaterialColor.BLUE_A200);
                break;
            case R.id.circle_blue_a3:
                storeFabColor(MaterialColor.BLUE_A400);
                break;
            case R.id.circle_blue_a4:
                storeFabColor(MaterialColor.BLUE_A700);
                break;

            case R.id.circle_light_blue_a1:
                storeFabColor(MaterialColor.LIGHT_BLUE_A100);
                break;
            case R.id.circle_light_blue_a2:
                storeFabColor(MaterialColor.LIGHT_BLUE_A200);
                break;
            case R.id.circle_light_blue_a3:
                storeFabColor(MaterialColor.LIGHT_BLUE_A400);
                break;
            case R.id.circle_light_blue_a4:
                storeFabColor(MaterialColor.LIGHT_BLUE_A700);
                break;

            case R.id.circle_cyan_a1:
                storeFabColor(MaterialColor.CYAN_A100);
                break;
            case R.id.circle_cyan_a2:
                storeFabColor(MaterialColor.CYAN_A200);
                break;
            case R.id.circle_cyan_a3:
                storeFabColor(MaterialColor.CYAN_A400);
                break;
            case R.id.circle_cyan_a4:
                storeFabColor(MaterialColor.CYAN_A700);
                break;

            case R.id.circle_purple_a1:
                storeFabColor(MaterialColor.PURPLE_A100);
                break;
            case R.id.circle_purple_a2:
                storeFabColor(MaterialColor.PURPLE_A200);
                break;
            case R.id.circle_purple_a3:
                storeFabColor(MaterialColor.PURPLE_A400);
                break;
            case R.id.circle_purple_a4:
                storeFabColor(MaterialColor.PURPLE_A700);
                break;

            case R.id.circle_green_a1:
                storeFabColor(MaterialColor.GREEN_A100);
                break;
            case R.id.circle_green_a2:
                storeFabColor(MaterialColor.GREEN_A200);
                break;
            case R.id.circle_green_a3:
                storeFabColor(MaterialColor.GREEN_A400);
                break;
            case R.id.circle_green_a4:
                storeFabColor(MaterialColor.GREEN_A700);
                break;

            case R.id.circle_light_green_a1:
                storeFabColor(MaterialColor.LIGHT_GREEN_A100);
                break;
            case R.id.circle_light_green_a2:
                storeFabColor(MaterialColor.LIGHT_GREEN_A200);
                break;
            case R.id.circle_light_green_a3:
                storeFabColor(MaterialColor.LIGHT_GREEN_A400);
                break;
            case R.id.circle_light_green_a4:
                storeFabColor(MaterialColor.LIGHT_GREEN_A700);
                break;

            case R.id.circle_lime_a1:
                storeFabColor(MaterialColor.LIME_A100);
                break;
            case R.id.circle_lime_a2:
                storeFabColor(MaterialColor.LIME_A200);
                break;
            case R.id.circle_lime_a3:
                storeFabColor(MaterialColor.LIME_A400);
                break;
            case R.id.circle_lime_a4:
                storeFabColor(MaterialColor.LIME_A700);
                break;

            case R.id.circle_teal_a1:
                storeFabColor(MaterialColor.TEAL_A100);
                break;
            case R.id.circle_teal_a2:
                storeFabColor(MaterialColor.TEAL_A200);
                break;
            case R.id.circle_teal_a3:
                storeFabColor(MaterialColor.TEAL_A400);
                break;
            case R.id.circle_teal_a4:
                storeFabColor(MaterialColor.TEAL_A700);
                break;

            case R.id.circle_white:
                storeFabColor(MaterialColor.WHITE);
                break;

            case R.id.circle_light_grey:
                storeFabColor(MaterialColor.GREY_300);
                break;

            case R.id.circle_grey:
                storeFabColor(MaterialColor.GREY_700);
                break;

            case R.id.circle_blue_grey:
                storeFabColor(MaterialColor.BLUE_GREY_500);
                break;

            case R.id.circle_brown:
                storeFabColor(MaterialColor.BROWN_500);
                break;

            case R.id.circle_black:
                storeFabColor(MaterialColor.BLACK);
                break;
        }

        applyCardTheme();

        ensureCloseLogoChooser();
        ensureCloseColorChooser();
    }


    @Override
    public void onIconChanged(String iconName) {
        Log.d(TAG, "onIconChanged: " + iconName + String.format("-> %s. (%s.java:%d)", getClass().getName(), getClass().getSimpleName(), 1192));
        ensureCloseLogoChooser();
        ensureCloseColorChooser();

        CardIcon icon = mCard.getCardIcon();

        String symbol = "";
        if (iconName.contains(Const.ICON_CHANGED_TO_CUSTOM)) {
            symbol = iconName.replace(Const.ICON_CHANGED_TO_CUSTOM, "");
        } else if (iconName.contains(Const.ICON_CHANGED_TO_ICONIC)) {
            symbol = iconName.replace(Const.ICON_CHANGED_TO_ICONIC, "");
        }

        icon.setSymbol(symbol);

        mCard.setCardIcon(new CardIcon(icon.toString()));
        mDatabase.updateCard(mCard);

        updateFab();
    }


    private void loadAds() {
        Log.d(TAG, "loadAds ");
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }


    private void ensureDisposeAds() {
        if (mAdView == null) return;
        try {
            mAdView.setVisibility(View.INVISIBLE);
            mAdView.destroy();
            mAdView = null;
        } catch (Exception e) {
            Log.e(TAG, "ensureDisposeAds: " + e);
        }
    }


    private void makeClipBoardOrUrlClickListener(final ListView listView) {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                TextView tvDesc = (TextView) view.findViewById(R.id.datapair_description);
                TextView tvToCopyFrom = (TextView) view.findViewById(R.id.datapair_value);

                if (Logic.isUrlField(mCtx, tvDesc.getText().toString())) {
                    Uri uri = Uri.parse(tvToCopyFrom.getText().toString());
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    try {
                        startActivity(intent);
                    } catch (Exception e) {
                        Log.e(TAG, "Exception while starting Activity from Url: " + e);
                        ToastUtils.toastShort(R.string.url_not_valid);
                    }
                } else {
                    final ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    final ClipData clip = ClipData.newPlainText(getString(R.string.clipboard_label_password), tvToCopyFrom.getText());

                    clipboard.setPrimaryClip(clip);

                    Logic.ensureClearClipboard(ShowCardActivity.this, clipboard);

                    ToastUtils.toastShort(R.string.data_copied_to_clipboard);
                }
            }
        });
    }


    private void broadcastIconChanged() {
        Intent intent = new Intent(Const.LBC_ICON_CHANGED);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    private void toggleFavoriteIcon(MenuItem item, boolean status) {
        if (status) item.setIcon(R.drawable.star);
        else item.setIcon(R.drawable.unstar_white);
    }


    private void loadContent() {
        List<DataPair> pairs = mDatabase.getDatapairsByCard(mCard.getId());
        mAdapter = new ShowCardListAdapter(mCtx, pairs);
        mListView.setAdapter(mAdapter);
        makeClipBoardOrUrlClickListener(mListView);
    }


    private void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar_show_card);
        mToolbarExtension = (RelativeLayout) findViewById(R.id.toolbar_show_card_extension);

        Logic.themeToolbar(this, mToolbar, mToolbarExtension);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mTvToolbarTitle = (TextView) mToolbarExtension.findViewById(R.id.tv_extension_title);
        if (!Logic.isDarkTheme(this)) mTvToolbarTitle.setTextColor(getResources().getColor(R.color.Grey_2));

        setAutoSizedTitle(mTvToolbarTitle, mCard.getTitle());
    }


    private void setAutoSizedTitle(TextView tvTitle, String title) {
        if (title.length() > Const.MAXWORDLNGT_FOR_LRGE_TITL_IN_SHOWCARD) {
            tvTitle.setTextSize(20);
        } else tvTitle.setTextSize(34);
        tvTitle.setText(mCard.getTitle());
    }


    private void displayWhenCardWasEdited() {
        String d = mCard.getCreated();

        String month = d.substring(4, 6);
        if (month.startsWith("1")) {
            int _month = Integer.parseInt(month);
            _month++;
            month = String.valueOf(_month);
        } else {
            month = month.substring(1);
            int _month = Integer.parseInt(month);
            _month++;
            month = String.format("%02d", _month);
        }

        d = d.substring(6, 8) + "." + month + "." + d.substring(0, 4) + " - " +
            d.substring(8, 10) + ":" + d.substring(10, 12);

        mTvLastEdit = (TextView) findViewById(R.id.tv_last_edit);
        mTvLastEdit.setText(String.format("%s %s %s", getString(R.string.last_edited), d, getString(R.string.watch)));
    }


    private void showIconChooserDialog() {
        FragmentManager fm = getSupportFragmentManager();
        Log.d(TAG, "showIconChooserDialog  " + mCard.toString());
        mFragmentSymbolChooser = new FragmentSymbolChooser();
        mFragmentSymbolChooser.show(fm, "FragmentSymbolChooser");
    }


    private void showColorChooserDialog() {
        FragmentManager fm = getSupportFragmentManager();
        mFragmentColorChooser = new FragmentColorChooser();
        mFragmentColorChooser.show(fm, "FragmentColorChooser");
    }


    private void ensureCloseColorChooser() {
        if (mFragmentColorChooser != null && mFragmentColorChooser.isVisible()) {
            mFragmentColorChooser.dismiss();
        }
    }


    private void ensureCloseLogoChooser() {
        if (mFragmentSymbolChooser != null && mFragmentSymbolChooser.isVisible()) {
            mFragmentSymbolChooser.dismiss();
        }
    }


    private void storeFabColor(String color) {
        if (color.equals(Const.ICON_UNSET)) return;
        String colorToParse = ColorUtils.hexToHtml(color);
        mCard.getCardIcon().setColor(colorToParse);
        mCard.setIcon(mCard.getRawIcon());
        mDatabase.updateCard(mCard);

        checkInitialColor(colorToParse);

        updateFab();
    }


    private void updateFab() {
        String symbol = mCard.getCardIcon().getSymbol();
        String bgColor = mCard.getCardIcon().getColor();
        bgColor = ColorUtils.hexToHtml(bgColor);
        String symbolColor = CardIcon.convertSymbolColor(mCard.getCardIcon().getSymbolColor());

        // set default
        Drawable drawable = Logic.loadIconics(mCtx, "faw_question", 32, symbolColor);

        if (mCard.getCardIcon().ensureSymbol()) {
            // symbol
            if (symbol.contains(Const.SYMB_TAGGD)) {
                // set text...
                symbol = CardIcon.replaceTagged(mCard.getCardIcon().getSymbol());

                Drawable[] layers = new Drawable[2];
                layers[0] = Loader.getDrawable(R.drawable.drawable_circle);
                layers[1] = new DrawableCircleLetter.Builder(DIAMETER_DP, "#00FFFFFF", symbol).textColor(symbolColor).build();
                drawable = new LayerDrawable(layers);
            } else {
                // set iconics
                drawable = Logic.loadIconics(this, symbol, SYMBOL_SIZE, symbolColor);
            }
        }
        mFabMenu.getMenuIconView().setImageDrawable(drawable);


        if (!bgColor.contains(Const.ICON_UNSET)) {
            // color!
            bgColor = bgColor.substring(0, 7);
            mFabMenu.setMenuButtonColorNormal(Color.parseColor(bgColor));
        } else {
            // load theme-dependent default color
            bgColor = Logic.isDarkTheme(mCtx) ? MaterialColor.BLUE_GREY_500 : MaterialColor.DEEP_PURPLE_A400;
            mFabMenu.setMenuButtonColorNormal(Color.parseColor(bgColor));
        }
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void applyCardTheme() {
        mToolbar.setBackgroundColor(mToolbarColor);
        mToolbarExtension.setBackgroundColor(mToolbarColor);

        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        int currentApiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentApiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(mStatusbarColor);
        }
    }


    private void sendCard() {
        String message = collectCardData();

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, message);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }


    private String collectCardData() {
        String data = "";
        data += getString(R.string.title) + ": " + mCard.getTitle();

        List<DataPair> datapairs = mDatabase.getDatapairsByCard(mCard.getId());
        for (DataPair dataPair : datapairs) {
            data += "\n";
            data += dataPair.getDescription() + ": " + dataPair.getValue();
        }

        return data;
    }


    /**
     * Fügt der Benachrichtigungszeile Anmeldedaten hinzu und
     * gibt diese mit Intent an den BroadcastReceiver weiter.
     * Der BroadcastReceiver nimmt dann die Daten entgegen und wenn
     * auf die Benachrichtigung gedrückt wird, werden die
     * Anmeldedaten in die Zwischenablage kopiert.
     */
    private void addCredentialsToNotificationBar(boolean isPassword) {
        String which, notificationTitle;
        int notificationId;
        Intent intent;

        if (isPassword) {
            intent = new Intent(this, ClipboardReceiverPassword.class);
            which = getString(R.string.password);
            notificationTitle = getString(R.string.notification_title_pwd);
            notificationId = Const.NOTIFICATION_PWD;
        } else {
            intent = new Intent(this, ClipboardReceiverLogin.class);
            notificationTitle = getString(R.string.notification_title_login);
            notificationId = Const.NOTIFICATION_LOGIN;
            which = getString(R.string.login);
        }

        String string = getValueForClipboard(which);

        /*Wenn Passwort leer wird Pin kopiert.*/
        if (which.equals(getString(R.string.password)) && TextUtils.isEmpty(string)) {
            notificationTitle = getString(R.string.notification_title_pin);
            string = getValueForClipboard(getString(R.string.pin));
        }

        /*Wenn Login leer wird Email-Adresse kopiert.*/
        if (which.equals(getString(R.string.login)) && TextUtils.isEmpty(string)) {
            notificationTitle = getString(R.string.notification_title_mail);
            string = getValueForClipboard(getString(R.string.u_mail));
        }

        intent.putExtra(Const.BNL_COPY_CLIPBOARD, string);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_border_color_white_18dp)
                .setContentTitle(notificationTitle + " " + mCard.getTitle())
                .addAction(R.drawable.ic_content_copy_white_24dp, getString(R.string.insert_to_clipboard), pendingIntent);

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(notificationId, builder.build());
    }


    /**
     * Sucht nach "Login", "Passwort" oder "Mail" Feldern und gibt den
     * jeweilig ERSTEN gefundenen Eintrag zurück.
     */
    private String getValueForClipboard(String which) {
        List<DataPair> dataPairs = mDatabase.getDatapairsByCard(mCard.getId());

        for (DataPair dataPair : dataPairs) {
            if (which.toLowerCase().contains(getString(R.string.password).toLowerCase())
                && dataPair.getDescription().toLowerCase().contains(getString(R.string.password).toLowerCase())) {
                return dataPair.getValue();
            }

            if (which.equals(getString(R.string.pin))
                && dataPair.getDescription().toLowerCase().contains(getString(R.string.pin).toLowerCase())) {
                return dataPair.getValue();
            }

            if (which.equals(getString(R.string.login))
                && dataPair.getDescription().toLowerCase().contains(getString(R.string.login).toLowerCase())) {
                return dataPair.getValue();
            }

            if (which.equals(getString(R.string.u_mail))
                && dataPair.getDescription().toLowerCase().contains(getString(R.string.u_mail).toLowerCase())) {
                return dataPair.getValue();
            }
        }
        return "";
    }


    private Intent makeEditCardIntent() {
        Intent intent = new Intent(this, CardEditorActivity.class);
        intent.putExtra(Const.BNL_PW, mPwd);
        intent.putExtra(Const.BNL_SHOW_ANIMATION, true);
        intent.putExtra(Const.BNL_CARD_ID, mCard.getId());
        intent.putExtra(Const.BNL_CARD_EDIT_MODE, true);
        intent.putExtra(Const.TB_P1_COLOR, mToolbarColor);
        intent.putExtra(Const.TB_P2_COLOR, mStatusbarColor);
        return intent;
    }


    /**
     * Label für Karte bestimmen.
     */
    public void showLabelSelectionDialog() {

        final List<Integer> selection = new ArrayList<Integer>();
        final List<Label> labels = mDatabase.getAllLabels();

        String lableNameArray[] = new String[labels.size()];
        List<Integer> _preSelected = new ArrayList<Integer>();

        for (int i = 0; i < labels.size(); i++) lableNameArray[i] = labels.get(i).getName();

        List<Label> linkedLabelIds = mDatabase.getAllLabelByCard(mCard.getId());

        int i = 0;
        for (Label label : labels) {
            for (Label linkedId : linkedLabelIds) {
                if (label.getName().equals(linkedId.getName())) _preSelected.add(i);
            }
            i++;
        }

        Integer[] preSelected = new Integer[_preSelected.size()];
        for (int j = 0; j < _preSelected.size(); j++) preSelected[j] = _preSelected.get(j);

        new MaterialDialog.Builder(this)
                .title(R.string.choose_label_title)
                .positiveText(R.string.ok)
                .neutralText(getString(R.string.New))
                .negativeText(R.string.cancel)
                .items(lableNameArray)
                .itemsCallbackMultiChoice(preSelected, new MaterialDialog.ListCallbackMultiChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, Integer[] which, CharSequence[] text) {
                        Collections.addAll(selection, which);
                        setLabel(labels, selection);
                        return true;
                    }
                })
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        // onSelection wird überschrieben -> funktioniert!
                        super.onPositive(dialog);
                    }


                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }


                    @Override
                    public void onNeutral(MaterialDialog dialog) {
                        showAddNewLabelDialog();
                    }
                })
                .show();
    }


    private void showAddNewLabelDialog() {
        new DialogAddNewLabel(this);
    }


    private void setLabel(List<Label> labels, List<Integer> selection) {
        // alte Einträge löschen
        mDatabase.removeAllLabelLinksToCard(mCard.getId());
        // neue Einträge setzen
        for (Integer aSelection : selection) {
            mDatabase.createLabelLink(mCard.getId(), labels.get(aSelection).getId());
        }
    }


    private void showDeleteDialog() {
        new DialogDeleteCard(this);
    }


    public void returnToMain() {
        Intent backToMain = new Intent(new Intent(this, MainActivity.class));
        backToMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        backToMain.putExtra(Const.BNL_PW, mPwd);
        startActivity(backToMain);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setAllowEnterTransitionOverlap(true);
            finishAfterTransition();
        } else {
            finish();
        }
    }


    public void deleteCard() {
        mDatabase.deleteCardInclDataPais(mCard.getId());
        ToastUtils.toastShort(R.string.toast_card_deleted_success);
    }


    private void changeFavoriteStatus() {
        mDatabase.updateCard(mCard);
    }


    private void checkInitialColor(String color) {
        Log.d(TAG, "checkInitialColor  " + color);
        color = color.toLowerCase();
        if (color.contains("0x")) {
            color = ColorUtils.webAlphaToHtml(color);
        }

        if (BooleanUtils.stringEquals(color, MaterialColor.RED_A100, MaterialColor.RED_A200, MaterialColor.RED_A400, MaterialColor.RED_A700)) {
            collectThemeColors(MaterialColor.TEAL_500, MaterialColor.TEAL_700);
        } else if (BooleanUtils.stringEquals(color, MaterialColor.PINK_A100, MaterialColor.PINK_A200, MaterialColor.PINK_A400, MaterialColor.PINK_A700)) {
            collectThemeColors(MaterialColor.CYAN_500, MaterialColor.CYAN_700);
        } else if (BooleanUtils.stringEquals(color, MaterialColor.PURPLE_A100, MaterialColor.PURPLE_A200, MaterialColor.PURPLE_A400, MaterialColor.PURPLE_A700)) {
            collectThemeColors(MaterialColor.AMBER_500, MaterialColor.AMBER_700);
        } else if (BooleanUtils.stringEquals(color, MaterialColor.DEEP_PURPLE_A100, MaterialColor.DEEP_PURPLE_A200, MaterialColor.DEEP_PURPLE_A400, MaterialColor.DEEP_PURPLE_A700)) {
            collectThemeColors(MaterialColor.ORANGE_500, MaterialColor.ORANGE_700);
        } else if (BooleanUtils.stringEquals(color, MaterialColor.INDIGO_A100, MaterialColor.INDIGO_A200, MaterialColor.INDIGO_A400, MaterialColor.INDIGO_A700)) {
            collectThemeColors(MaterialColor.RED_500, MaterialColor.RED_700);
        } else if (BooleanUtils.stringEquals(color, MaterialColor.BLUE_A100, MaterialColor.BLUE_A200, MaterialColor.BLUE_A400, MaterialColor.BLUE_A700)) {
            collectThemeColors(MaterialColor.LIGHT_GREEN_500, MaterialColor.LIGHT_GREEN_700);
        } else if (BooleanUtils.stringEquals(color, MaterialColor.LIGHT_BLUE_A100, MaterialColor.LIGHT_BLUE_A200, MaterialColor.LIGHT_BLUE_A400, MaterialColor.LIGHT_BLUE_A700)) {
            collectThemeColors(MaterialColor.PINK_500, MaterialColor.PINK_700);
        } else if (BooleanUtils.stringEquals(color, MaterialColor.CYAN_A100, MaterialColor.CYAN_A200, MaterialColor.CYAN_A400, MaterialColor.CYAN_A700)) {
            collectThemeColors(MaterialColor.YELLOW_500, MaterialColor.YELLOW_700);
        } else if (BooleanUtils.stringEquals(color, MaterialColor.TEAL_A100, MaterialColor.TEAL_A200, MaterialColor.TEAL_A400, MaterialColor.TEAL_A700)) {
            collectThemeColors(MaterialColor.DEEP_ORANGE_500, MaterialColor.DEEP_ORANGE_700);
        } else if (BooleanUtils.stringEquals(color, MaterialColor.GREEN_A100, MaterialColor.GREEN_A200, MaterialColor.GREEN_A400, MaterialColor.GREEN_A700)) {
            collectThemeColors(MaterialColor.LIGHT_BLUE_500, MaterialColor.LIGHT_BLUE_700);
        } else if (BooleanUtils.stringEquals(color, MaterialColor.LIGHT_GREEN_A100, MaterialColor.LIGHT_GREEN_A200, MaterialColor.LIGHT_GREEN_A400, MaterialColor.LIGHT_GREEN_A700)) {
            collectThemeColors(MaterialColor.DEEP_PURPLE_500, MaterialColor.DEEP_PURPLE_700);
        } else if (BooleanUtils.stringEquals(color, MaterialColor.LIME_A100, MaterialColor.LIME_A200, MaterialColor.LIME_A400, MaterialColor.LIME_A700)) {
            collectThemeColors(MaterialColor.PURPLE_500, MaterialColor.PURPLE_700);
        } else if (BooleanUtils.stringEquals(color, MaterialColor.YELLOW_A100, MaterialColor.YELLOW_A200, MaterialColor.YELLOW_A400, MaterialColor.YELLOW_A700)) {
            collectThemeColors(MaterialColor.INDIGO_500, MaterialColor.INDIGO_700);
        } else if (BooleanUtils.stringEquals(color, MaterialColor.ORANGE_A100, MaterialColor.ORANGE_A200, MaterialColor.ORANGE_A400, MaterialColor.ORANGE_A700)) {
            collectThemeColors(MaterialColor.ORANGE_500, MaterialColor.ORANGE_700);
        } else if (BooleanUtils.stringEquals(color, MaterialColor.AMBER_A100, MaterialColor.AMBER_A200, MaterialColor.AMBER_A400, MaterialColor.AMBER_A700)) {
            collectThemeColors(MaterialColor.LIME_500, MaterialColor.LIME_700);
        } else if (BooleanUtils.stringEquals(color, MaterialColor.DEEP_ORANGE_A100, MaterialColor.DEEP_ORANGE_A200, MaterialColor.DEEP_ORANGE_A400, MaterialColor.DEEP_ORANGE_A700)) {
            collectThemeColors(MaterialColor.GREEN_500, MaterialColor.GREEN_700);
        } else {
            if (Logic.isDarkTheme(this)) {
                collectThemeColors(MaterialColor.ORANGE_500, MaterialColor.ORANGE_500);
            } else collectThemeColors(MaterialColor.RED_500, MaterialColor.RED_700);
        }
        applyCardTheme();
    }


    private void collectThemeColors(String tb, String sb) {
        mToolbarColor = Color.parseColor(tb);
        mStatusbarColor = Color.parseColor(sb);
        if (tb.equals(MaterialColor.YELLOW_500)) {
            mTvToolbarTitle.setTextColor(Color.parseColor(MaterialColor.GREY_800));
        } else mTvToolbarTitle.setTextColor(Color.parseColor(MaterialColor.WHITE));
    }


    public Database getDatabase() {
        return mDatabase;
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}

