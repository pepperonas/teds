package com.pepperonas.teds.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.clans.fab.FloatingActionButton;
import com.pepperonas.andbasx.base.ToastUtils;
import com.pepperonas.teds.R;
import com.pepperonas.teds.adapter.LabelListAdapter;
import com.pepperonas.teds.model.Database;
import com.pepperonas.teds.model.Label;
import com.pepperonas.teds.settings.Pref;
import com.pepperonas.teds.utils.Const;
import com.pepperonas.teds.utils.Logic;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LabelEditorActivity
        extends AppCompatActivity
        implements View.OnClickListener {

    private static final String TAG = "LabelEditor";

    private static final boolean
            ADD_NEW_LABLE = true,
            RENAME_LABLE = false;

    private ListView mListView;

    private List<Label> mLabelList = new ArrayList<Label>();

    private LabelListAdapter mLabelListAdapter;

    private Database mDatabase;

    private String mPwd;

    private BroadcastReceiver mMsgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctx, Intent intent) {
            finish();
        }
    };

    private BroadcastReceiver mScreenOffReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctx, Intent intent) {
            Logic.ensureShowLoginScreen((Activity) ctx);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Logic.applyTheme(this);
        Logic.ensureHideFromRecents(this);

        super.onCreate(savedInstanceState);

        mPwd = getIntent().getStringExtra(Const.BNL_PW);

        Logic.ensureShowHint(
                this, Const.SHOW_LABEL_EDITOR_HINT,
                getString(R.string.hint_label_editor), 3);

        setContentView(R.layout.activity_label_editor);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_add_label);

        mDatabase = new Database(this, mPwd);

        mLabelList = mDatabase.getAllLabels();

        toolbarSetup();

        mListView = (ListView) findViewById(R.id.listViewLabelEditor);

        mLabelListAdapter = new LabelListAdapter(this, mLabelList);
        mListView.setAdapter(mLabelListAdapter);

        // ClickListener für Namensänderung des Labels
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                showSetupDialog(RENAME_LABLE, position);
            }
        });

        // LongClickListener zum Löschen
        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                return true;
            }
        });

        mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        mListView.setMultiChoiceModeListener(new ModeCallback());
    }


    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(mMsgReceiver, new IntentFilter(Const.LBC_INTENT_TO_FINISH));

        registerReceiver(mScreenOffReceiver, new IntentFilter(Intent.ACTION_SCREEN_OFF));

        Logic.ensureShowLoginScreen(this);
    }


    @Override
    protected void onPause() {
        Logic.ensureShowLoginIfLocked(this);
        Logic.ensureEnableLockTimer(this, this);
        mDatabase.close();

        if (mScreenOffReceiver != null) {
            unregisterReceiver(mScreenOffReceiver);
        }

        super.onPause();
    }


    @Override
    protected void onDestroy() {
        mPwd = null;
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMsgReceiver);

        super.onDestroy();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_label_list_editor, menu);
        MenuItem delete = menu.getItem(0);
        delete.setEnabled(false);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.fab_add_label) {
            showSetupDialog(ADD_NEW_LABLE, 0);
        }
    }


    private void showSetupDialog(final boolean isAddNew, final int position) {
        final String oldValue, title;

        if (!isAddNew) {
            oldValue = ((Label) mLabelListAdapter.getItem(position)).getName();
            title = getString(R.string.rename);
        } else {
            title = getString(R.string.dialog_title_new_label);
            oldValue = "";
        }

        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .title(title)
                .customView(R.layout.dialog_data_editor, true)
                .positiveText(R.string.ok)
                .negativeText(R.string.cancel)
                .autoDismiss(true)
                .showListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(final DialogInterface dialog) {
                        final MaterialDialog matDia = (MaterialDialog) dialog;
                        final EditText et = (EditText) matDia.findViewById(R.id.et_rename_data);
                        et.setText(oldValue);
                        et.setSelection(oldValue.length());

                        if (et.getText().toString().length() < 2) {
                            matDia.getActionButton(DialogAction.POSITIVE).setEnabled(false);
                            matDia.getActionButton(DialogAction.POSITIVE).setClickable(false);
                        }

                        Logic.applyLengthCheck(et, matDia);
                        et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                            @Override
                            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                matDia.getActionButton(DialogAction.POSITIVE).callOnClick();
                                return true;
                            }
                        });
                    }
                })
                .callback(new MaterialDialog.ButtonCallback() {

                    // OK...
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        EditText et = (EditText) dialog.findViewById(R.id.et_rename_data);
                        String input = et.getText().toString();

                        if (inputFail(et, input)) return;

                        if (isAddNew) makeNewLabelAndRefresh(input);

                        else renameLabelAndRefresh(input, position);
                    }


                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }
                });

        MaterialDialog dialog = builder.build();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.show();
    }


    private void renameLabelAndRefresh(String input, int position) {
        Label labToUpdate = mDatabase.getLabelByName(
                ((Label) mLabelListAdapter.getItem(position)).getName());

        labToUpdate.setName(input);
        mDatabase.updateLabel(labToUpdate);
        mLabelList.set(position, labToUpdate);
        mLabelListAdapter.notifyDataSetChanged();
    }


    private void makeNewLabelAndRefresh(String input) {
        Label userLabel = new Label(0, input, String.valueOf(System.currentTimeMillis()));
        mDatabase.createLabel(userLabel);
        mLabelList.add(userLabel);
        mLabelListAdapter.notifyDataSetChanged();
    }


    private boolean inputFail(EditText et, String input) {
        return isInputVoid(et)
               || isInputEqualToStaticLabels(input)
               || isInputAllreadySetAsCustomLabel(input);
    }


    private boolean isInputVoid(EditText et) {
        if (et.getText().toString().isEmpty()) {
            ToastUtils.toastLong(R.string.toast_label_editor_empty_inputs_are_not_allowed);
            return true;
        }

        return false;
    }


    private boolean isInputEqualToStaticLabels(String input) {
        String[] protectedLabels = getResources()
                .getStringArray(R.array.logical_label);

        if (input.equals(protectedLabels[Const.ALL_CARDS])
            || input.equals(protectedLabels[Const.FAVORITES])
            || input.equals(protectedLabels[Const.UNLABELED])
            || input.equals(protectedLabels[Const.WEAK_PASS])) {

            // falls Nutzer Label-Namen aus Standard-Vorgaben
            // doppelt vergeben möchte...
            ToastUtils.toastShort(R.string.label_name_allready_set);
            return true;
        }
        return false;
    }


    private boolean isInputAllreadySetAsCustomLabel(String input) {
        List<String> activeUserLabels = new ArrayList<String>();

        for (Label labl : mLabelList) activeUserLabels.add(labl.getName());
        if (activeUserLabels.contains(input)) {
            ToastUtils.toastShort(R.string.label_name_allready_set);
            return true;
        }

        return false;
    }


    private Toolbar toolbarSetup() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_label_editor);
        Logic.themeToolbar(this, mToolbar, null);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return mToolbar;
    }


    private class ModeCallback implements AbsListView.MultiChoiceModeListener {

        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_label_list_editor, menu);

            MenuItem delete = menu.getItem(0);
            delete.setVisible(true);

            return true;
        }


        public boolean onPrepareActionMode(ActionMode mode, Menu menu) { return true; }


        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {

                case R.id.action_delete_label:
                    SharedPreferences prefs = getSharedPreferences(Pref.CONFIG_FILE, Context.MODE_PRIVATE);

                    if (prefs.getBoolean(Pref.PK_CB_APPLY_DELETE, true)) {
                        showDeleteDialog(mode);
                    } else {
                        deleteSelectedLabelsAndRefresh(mode);
                        mode.finish();
                    }

                    break;

                default:
                    break;
            }

            return true;
        }


        public void onDestroyActionMode(ActionMode mode) { }


        public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
            final int checkedCount = mListView.getCheckedItemCount();

            switch (checkedCount) {
                case 0:
                    mode.setTitle(getString(R.string.title_edit_labels));
                    break;
                case 1:
                    mode.setTitle(getString(R.string.one_label_selected));
                    break;
                default:
                    mode.setTitle(checkedCount + " " + getString(R.string.labels_selected));
                    break;
            }
        }
    }


    private void showDeleteDialog(final ActionMode mode) {
        new MaterialDialog.Builder(this)
                .title(R.string.hint)
                .content(R.string.dialog_msg_delete_selected_labels)
                .positiveText(R.string.ok)
                .negativeText(R.string.cancel)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        deleteSelectedLabelsAndRefresh(mode);
                    }


                    @Override
                    public void onNegative(MaterialDialog dialog) { }
                })
                .show();
    }


    private void deleteSelectedLabelsAndRefresh(ActionMode mode) {
        long[] checkedIds = mListView.getCheckedItemIds();

        for (long checkedId : checkedIds) {

            Label label = mDatabase.getLabel(checkedId);

            mDatabase.deleteLabel(checkedId);
            mDatabase.deleteLinksOfLabel(checkedId);

            // in Ui löschen
            mLabelList.remove(label);

        }

        if (mode != null) mode.finish();

        mLabelListAdapter.notifyDataSetChanged(mDatabase);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}