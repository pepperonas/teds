package com.pepperonas.teds.activities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.pepperonas.andbasx.base.ToastUtils;
import com.pepperonas.teds.R;
import com.pepperonas.teds.model.AccountInfo;
import com.pepperonas.teds.model.Card;
import com.pepperonas.teds.model.CardIcon;
import com.pepperonas.teds.model.DataPair;
import com.pepperonas.teds.model.Database;
import com.pepperonas.teds.dialogs.DialogAddCustomDatapair;
import com.pepperonas.teds.dialogs.DialogDefaultChooser;
import com.pepperonas.teds.settings.Pref;
import com.pepperonas.teds.utils.Const;
import com.pepperonas.teds.utils.Logic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by pepperonas on 05.02.15.
 * CardEditorActivity - Activity ermöglicht das Hinzufügen einer neuen Karte.
 */
public class CardEditorActivity
        extends AppCompatActivity
        implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    private static final String TAG = "CardEditorAct";

    public static final boolean NOT_FOCUSED = false, FOCUSED = true;

    private static final int
            AMOUNT_OF_DICTIONARIES = 96,
            LINES_IN_DICT_INCL_OFFSET = 450;

    /*
     * Dialog-UI
     */
    private MaterialDialog mMaterialDialog;
    private TextView mTvPassword, mTvPasswordLength;
    private SeekBar mSkbPasswordLength;
    private Spinner mSpinner;

    /*
     * Activity-UI
     */
    private LinearLayout mContainer;

    private String mPwd = "";
    private Card mCard;
    private Database mDatabase;

    private Toolbar mToolbar;

    private int mToolbarColor = 0, mStatusbarColor = 0;

    private boolean mIsWritten = false;

    private boolean mIsEditing;

    private EditText mEtTitle;

    private Random mRandomGenerator;

    private String mP;

    private BroadcastReceiver mMsgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    };

    private BroadcastReceiver mScreenOffReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctx, Intent intent) {
            Logic.ensureShowLoginScreen((Activity) ctx);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (getIntent().getBooleanExtra(Const.BNL_SHOW_ANIMATION, false)) {
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }

        Logic.applyTheme(this);
        Logic.ensureHideFromRecents(this);
        ensureHideKeyboard(getWindow());

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_card_editor);

        mContainer = (LinearLayout) findViewById(R.id.container);

        mP = getIntent().getStringExtra(Const.BNL_PW);
        try {
            mToolbarColor = getIntent().getIntExtra(Const.TB_P1_COLOR, 0);
            mStatusbarColor = getIntent().getIntExtra(Const.TB_P2_COLOR, 0);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "onCreate no Toolbar color found.");
        }

        mRandomGenerator = new Random();

        // erkennen, ob Nutzer Karte bearbeiten möchte
        mIsEditing = getIntent().getBooleanExtra(Const.BNL_CARD_EDIT_MODE, false);

        mDatabase = new Database(this, mP);

        toolbarSetup();
        applyCardTheme();

        if (!mIsEditing) addTitleSection("");

        // erkennen, ob Nutzer eine Vorlage gewählt hat (Neuanlage)
        String preset = getIntent().getStringExtra(Const.BNL_CARD_PRESET_MODE);
        if (preset != null) loadPreset(preset);

        if (mIsEditing) loadCard();
    }


    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(mMsgReceiver, new IntentFilter(Const.LBC_INTENT_TO_FINISH));

        registerReceiver(mScreenOffReceiver, new IntentFilter(Intent.ACTION_SCREEN_OFF));

        Logic.ensureShowLoginScreen(this);
    }


    @Override
    protected void onPause() {
        if (isApplyChangesDirectly()) collectCardData();
        Logic.ensureShowLoginIfLocked(this);
        Logic.ensureEnableLockTimer(this, this);
        mDatabase.close();

        if (mScreenOffReceiver != null) {
            unregisterReceiver(mScreenOffReceiver);
        }

        super.onPause();
    }


    @Override
    protected void onDestroy() {
        mP = null;
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMsgReceiver);

        super.onDestroy();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        backToShowCardActivity();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_card_editor, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save_card:
                collectCardData();
                break;

            case android.R.id.home:
                backToShowCardActivity();
                return false;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(final View view) {
        if (view.getId() == R.id.btn_choose_content) {
            showDefaultsChooserDialog();
        }
    }


    @Override
    public void onStartTrackingTouch(final SeekBar seekBar) {
        if (mTvPasswordLength == null) {
            mTvPasswordLength = (TextView) mMaterialDialog.findViewById(R.id.tv_pwd_current_lenght);
        }
        mTvPasswordLength.setTextSize(Const.TEXT_SIZE_SLIDER_PRESSED);
    }


    @Override
    public void onStopTrackingTouch(final SeekBar seekBar) {
        if (mTvPasswordLength == null) {
            mTvPasswordLength = (TextView) mMaterialDialog.findViewById(R.id.tv_pwd_current_lenght);
        }
        mTvPasswordLength.setTextSize(Const.TEXT_SIZE_SLIDER_UNPRESSED);
    }


    private void loadCard() {
        mCard = mDatabase.getCard(getIntent().getIntExtra(Const.BNL_CARD_ID, 0));

        addTitleSection(mCard.getTitle());

        List<DataPair> datapairs = mDatabase.getDatapairsByCard(mCard.getId());

        // Kartentitel in Datenverabeitung
        for (DataPair dataPair : datapairs) {

            // Passwort-Feld anzeigen
            if (dataPair.getDescription().equals(getString(R.string.password))) {
                addPwdContent(dataPair.getDescription(), dataPair.getValue(), NOT_FOCUSED);
            }
            // Login-Feld anzeigen
            else if (dataPair.getDescription().equals(getString(R.string.login))) {
                addLoginContent(dataPair.getDescription(), dataPair.getValue(), NOT_FOCUSED);
            }
            // normales Wertepaar anzeigen
            else addContent(dataPair.getDescription(), dataPair.getValue(), NOT_FOCUSED);

        } // Ende Loop Karten-Beschreibung
    }


    private void loadPreset(String preset) {
        if (preset.equals(Const.BNL_PRESET_ACCOUNT)) {
            addLoginContent(getString(R.string.login), "", NOT_FOCUSED);
            addPwdContent(getString(R.string.password), "", NOT_FOCUSED);
            addContent(getString(R.string.note), "", NOT_FOCUSED);

        } else if (preset.equals(Const.BNL_PRESET_MAIL)) {
            addLoginContent(getString(R.string.mail_address), "", NOT_FOCUSED);
            addPwdContent(getString(R.string.password), "", NOT_FOCUSED);
            addContent(getString(R.string.note), "", NOT_FOCUSED);

        } else if (preset.equals(Const.BNL_PRESET_CODE)) {
            addContent(getString(R.string.code), "", NOT_FOCUSED);
            addContent(getString(R.string.note), "", NOT_FOCUSED);

        } else if (preset.equals(Const.BNL_PRESET_PIN)) {
            addContent(getString(R.string.pin), "", NOT_FOCUSED);
            addContent(getString(R.string.note), "", NOT_FOCUSED);

        } else if (preset.equals(Const.BNL_PRESET_NOTE)) {
            addContent(getString(R.string.message), "", NOT_FOCUSED);
            addContent(getString(R.string.note), "", NOT_FOCUSED);

        } else if (preset.equals(Const.BNL_PRESET_PGP)) {
            addPwdContent(getString(R.string.password), "", NOT_FOCUSED);
            addContent(getString(R.string.pgp_public_key), "", NOT_FOCUSED);
            addContent(getString(R.string.pgp_private_key), "", NOT_FOCUSED);
            addContent(getString(R.string.note), "", NOT_FOCUSED);
        }
    }


    private boolean isApplyChangesDirectly() {
        SharedPreferences prefs = getSharedPreferences(Pref.CONFIG_FILE, MODE_PRIVATE);
        return prefs.getBoolean(Pref.PK_CB_SAVE_INSTANT, false);
    }


    private boolean isTitleSet() { return mEtTitle.getText().toString().isEmpty(); }


    private void showDefaultsChooserDialog() {
        new DialogDefaultChooser(this);
    }


    private void toolbarSetup() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar_card_editor);
        Logic.themeToolbar(this, mToolbar, null);

        TextView toolbarTitle = (TextView) mToolbar.findViewById(R.id.tv_toolbar_title_card_editor);

        if (mIsEditing) toolbarTitle.setText(getString(R.string.card_editor_toolbar_title));
        else toolbarTitle.setText(getString(R.string.title_activity_add_card));

        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void applyCardTheme() {
        if (mToolbarColor == 0) return;

        mToolbar.setBackgroundColor(mToolbarColor);

        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP
            && mStatusbarColor != 0) {
            window.setStatusBarColor(mStatusbarColor);
        }
    }


    public void showAddCustomDatapairDialog() {
        new DialogAddCustomDatapair(this);
    }


    private void backToShowCardActivity() {

        if (!mIsEditing) backToMain();

        else {
            Intent intent = new Intent(CardEditorActivity.this, ShowCardActivity.class);

            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

            intent.putExtra(Const.BNL_PW, mP);
            intent.putExtra(Const.BNL_CARD_ID, mCard.getId());

            intent.putExtra(Const.BNL_SHOW_ANIMATION, true);

            intent.putExtra(Const.TB_P1_COLOR, mToolbarColor);
            intent.putExtra(Const.TB_P2_COLOR, mStatusbarColor);

            startActivity(intent);
            finish();
        }
    }


    private void backToMain() {
        Intent intent = new Intent(CardEditorActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Const.BNL_PW, mP);
        startActivity(intent);
        finish();
    }


    /**
     * Speichert alle Einträge, die der Karte hinzugefügt wurden, ab.
     */
    private void collectCardData() {

        if (mIsWritten) return;

        String desc = "", value = "";
        ArrayList<DataPair> dataList = new ArrayList<DataPair>();

        for (int i = 0; i < mContainer.getChildCount(); i++) {

            LinearLayout frameLayout = (LinearLayout) mContainer.getChildAt(i);
            RelativeLayout dataPairLayout = (RelativeLayout) frameLayout.getChildAt(0);
            // durch den Container mit den RelativeLayouts interieren,
            // um die Datenpaare heraus zu ziehen
            for (int j = 0; j < 2; j++) {
                if (j == 0) {
                    // Beschreibung des Datenpaars
                    TextView tvDescription = (TextView) dataPairLayout.getChildAt(j);
                    desc = tvDescription.getText().toString();
                } else {
                    // Wert des Datenpaars
                    EditText etValue = (EditText) dataPairLayout.getChildAt(j);
                    value = etValue.getText().toString();
                }
            }
            if (!desc.equals(getString(R.string.title))) dataList.add(new DataPair(desc, value));
        }

        if (isTitleSet()) {
            ToastUtils.toastLong(R.string.toast_card_could_not_be_saved);
            return;
        }

        if (mIsEditing) changeRecord(dataList);
        else addRecord(dataList);

        // ab jetzt kann kein zweites Mal gespeichert werden
        mIsWritten = true;
    }


    private void addRecord(ArrayList<DataPair> dataList) {
        mCard = new Card(mEtTitle.getText().toString(), false, new CardIcon(CardIcon.getUnset()));
        long cardId = mDatabase.createCard(mCard);

        for (DataPair dataPair : dataList) {
            long datapairId = mDatabase.createDataPair(dataPair);
            mDatabase.createDataPairLink(cardId, datapairId);
        }

        // Karte überschreiben -> fehlerfreie Zurück-Navigation ermöglichen
        mCard.setId((int) cardId);

        backToShowCardActivity();
    }


    private void changeRecord(ArrayList<DataPair> dataPairs) {
        List<DataPair> oldDataPairs = mDatabase.getDatapairsByCard(mCard.getId());

        for (DataPair dataPair : oldDataPairs) {
            mDatabase.deleteDatapair(dataPair.getId());
        }

        mDatabase.removeAllDatapairLinks(mCard.getId());

        mCard.setTitle(mEtTitle.getText().toString());
        mCard.setCreated(Logic.getTimeForId());
        mDatabase.updateCard(mCard);

        for (DataPair dataPair : dataPairs) {
            long dataPairId = mDatabase.createDataPair(dataPair);
            // Achtung hier für Karte nicht den Return-Wert nehmen.
            mDatabase.createDataPairLink(mCard.getId(), dataPairId);
        }

        backToShowCardActivity();
    }


    private void addTitleSection(String title) {
        View view = getLayoutInflater().inflate(R.layout.card_editor_title_layout, null);
        TextView tvTitle = (TextView) view.findViewById(R.id.tv_description);
        tvTitle.setText(getString(R.string.title));

        mEtTitle = (EditText) view.findViewById(R.id.et_value);
        mEtTitle.setText(title);

        mEtTitle.requestFocus();
        mEtTitle.setSelection(mEtTitle.getText().length());
        mContainer.addView(view);
    }


    public void addContent(String description, String value, boolean requestFocus) {
        final View view = getLayoutInflater().inflate(R.layout.card_editor_datapair_layout, null);
        TextView tvDescription = (TextView) view.findViewById(R.id.tv_description);
        tvDescription.setText(description);

        final EditText et = (EditText) view.findViewById(R.id.et_value);
        et.setText(value);
        et.setHint(getString(R.string.insert_some_data));
        if (requestFocus) et.requestFocus();

        ImageView btnRemove = (ImageView) view.findViewById(R.id.btn_remove);
        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContainer.removeView(view);
            }
        });

        mContainer.addView(view, mContainer.getChildCount());
    }


    public void addPwdContent(String description, String value, boolean requestFocus) {
        final View view = getLayoutInflater().inflate(R.layout.card_editor_datapair_generator_layout, null);

        TextView tvDescription = (TextView) view.findViewById(R.id.tv_description);
        tvDescription.setText(description);

        final EditText et = (EditText) view.findViewById(R.id.et_value);
        et.setText(value);
        et.setHint("");
        if (requestFocus) et.requestFocus();

        ImageView btnGenerate = (ImageView) view.findViewById(R.id.btn_generate);
        btnGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPasswordDialog(et);
            }
        });

        ImageView btnRemove = (ImageView) view.findViewById(R.id.btn_remove);
        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContainer.removeView(view);
            }
        });

        mContainer.addView(view, mContainer.getChildCount());
    }


    public void addLoginContent(String description, String value, boolean requestFocus) {
        final View view = getLayoutInflater()
                .inflate(R.layout.card_editor_datapair_login_layout, null);

        TextView tvDesc = (TextView) view.findViewById(R.id.tv_description);
        tvDesc.setText(description);

        // Account-Manager für Autocomplete...
        final ArrayList<String> accountList = new ArrayList<String>();
        List<AccountInfo> accountInfos = mDatabase.getAllAccounts();

        for (AccountInfo ai : accountInfos) {
            accountList.add(ai.getName());
        }

        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, accountList);

        final AutoCompleteTextView autoCompleteTextView =
                (AutoCompleteTextView) view.findViewById(R.id.et_value);

        autoCompleteTextView.setText(value);

        autoCompleteTextView.setAdapter(adapter);

        if (requestFocus) autoCompleteTextView.requestFocus();

        final ImageView btnAccounts = (ImageView) view.findViewById(R.id.btn_autocomplete_account);

        if (accountList.size() >= 1) {
            btnAccounts.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    makePopupMenu(btnAccounts, accountList, autoCompleteTextView);
                }
            });

        } else removeAccountButton(view, btnAccounts);

        ImageView btnRemove = (ImageView) view.findViewById(R.id.btn_remove);
        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContainer.removeView(view);
            }
        });

        mContainer.addView(view, mContainer.getChildCount());
    }


    private void removeAccountButton(View view, ImageView btnAccounts) {
        ImageView rippleView = (ImageView) view.findViewById(R.id.btn_remove);
        rippleView.setVisibility(View.GONE);
        btnAccounts.setVisibility(View.GONE);
    }


    private void makePopupMenu(ImageView btnAccounts, ArrayList<String> accountList, final AutoCompleteTextView autoCompleteTextView) {
        PopupMenu popup = new PopupMenu(this, btnAccounts);

        int i = 0;
        for (String a : accountList) {
            // PopupMenu mit Einträgen füllen
            popup.getMenu().add(i, i, i, a);
            i++;
        }

        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                autoCompleteTextView.setText(item.getTitle());
                return true;
            }
        });

        popup.show();
    }


    private void showPasswordDialog(final EditText pwdField) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .customView(R.layout.dialog_pwd_creation, true)
                .title(R.string.dialog_title_generate_passwort)
                .positiveText(R.string.ok)
                .neutralText(R.string.cancel)
                .negativeText(R.string.gen)
                .autoDismiss(false)
                .callback(new MaterialDialog.ButtonCallback() {

                    // OK...
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        applyPwdAndDismissDialog(dialog, pwdField);
                    }


                    // GENERATE...
                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        showPwd(dialog, generatePwd());
                    }


                    // CANCEL...
                    @Override
                    public void onNeutral(MaterialDialog dialog) {
                        dialog.dismiss();
                    }
                });

        mMaterialDialog = builder.build();
        mMaterialDialog.show();

        String[] pwdCreationMode = getResources().getStringArray(R.array.password_creation_modes);

        mSpinner = (Spinner) mMaterialDialog.findViewById(R.id.spinner);
        mSpinner.setAdapter(new ArrayAdapter<String>(this,
                                                     android.R.layout.simple_spinner_dropdown_item,
                                                     pwdCreationMode));

        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                showPwd(mMaterialDialog, generatePwd());
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mSkbPasswordLength = (SeekBar) mMaterialDialog.findViewById(R.id.sbk_set_pwd_length);
        mSkbPasswordLength.setOnSeekBarChangeListener(this);
        mTvPassword = (TextView) mMaterialDialog.findViewById(R.id.tv_pwd);
        mTvPasswordLength = (TextView) mMaterialDialog.findViewById(R.id.tv_pwd_current_lenght);
        mTvPasswordLength.setTextSize(Const.TEXT_SIZE_SLIDER_UNPRESSED);
        mTvPasswordLength.setText(String.format("%s%d", getString(R.string.length), mSkbPasswordLength.getProgress()));
    }


    private void applyPwdAndDismissDialog(MaterialDialog dialog, EditText pwdField) {
        // Passwort in EditText übergeben
        pwdField.setText(mPwd);
        // Cursor ans Ende setzen
        pwdField.setSelection(pwdField.getText().length());
        dialog.dismiss();
    }


    private String generatePwd() {

        // Länge des Passworts
        mSkbPasswordLength = (SeekBar) mMaterialDialog.findViewById(R.id.sbk_set_pwd_length);

        int pwdLength = mSkbPasswordLength.getProgress();
        char[] pwdChars = new char[pwdLength];

        // Modus
        int selection = mSpinner.getSelectedItemPosition();
        switch (selection) {
            case Const.GEN_KEEP_IN_MIND:
                // Wörterbuch Generierung..
                String sentence = "";

                for (; ; ) {
                    if (sentence.length() > pwdLength + 5) {
                        for (int j = 0; j < pwdLength; j++) {
                            pwdChars[j] = sentence.charAt(j);
                        }
                        return mPwd = String.copyValueOf(pwdChars);
                    }
                    int _theSwitch = getRndInt(300, 0);
                    if (_theSwitch <= 150) {
                        sentence += getTheBestOfAllPossibleWords();
                    } else if (_theSwitch > 150 && _theSwitch < 250) {
                        int _howMany = getRndInt(3, 1);
                        for (int i = 0; i < _howMany; ) {
                            int specialChar;
                            int _specialSwitch = getRndInt(400, 0);
                            if (_specialSwitch <= 200) {
                                specialChar = getRndInt(10, 48);
                            } else if (_specialSwitch > 200 && _specialSwitch <= 300) {
                                specialChar = getRndInt(13, 33);
                            } else if (_specialSwitch > 300 && _specialSwitch < 350) {
                                specialChar = getRndInt(7, 58);
                            } else specialChar = getRndInt(4, 123);

                            if (annoyingCharCheck(specialChar)) continue;
                            char[] c = Character.toChars(specialChar);
                            sentence += c[0];
                            i++;
                        }
                    } else {
                        int _randMax = getRndInt(2, 1);
                        for (int i = 0; i < _randMax; ) {
                            int character = getRndInt(93, 33);
                            if (annoyingCharCheck(character)) continue;
                            char[] c = Character.toChars(character);
                            sentence += c[0];
                            i++;
                        }
                    }
                }

            case Const.GEN_PARANOID:
                for (int i = 0; i < pwdLength; ) {
                    int character = getRndInt(93, 33);
                    if (annoyingCharCheck(character)) continue;
                    char[] c = Character.toChars(character);
                    pwdChars[i] = c[0];
                    i++;
                }
                break;

            case Const.GEN_NO_SPECIAL_CHARS:
                for (int i = 0; i < pwdLength; ) {
                    int character;
                    int flipTheSkip = getRndInt(300, 0);
                    if (flipTheSkip < 100) {
                        character = getRndInt(26, 65);
                    } else if (flipTheSkip > 99 && flipTheSkip < 200) {
                        character = getRndInt(26, 97);
                    } else character = getRndInt(10, 48);
                    if (annoyingCharCheck(character)) continue;
                    char[] c = Character.toChars(character);
                    pwdChars[i] = c[0];
                    i++;
                }
                break;

            case Const.GEN_ONLY_NUMBERS:
                for (int i = 0; i < pwdLength; i++) {
                    int character = getRndInt(10, 48);
                    char[] c = Character.toChars(character);
                    pwdChars[i] = c[0];
                }
                break;

            case Const.GEN_ONLY_CHARS:
                for (int i = 0; i < pwdLength; ) {
                    int character;
                    if (getRndInt(100, 0) < 50) character = getRndInt(26, 65);
                    else character = getRndInt(26, 97);
                    if (annoyingCharCheck(character)) continue;
                    char[] c = Character.toChars(character);
                    pwdChars[i] = c[0];
                    i++;
                }
                break;
        }

        // Passwort wird mPwd abgelegt!
        return mPwd = String.copyValueOf(pwdChars);
    }


    private String getTheBestOfAllPossibleWords() {
        int atLine = getRndInt(LINES_IN_DICT_INCL_OFFSET, 1);
        String lineIWant = "";
        BufferedReader r = null;
        try {

            r = new BufferedReader(new InputStreamReader(
                    getAssets().open("dict_" +
                                     String.format("%02d", getRndInt(AMOUNT_OF_DICTIONARIES, 0)) +
                                     ".txt"
                    )));

            for (int i = 0; i < atLine; i++) r.readLine();

            while (r.readLine() != null) {
                lineIWant = r.readLine();
                if (lineIWant.length() < 7) {
                    r.close();
                    if (getRndInt(100, 1) < 23) return lineIWant;
                    else {
                        return lineIWant.replace(
                                String.valueOf(lineIWant.charAt(0)),
                                String.valueOf(lineIWant.charAt(0)).toUpperCase());
                    }
                }
            }

        } catch (IOException e) {
            Log.e(TAG, "Error while reading file: " + e);
        } finally {
            if (r != null) {
                try {
                    r.close();
                } catch (IOException e) {
                    Log.e(TAG, "Error while reading file: " + e);
                }
            }
        }
        return lineIWant;
    }


    private boolean annoyingCharCheck(int c) {
        return c == '0'
               || c == 'O'
               || c == 'l'
               || c == 'I'
               || c == '`';
    }


    /**
     * ASCII-Char
     * 0: Leicht zu merken -> ..... 33 - 126 (alle Zeichen mit Wörterbuch) => ... nextInt(93) + 33
     * 1: Paranoid -> ............. 33 - 126 (alle Zeichen)                => ... nextInt(93) + 33
     * 2: Keine Sonderzeichen -> .. aus [3] und [4]
     * 3: Nur Zahlen -> ........... 48 - 57                                => ... nextInt(10) + 48
     * 4: Nur Buchstaben -> ....... 65 - 90  +  97 - 122                   => ... nextInt(26) + 65  / nextInt(26) + 97
     */
    private int getRndInt(int amount, int offset) {
        return mRandomGenerator.nextInt(amount) + offset;
    }


    /**
     * Passwort in Ui anzeigen
     */
    private void showPwd(MaterialDialog materialDialog, String p) {
        mTvPassword = (TextView) materialDialog.findViewById(R.id.tv_pwd);
        mTvPassword.setText(p);
        TextView tvPwdStrengh = (TextView) materialDialog.findViewById(R.id.tv_password_strength);
        tvPwdStrengh.setText(Logic.calculateStrength(CardEditorActivity.this, mPwd));
    }


    @Override
    public void onProgressChanged(final SeekBar seekBar, final int progress, final boolean fromUser) {
        if (mTvPasswordLength == null)
            mTvPasswordLength = (TextView) mMaterialDialog.findViewById(R.id.tv_pwd_current_lenght);

        if (progress < Const.PROGRBAR_PWD_MIN_LENGTH) {
            // Passwort zu kurz
            seekBar.setProgress(Const.PROGRBAR_PWD_MIN_LENGTH);
            mTvPasswordLength.setText(getString(R.string.length) + Const.PROGRBAR_PWD_MIN_LENGTH);
            return;
        }

        if (progress >= Const.PROGRBAR_PWD_MIN_LENGTH) {
            // Passwort ausreichend lang
            mTvPasswordLength.setText(getString(R.string.length) + progress);
            showPwd(mMaterialDialog, generatePwd());
        }
    }


    /**
     * Automatisches Ausfahren der Tastatur verhindern.
     */
    private void ensureHideKeyboard(Window window) {
        SharedPreferences prefs = getSharedPreferences(Pref.CONFIG_FILE, Context.MODE_PRIVATE);
        if (!prefs.getBoolean(Pref.PK_CB_OPEN_KEYBOARD, false)) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
