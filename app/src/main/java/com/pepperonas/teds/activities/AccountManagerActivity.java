package com.pepperonas.teds.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.pepperonas.teds.R;
import com.pepperonas.teds.adapter.AccountAdapter;
import com.pepperonas.teds.model.AccountInfo;
import com.pepperonas.teds.model.Database;
import com.pepperonas.teds.dialogs.DialogAccountSetup;
import com.pepperonas.teds.dialogs.DialogAddAccount;
import com.pepperonas.teds.listener.RecyclerItemClickListener;
import com.pepperonas.teds.listener.SwipeableRecyclerViewTouchListener;
import com.pepperonas.teds.utils.Const;
import com.pepperonas.teds.utils.Logic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AccountManagerActivity extends AppCompatActivity {

    private static final String TAG = "AccountManager";
    private Database mDatabase;

    private String mP;

    private RecyclerView mRecyclerView;
    private AccountAdapter mAdapter;

    private BroadcastReceiver mMsgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctx, Intent intent) {
            finish();
        }
    };

    private BroadcastReceiver mScreenOffReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctx, Intent intent) {
            Logic.ensureShowLoginScreen((Activity) ctx);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Logic.applyTheme(this);
        Logic.ensureHideFromRecents(this);

        super.onCreate(savedInstanceState);

        mP = getIntent().getStringExtra(Const.BNL_PW);
        mDatabase = new Database(this, mP);

        setContentView(R.layout.activity_account_manager);
        toolbarSetup();
        Logic.ensureShowHint
                (this, Const.SHOW_ACCOUNT_MANAGER_HINT,
                 getString(R.string.hint_account_manager), 3);

        setupRecyclerView();
    }


    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(mMsgReceiver, new IntentFilter(Const.LBC_INTENT_TO_FINISH));

        registerReceiver(mScreenOffReceiver, new IntentFilter(Intent.ACTION_SCREEN_OFF));

        Logic.ensureShowLoginScreen(this);
    }


    @Override
    protected void onPause() {
        Logic.ensureShowLoginIfLocked(this);
        Logic.ensureEnableLockTimer(this, this);
        mDatabase.close();

        if (mScreenOffReceiver != null) {
            unregisterReceiver(mScreenOffReceiver);
        }

        super.onPause();
    }


    @Override
    protected void onDestroy() {
        mP = null;
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMsgReceiver);

        super.onDestroy();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_account_manager, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;

            case R.id.action_load_accounts:
                showAddAccountsDialog();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void setupRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        TextView tvAccName = (TextView) view.findViewById(R.id.accountName);
                        showSetupDialog(tvAccName.getText().toString());
                    }
                })                          );

        mAdapter = new AccountAdapter(mDatabase.getAllAccounts(), R.layout.account_row, this);

        mRecyclerView.setAdapter(mAdapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_add_account);
        //        fab.setShadow(true);
        //        fab.attachToRecyclerView(mRecyclerView, new ScrollDirectionListener() {
        //            @Override
        //            public void onScrollDown() {
        //
        //            }
        //
        //
        //            @Override
        //            public void onScrollUp() {
        //
        //            }
        //        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSetupDialog("");
            }
        });

        SwipeableRecyclerViewTouchListener swipeDismissTouchListener =
                new SwipeableRecyclerViewTouchListener(
                        mRecyclerView,
                        new SwipeableRecyclerViewTouchListener.SwipeListener() {
                            @Override
                            public boolean canSwipe(int position) {
                                return true;
                            }


                            @Override
                            public void onDismissedBySwipeLeft(RecyclerView recyclerView, int[] reverseSortedPositions) {
                                for (int position : reverseSortedPositions) {
                                    mDatabase.deleteAccount(mAdapter.getItemId(position));
                                    mAdapter.refreshAccountList(mDatabase);
                                }
                            }


                            @Override
                            public void onDismissedBySwipeRight(RecyclerView recyclerView, int[] reverseSortedPositions) {
                                for (int position : reverseSortedPositions) {
                                    mDatabase.deleteAccount(mAdapter.getItemId(position));
                                    mAdapter.refreshAccountList(mDatabase);
                                }
                            }
                        });

        mRecyclerView.addOnItemTouchListener(swipeDismissTouchListener);
    }


    private void showSetupDialog(final String name) {
        new DialogAccountSetup(this, name);
    }


    public void renameAccount(String oldName, String accountName) {
        List<AccountInfo> accountInfos = mDatabase.getAllAccounts();
        mDatabase.deleteAllAccounts();

        int accToDelete = -1;
        for (int i = 0; i < accountInfos.size(); i++) {
            if (accountInfos.get(i).getName().equals(oldName)) {
                accToDelete = i;
            }
        }
        accountInfos.remove(accToDelete);

        AccountInfo ai = new AccountInfo(0, accountName, Logic.getIV());
        accountInfos.add(ai);

        for (AccountInfo info : accountInfos) {
            mDatabase.createAccount(info);
        }

        mAdapter.refreshAccountList(mDatabase);
    }


    public void addNewAccount(String accountName) {
        AccountInfo ai = new AccountInfo(0, accountName, Logic.getIV());
        mDatabase.createAccount(ai);
        mAdapter.refreshAccountList(mDatabase);
    }


    private Toolbar toolbarSetup() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_account_manager);
        Logic.themeToolbar(this, toolbar, null);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return toolbar;
    }


    public void writeAccountsIntoDb() {
        AccountManager manager = (AccountManager) getSystemService(ACCOUNT_SERVICE);

        final Account[] list = manager.getAccounts();
        final ArrayList<String> founds = new ArrayList<String>();

        for (Account a : list) {
            if (Logic.isAccountInfoUseful(founds, a)) founds.add(a.name);
        }

        final List<AccountInfo> exist = mDatabase.getAllAccounts();
        mDatabase.deleteAllAccounts();

        List<String> exists = new ArrayList<String>();
        for (AccountInfo info : exist) {
            exists.add(info.getName());
        }

        for (String s : founds) {
            exists.add(s);
        }

        Collection<String> hs = new HashSet<String>();
        hs.addAll(exists);
        exists.clear();
        exists.addAll(hs);

        for (String s : exists) {
            mDatabase.createAccount(new AccountInfo(0, s, Logic.getIV()));
        }

        mAdapter.refreshAccountList(mDatabase);
    }


    private void showAddAccountsDialog() {
        new DialogAddAccount(this);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    public AccountAdapter getAdapter() {
        return mAdapter;
    }


    public Database getDatabase() {
        return mDatabase;
    }
}
